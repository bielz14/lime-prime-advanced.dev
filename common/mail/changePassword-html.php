<?php
use yii\helpers\Html;
?>
<div class="invite-candidate">
    <h2>Здравствуйте, ваш пароль на ресурсе lime-prime был изменен</h2>
    <div class="text">
    	<p>
    		Ваш новый пароль <b><span style="color: red"><?= $newPass ?></span></b>
    	</p>
    </div>
</div>
