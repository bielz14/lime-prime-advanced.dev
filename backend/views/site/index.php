<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use frontend\assets\AppAsset;
use yii\bootstrap\ActiveForm;
use frontend\components\LinkPager;

AppAsset::register($this);
?>
	<div class="navbar" id="navbar">
		<div class="navbar-content">
			<div class="name">
				<h3><a href="http://crewmsg.com" target="_blank">Crew<span>MSG</span></a></h3>
			</div>
			<div class="nav" id="mynav">
			  <a id="nav-contacts" class="menu_item menu_item_first" href="/admin/site/testingoperation">
			  	<i class="icon icon-Settings"></i>
			  	<p class="lang" key="en_contacts">Testing operation</p>
			  </a>
			  <a id="nav-contacts" class="menu_item menu_item_first" href="/admin/site/logout">
			  	<i class="icon icon-Exit"></i>
			  	<p class="lang" key="en_contacts">Exit</p>
			  </a>
		    </div>
		</div>
	</div>
	<main class="cd-main-content">
		<?php if (isset($resumes) && count($resumes)): ?>
			<?php 
		        	$items = [
				        'Catering' => [
		                    '0' => '2nd Cook',
		                    '1' => 'Camp Boss',
		                    '2' => 'Chief Cook',
		                    '3' => 'Chief Steward',
		                    '4' => 'Cook',
		                    '5' => 'Cook Assistant',
		                    '6' => 'Laundress',
		                    '7' => 'Messman',
		                    '8' => 'Stewardess'
				        ],
				        'Cruise Ships Positions' => [
				            '9' => 'Activities Supervisor',
		                    '10' => 'Art Auctioneer',
		                    '11' => 'Assistant Cook',
		                    '12' => 'Assistant Hotel Manager',
		                    '13' => 'Baker',
		                    '14' => 'Bar Manager',
		                    '15' => 'Bartender',
		                    '16' => 'Barwaiteress',
		                    '17' => 'Buffet attendant',
		                    '18' => 'Cabin Steward assistant',
		                    '19' => 'Cabin Stewardess',
		                    '20' => 'Chef de Cuisine',
		                    '21' => 'Chef de Rang',
		                    '22' => 'Cleaner',
		                    '23' => 'Cook Trainee',
		                    '24' => 'Deckhand',
		                    '25' => 'Dining Room Head Waiter',
		                    '26' => 'Dishwasher',
		                    '27' => 'First Cook',
		                    '28' => 'Galley Cleaner',
		                    '29' => 'General Cook',
		                    '30' => 'Head Waiter',
		                    '31' => 'Hotel Engineer',
		                    '32' => 'Hotel Manager',
		                    '33' => 'Laundry Man',
		                    '34' => 'Medical Officer',
		                    '35' => 'Mess Boy',
		                    '36' => 'Pastry Man',
		                    '37' => 'Photographer',
		                    '38' => 'Purser',
		                    '39' => 'Receptionist',
		                    '40' => 'Restaurant Steward',
		                    '41' => 'Second Cook',
		                    '42' => 'Shop staff',
		                    '43' => 'Shops Salesperson',
		                    '44' => 'Sommelier',
		                    '45' => 'Staff Engineer',
		                    '46' => 'Steward',
		                    '47' => 'Third Cook',
		                    '48' => 'Training and Development Manager',
		                    '49' => 'Utility',
		                    '50' => 'Waiter',
		                    '51' => 'Waitress',
		                    '52' => 'Wine Steward',
		                    '53' => 'Аdministrator'
				        ],
				        ' Deck' => [
	                        '54' => 'Master',
	                        '55' => '1st Officer',
	                        '56' => '2nd Officer',
	                        '57' => '3rd Officer',
	                        '58' => 'AB / Crane Operator',
	                        '59' => 'AB / Welder',
	                        '60' => 'AB Seaman',
	                        '61' => 'AB-Cook',
	                        '62' => 'Anchor Foreman',
	                        '63' => 'Barge Master',
	                        '64' => 'Boatswain',
	                        '65' => 'Carpenter',
	                        '66' => 'Chief Officer',
	                        '67' => 'Crane Operator',
	                        '68' => 'Deck Cadet',
	                        '69' => 'Deck Fitter',
	                        '70' => 'DP Officer',
	                        '71' => 'Fish-Processor',
	                        '72' => 'Gangway operator',
	                        '73' => 'Junior DP Officer',
	                        '74' => 'Junior officer',
	                        '75' => 'Navigation Officer',
	                        '76' => 'OIM',
	                        '77' => 'OS',
	                        '78' => 'OS / Welder',
	                        '79' => 'OS-Cook',
	                        '80' => 'Painter',
	                        '81' => 'Pilot',
	                        '82' => 'Radio Officer',
	                        '83' => 'Safety Officer',
	                        '84' => 'Senior DP Engineer',
	                        '85' => 'Senior DPO',
	                        '86' => 'Staff Captain',
	                        '87' => 'Trainee Officer',
	                        '88' => 'Trawler',
	                        '89' => 'Watchkeeping Officer',
	                        '90' => 'Yacht Master'
				        ],
				        ' Engine' => [
					         '91' => '1st Engineer',
		                     '92' => '2nd Electrical Engineer',
		                     '93' => '2nd Engineer',
		                     '94' => '2nd ETO',
		                     '95' => '3rd Electrical Engineer',
		                     '96' => '3rd Engineer',
		                     '97' => '4th Engineer',
		                     '98' => 'AB / Motorman',
		                     '99' => 'Barge Engineer',
		                     '100' => 'Cargo Engineer',
		                     '101' => 'Chief Electrical Engineer',
		                     '102' => 'Chief Engineer',
		                     '103' => 'El. Eng. Assistant',
		                     '104' => 'El. Engineer',
		                     '105' => 'El.Eng / 3rd Engineer',
		                     '106' => 'Electric Cadet',
		                     '107' => 'Electrician',
		                     '108' => 'Engine Cadet',
		                     '109' => 'Engineer Assistant',
		                     '110' => 'ETO',
		                     '111' => 'Fitter / Welder',
		                     '112' => 'Gas Engineer',
		                     '113' => 'Mechanic',
		                     '114' => 'Motorman',
		                     '115' => 'Motorman-electrician',
		                     '116' => 'Motorman-welder',
		                     '117' => 'Oiler',
		                     '118' => 'OOW Engine',
		                     '119' => 'Plumber',
		                     '120' => 'Pumpman',
		                     '121' => 'Ref. Engineer',
		                     '122' => 'Ref. Machinist',
		                     '123' => 'Single Engineer',
		                     '124' => 'Staff Chief Engineer',
		                     '125' => 'Storekeeper',
		                     '126' => 'Trainee Engineer',
		                     '127' => 'Turner',
		                     '128' => 'Wiper'
		                ],
		                'Rigs / FPSO / other units' => [
						     '129' => 'Ballast Control Operator',
		                     '130' => 'Cargo Operator',
		                     '131' => 'Cargo Superintendent',
		                     '132' => 'Cargo Supervisor',
		                     '133' => 'Control Room Operator',
		                     '134' => 'Diver',
		                     '135' => 'Elect & Instr Supervisor',
		                     '136' => 'Electrical Technician',
		                     '137' => 'Engine Room Supervisor',
		                     '138' => 'Foreman',
		                     '139' => 'Instrument Technician',
		                     '140' => 'Maintenance Engineer',
		                     '141' => 'Maintenance Operator',
		                     '142' => 'Maintenance Superintendent',
		                     '143' => 'Maintenance Supervisor',
		                     '144' => 'Mechanical Supervisor',
		                     '145' => 'Mechanical Technician',
		                     '146' => 'Plant Operator',
		                     '147' => 'Production Engineer',
		                     '148' => 'Production Operator',
		                     '149' => 'Production Technician',
		                     '150' => 'Roustabout',
		                     '151' => 'Superintendant'
		                ],
		                ' Safety / Security / Medical' => [
		                     '152' => 'Doctor',
		                     '153' => 'HSE Manager',
		                     '154' => 'Security Staff',
		                     '155' => 'Ship Security Officer'
		                ],
		                'Shore Positions' => [
		                     '156' => 'Auditor',
		                     '157' => 'Electrical inspector',
		                     '158' => 'Engineer',
		                     '159' => 'Health and Safety Officer',
		                     '160' => 'Mechanical Foreman',
		                     '161' => 'Motorist',
		                     '162' => 'Safety and Operations Manager',
		                     '163' => 'Sandblaster',
		                     '164' => 'Senior Crewing Executive',
		                     '165' => 'Service Technician',
		                     '166' => 'Technical Superintendent',
		                     '167' => 'Training and Competency Coordinator',
		                     '168' => 'Welder'
		                ]
				    ];    
	        ?>
			<section class="cd-gallery">
				<?php foreach ($resumes as $resume): ?>
					<div class="card">
						<div class="card_content">
						  <?php if ($resume->image): ?>
						  	<img class="card_img" src="/frontend/web/uploads/<?= $resume->image ?>" alt="Avatar">
						  <?php else: ?>
						  	<img class="card_img" src="https://www.w3schools.com/howto/img_avatar2.png" alt="Avatar">
						  <?php endif; ?>
						  <div class="card_info">
						    <div class="name_surname">
						    	<h4 class="seafarer_name"><?= $resume->firstname ?></h4>
						    	<h4><?= $resume->surname ?></h4>
						    </div>
						    <p>
						    	<label>Position:</label>
						    	<?php $application_for_position = null; ?>
	        					<?php foreach ($items as $item): ?>
	        						<?php if ($item[$resume->application_for_position]): ?>
	        							<?php $application_for_position = $item[$resume->application_for_position]; ?>
	        							<?php break; ?>
	        						<?php endif; ?>
						    	<?php endforeach; ?>
						    	<span><?= $application_for_position ?></span>
						    </p>
						    <p>
						    	<label>Salary:</label>
						    	<span><?= $resume->salary ?></span>
						    </p>
						    <?php 
					        	$itemsCountries = [
					        		'0' => 'Albania',
				                    '1' => 'Algeria',
				                    '2' => 'Andorra',
				                    '3' => 'Angola',
				                    '4' => 'Antigua and Barbuda',
				                    '5' => 'Argentina',
				                    '6' => 'Armenia',
				                    '7' => 'Aruba',
				                    '8' => 'Australia',
				                    '9' => 'Austria',
				                    '10' => 'Azerbaijan',
				                    '11' => 'Bahamas',
				                    '12' => 'Bahrain',
				                    '13' => 'Bangladesh',
				                    '14' => 'Barbados',
				                    '15' => 'Belarus',
				                    '16' => 'Belgium',
				                    '17' => 'Belize',
				                    '18' => 'Benin',
				                    '19' => 'Bermuda',
				                    '20' => 'Bhutan',
				                    '21' => 'Bolivia',
				                    '22' => 'Bosnia and Herzegovina',
				                    '23' => 'Botswana',
				                    '24' => 'Brazil',
				                    '25' => 'Brunei Darussalam',
				                    '26' => 'Bulgaria',
				                    '27' => 'Burkina Faso',
				                    '28' => 'Burma',
				                    '29' => 'Burundi',
				                    '30' => 'Cambodia',
				                    '31' => 'Cameroon',
				                    '32' => 'Canada',
				                    '33' => 'Cape Verde Islands',
				                    '34' => 'Cayman Islands',
				                    '35' => 'Central African Republic',
				                    '36' => 'Chad',
				                    '37' => 'Chile',
				                    '38' => 'China',
				                    '39' => 'Colombia',
				                    '40' => 'Comoros',
				                    '41' => 'Congo',
				                    '42' => 'Cook Islands',
				                    '43' => 'Costa Rica',
				                    '44' => 'Cote d Ivoire',
				                    '45' => 'Croatia',
				                    '46' => 'Cuba',
				                    '47' => 'Cyprus',
				                    '48' => 'Czech Republic',
				                    '49' => 'Denmark',
				                    '50' => 'Djibouti',
				                    '51' => 'Dominica',
				                    '52' => 'Dominicana',
				                    '53' => 'Ecuador',
				                    '54' => 'Egypt',
				                    '55' => 'El Salvador',
				                    '56' => 'Equatorial Guinea',
				                    '57' => 'Eritrea',
				                    '58' => 'Estonia',
				                    '59' => 'Ethiopia',
				                    '60' => 'Faroe Islands',
				                    '61' => 'Fiji',
				                    '62' => 'Finland',
				                    '63' => 'France',
				                    '64' => 'Gabon',
				                    '65' => 'Gambia',
				                    '66' => 'Georgia',
				                    '67' => 'Germany',
				                    '68' => 'Ghana',
				                    '69' => 'Gibraltar',
				                    '70' => 'Greece',
				                    '71' => 'Grenada',
				                    '72' => 'Guatemala',
				                    '73' => 'Guinea',
				                    '74' => 'Guinea-Bissau',
				                    '75' => 'Guyana',
				                    '76' => 'Haiti',
				                    '77' => 'Honduras',
				                    '78' => 'Hong Kong',
				                    '79' => 'Hungary',
				                    '80' => 'Iceland',
				                    '81' => 'India',
				                    '82' => 'Indonesia',
				                    '83' => 'Iran',
				                    '84' => 'Iraq',
				                    '85' => 'Ireland',
				                    '86' => 'Isle of Man',
				                    '87' => 'Israel',
				                    '88' => 'Italy',
				                    '89' => 'Jamaica',
				                    '90' => 'Japan',
				                    '91' => 'Jordan',
				                    '92' => 'Kazakhstan',
				                    '93' => 'Kenya',
				                    '94' => 'Kiribati',
				                    '95' => 'Korea South',
				                    '96' => 'Korea, North',
				                    '97' => 'Kuwait',
				                    '98' => 'Kyrgyzstan',
				                    '99' => 'Latvia',
				                    '100' => 'Lebanon',
				                    '101' => 'Liberia',
				                    '102' => 'Libya',
				                    '103' => 'Liechtenstein',
				                    '104' => 'Lithuania',
				                    '105' => 'Luxembourg',
				                    '106' => 'Macau',
				                    '107' => 'Macedonia',
				                    '108' => 'Madagascar',
				                    '109' => 'Malawi',
				                    '110' => 'Malaysia',
				                    '111' => 'Maldives',
				                    '112' => 'Mali',
				                    '113' => 'Malta',
				                    '114' => 'Marshall Islands',
				                    '115' => 'Mauritania',
				                    '116' => 'Mauritius',
				                    '117' => 'Mexico',
				                    '118' => 'Micronesia',
				                    '119' => 'Moldova',
				                    '120' => 'Monaco',
				                    '121' => 'Mongolia',
				                    '122' => 'Montenegro',
				                    '123' => 'Morocco',
				                    '124' => 'Mozambique',
				                    '125' => 'Myanmar',
				                    '126' => 'Namibia',
				                    '127' => 'Nauru',
				                    '128' => 'Nepal',
				                    '129' => 'Netherlands',
				                    '130' => 'Netherlands Antilles',
				                    '131' => 'New Zealand',
				                    '132' => 'Nicaragua',
				                    '133' => 'Niger',
				                    '134' => 'Nigeria',
				                    '135' => 'Norway',
				                    '136' => 'Oman',
				                    '137' => 'Pakistan',
				                    '138' => 'Panama',
				                    '139' => 'Papua New Guinea',
				                    '140' => 'Paraguay',
				                    '141' => 'Peru',
				                    '142' => 'Philippines',
				                    '143' => 'Poland',
				                    '144' => 'Portugal',
				                    '145' => 'Qatar',
				                    '146' => 'Romania',
				                    '147' => 'Russia',
				                    '148' => 'Rwanda',
				                    '149' => 'Saint Kitts and Nevis',
				                    '150' => 'Saint Lucia',
				                    '151' => 'Saint Vincent and the Grenadines',
				                    '152' => 'Samoa',
				                    '153' => 'San Marino',
				                    '154' => 'Sao Tome and Principe',
				                    '155' => 'Saudi Arabia',
				                    '156' => 'Senegal',
				                    '157' => 'Serbia',
				                    '158' => 'Seychelles',
				                    '159' => 'Sierra Leone',
				                    '160' => 'Singapore',
				                    '161' => 'Slovakia',
				                    '162' => 'Slovenia',
				                    '163' => 'Solomon Islands',
				                    '164' => 'Somalia',
				                    '165' => 'South Africa',
				                    '166' => 'Spain',
				                    '167' => 'Srb',
				                    '168' => 'Sri Lanka',
				                    '169' => 'St. Vincent, the Grenadines',
				                    '170' => 'Sudan',
				                    '171' => 'Suriname',
				                    '172' => 'Swaziland',
				                    '173' => 'Sweden',
				                    '174' => 'Switzerland',
				                    '175' => 'Syria',
				                    '176' => 'Taiwan',
				                    '177' => 'Tajikistan',
				                    '178' => 'Tanzania',
				                    '179' => 'Thailand',
				                    '181' => 'Timor-Leste',
				                    '182' => 'Togo',
				                    '183' => 'Tonga',
				                    '184' => 'Trinidad and Tobago',
				                    '185' => 'Tunisia',
				                    '186' => 'Turkey',
				                    '187' => 'Turkmenistan',
				                    '188' => 'Tuvalu',
				                    '189' => 'Uganda',
				                    '190' => 'Ukraine',
				                    '191' => 'United Arab Emirates',
				                    '192' => 'United Kingdom',
				                    '193' => 'United States',
				                    '194' => 'Uruguay',
				                    '195' => 'Uzbekistan',
				                    '196' => 'Vanuatu',
				                    '197' => 'Venezuela',
				                    '198' => 'Vietnam',
				                    '199' => 'Yemen',
				                    '200' => 'Zambia',
				                    '201' => 'Zimbabwe',
							    ];
				        	?>
						    <p>
						    	<label>Country:</label>
						    	<span><?= $itemsCountries[$resume->citizenship] ?></span>
						    </p>
						  </div>
						</div>
						<div class="whole_cv">
							<?php $form = ActiveForm::begin(['action' => '/admin/site/updateresume?id=' . $resume->id]); ?>
							    <button>View</button>
							<?php ActiveForm::end(); ?>
						</div>
					</div>
				<?php endforeach; ?>
				<div class="cd-fail-message">No results found</div>
				<?= LinkPager::widget([
					'pagination' => $pages,
					'activePageCssClass' => 'active',
				]); ?>
			</section> <!-- cd-gallery -->
			<div class="cd-filter">
				<?php $form = ActiveForm::begin(['action' => '/admin/site/filter', 'id' => 'search-form']); ?>
					<div class="cd-filter-block">
						<h4>Email</h4>
						
						<div class="cd-filter-content">
							<?= Html::input('text', 'email', null, ['type' => 'email', 'type' => 'search', 'placeholder' => 'Email']) ?>
						</div> <!-- cd-filter-content -->
					</div> <!-- cd-filter-block -->

					<div class="cd-filter-block">
						<h4>Position</h4>
						
						<div class="cd-filter-content">
							<div class="cd-select cd-filters">
								<?= Html::dropDownList('application_for_position', 'null', $items, ['class' => 'filter', 'id' => 'application_for_position', 'label' => false]); ?>
							</div> <!-- cd-select -->
						</div> <!-- cd-filter-content -->
					</div> <!-- cd-filter-block -->

					<div class="cd-filter-block">
						<h4>Citizenship</h4>
						
						<div class="cd-filter-content">
							<div class="cd-select cd-filters">
					        	<?= Html::dropDownList('citizenship', 'null', $itemsCountries, ['class' => 'filter', 'id' => 'citizenship', 'label' => false]); ?>
							</div> <!-- cd-select -->
						</div> <!-- cd-filter-content -->
					</div> <!-- cd-filter-block -->

					<div class="cd-filter-block">
						<h4>Age</h4>
						
						<div class="cd-filter-content">
							<?= Html::input('text', 'min-age', null, ['style' => 'width: 30%', 'type' => 'search', 'placeholder' => 'min']) ?>
							 - 
							<?= Html::input('text', 'max-age', null, ['style' => 'width: 30%', 'type' => 'search', 'placeholder' => 'max']) ?>
						</div> <!-- cd-filter-content -->
					</div> <!-- cd-filter-block -->

					<div class="cd-filter-block">
						<h4>Salary</h4>
						
						<div class="cd-filter-content">
							<?= Html::input('text', 'min-salary', null, ['style' => 'width: 30%', 'type' => 'search', 'placeholder' => 'min']) ?>
							 - 
							<?= Html::input('text', 'max-salary', null, ['style' => 'width: 30%', 'type' => 'search', 'placeholder' => 'max']) ?>
						</div> <!-- cd-filter-content -->
					</div> <!-- cd-filter-block -->

					<div class="cd-filter-block">
						<h4>D.W.T.</h4>
						
						<div class="cd-filter-content">
							<?= Html::input('text', 'min-dwt', null, ['style' => 'width: 30%', 'type' => 'search', 'placeholder' => 'min']) ?>
							 - 
							<?= Html::input('text', 'max-dwt', null, ['style' => 'width: 30%', 'type' => 'search', 'placeholder' => 'max']) ?>
						</div> <!-- cd-filter-content -->
					</div> <!-- cd-filter-block -->

					<div class="cd-filter-block">
						<h4>Check boxes</h4>

						<ul class="cd-filter-content cd-filters list">
							<li>
								<input class="filter" data-filter=".check1" type="checkbox" id="checkbox1">
				    			<label class="checkbox-label" for="checkbox1">Visa USA</label>
							</li>
						</ul> <!-- cd-filter-content -->
					</div> <!-- cd-filter-block -->
					<button class="search_cv" type="submit">Search</button>
				<?php ActiveForm::end(); ?>

				<a href="#0" class="cd-close">Close</a>
			</div> <!-- cd-filter -->
			<a href="#0" class="cd-filter-trigger">Filters</a>
		<?php else: ?>
			<span>Ещё ни одного резюме пока не было создано</span>
		<?php endif; ?>        	
	</main> <!-- cd-main-content -->
<script src="/admin/js/jquery-2.1.1.js"></script>
<script src="/admin/js/admin.js"></script> <!-- Resource jQuery -->
<script>
	$('#application_for_position').val(null);
	$('#citizenship').val(null);
	$('.search_cv').on('click', function(event){
		event.preventDefault();
		var msg = $('#search-form').serialize(); 
		$.ajax({
			type: "POST",
			url: "/admin/site/resumefilter",
			data: msg,
			success: function(data) {
				if (data) {
					$("body").html(data);
				} else {
					alert('Incorrect filter operation');
				}
			},
			error:  function(xhr, str){

			}
		});
	});
</script>