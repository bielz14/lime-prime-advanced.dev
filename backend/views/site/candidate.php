<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\SignupForm */

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\bootstrap\ActiveForm;

?>
<div id="new_candidate" class="modal">
   <div class="modal-content animate">
      <div class="container_modal">
         <span onclick="document.getElementById('new_candidate').style.display='none'" class="close" title="Close Modal">x</span>
         <p>Fill candidate's data</p>
         <?php $form = ActiveForm::begin(['action' => Yii::getAlias('@web') . '/site/addcandidate', 'id' => 'candidate-form', 'options' => ['class' => 'modal_form']]); ?>
            <?=  Html::hiddenInput(Yii::$app->getRequest()->csrfParam, Yii::$app->getRequest()->getCsrfToken(), []); ?>
            <?= $form->field($model, 'firstname')->input('text', ['class' => 'modal_input', 'name' => 'firstname', 'placeholder' => 'Candidate\'s Name', 'required' => ''])->label(false); ?>
            <?= $form->field($model, 'surname')->input('text', ['class' => 'modal_input', 'name' => 'surname', 'placeholder' => 'Candidate\'s Surname', 'required' => ''])->label(false); ?>
            <?= $form->field($model, 'email')->input('email', ['class' => 'modal_input', 'name' => 'email', 'placeholder' => 'Candidate\'s Email', 'required' => ''])->label(false); ?>
            <?= $form->field($model, 'list_id')->dropDownList($questionLists, ['name' => 'list_id', 'required' => '', 'prompt' => 'Выберите список вопросов...'])->label(false); ?>
            <div>
               <button type="submit" class="modal_button">Create</button>
            </div>
         <?php ActiveForm::end(); ?>
         <button class="modal_button cancel_btn" onclick="document.getElementById('new_candidate').style.display = 'none'">Cancel</button>
      </div>
      <div class="container_modal_rights">
         <span>- Made by <a href="#">CrewMSG -</a></span>
         <span>- All rights reserved -</span>
      </div>
   </div>
</div>