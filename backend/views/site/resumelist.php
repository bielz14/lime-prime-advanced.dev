<div class="container" style="margin-top: 10%; display: block; width: 58%;">
    <div class="wrapper" style="padding-left: 5.5%">
    	<div class="question-list-content" style="float: left">
        	<?php if (isset($resumes) && count($resumes)): ?>
		    <table class="table table-bordered" style="margin-top: 2.5%">
		      <thead>
		      	<span style="color: #138496; font-size: 150%">Список резюме</span>
		        <tr>
		          <th>Firstname</th>
		          <th>Surname</th>
		          <th></th>
		        </tr>
		      </thead>
		      <tbody>
		      	<?php foreach ($resumes as $resume): ?>
		      		<tr>
			          <td><?= $resume->firstname ?></td>
			          <td><?= $resume->surname ?></td>
			          <td>
            			<a href="/admin/operation/updateresume?id=<?= $resume->id ?>" style="color: blue">
            				Редактировать
            			</a>
			          </td>
			        </tr>
            	<?php endforeach; ?>
		      </tbody>
		    </table>
            <?php endif; ?>        	
        </div>
    </div>
</div>
