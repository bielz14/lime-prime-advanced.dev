<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $modelLogin \frontend\models\SignupForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

?>
<!DOCTYPE html>
<html lang="en" >
<head>
  <meta charset="UTF-8">
  <title> Crewing service CrewMSG | быстрый старт вашей карьеры в море</title>
  <link rel="shortcut icon" href="img/logo.png" type="image/x-icon">
  <link href="https://fonts.googleapis.com/css?family=Roboto:400,500,700,900" rel="stylesheet">
  <link rel="stylesheet" href="/css/registration.css">
  <link rel="stylesheet" href="/css/stroke-gap-icons.css">
</head>
<body>
  <div class="form">
    <a id="btn-home-page" href="/" class="menu_item">
      <i class="icon icon-House"></i>
    </a>
    <ul class="tab-group">
        <li class="tab active"><a href="#login">Log In</a></li>
      </ul>
      <div class="tab-content">        
        <div id="login">   
          <h1>Welcome Back!</h1>
          <?php $form = ActiveForm::begin(['action' => Yii::getAlias('@web') . '/site/login', 'id' => 'form-login']); ?>
            <?=  Html::hiddenInput(\Yii :: $app->getRequest()->csrfParam, \Yii :: $app->getRequest()->getCsrfToken(), []); ?>
            <div class="field-wrap">
            <?= $form->field($model, 'email')->input('email', ['name' => 'email', 'class' => 'req', 'required' => '', 'autocomplete' => 'off'])->label('Email Address<span class="req">*</span>'); ?>
          </div>
          <div class="field-wrap">
            <?= $form->field($model, 'password')->input('password', ['name' => 'password', 'class' => 'req', 'required' => '', 'autocomplete' => 'off'])->label('Set A Password<span class="req">*</span>'); ?>
          </div>
          <p class="forgot"><a href="#" onclick="document.getElementById('new_pass_req').style.display='block'">Forgot Password?</a></p>
          <?= Html::submitButton('Log In', ['class' => 'button button-block']) ?>
          <?php ActiveForm::end(); ?>
          <div id="new_pass_req" class="modal">
      <div class="modal-content animate">
        <div class="container_modal">
          <span onclick="document.getElementById('new_pass_req').style.display='none'" class="close" title="Close Modal">x</span>
          <p>Type your mail to get new password</p>
          <?php $form = ActiveForm::begin(['action' => Yii::getAlias('@web') . '/site/getnewpass', 'class' => 'get_new_pass']); ?>
              <?= Html::hiddenInput(\Yii :: $app->getRequest()->csrfParam, \Yii :: $app->getRequest()->getCsrfToken(), []); ?>
              <?= Html::textInput('email', null, ['type' => 'email', 'class' => 'mail', 'required' => '', 'placeholder' => 'Email']); ?>
              <?= Html::submitButton('Get new password', ['class' => 'modal_button']) ?>
          <?php ActiveForm::end(); ?>
        </div>
        <div class="container_modal_rights">
          <span>- Made by <a href="#">CrewMSG -</a></span>
          <span>- All rights reserved -</span>
        </div>
      </div>
    </div><!--modal-->
        </div>
      </div>
  <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
  <script  src="/js/registration.js"></script>
  <script>
    var result = window.location.href.match(/login/i);
    if (result) {
      $('#login').toggle('active');
    }
  </script>
</body>
</html>
