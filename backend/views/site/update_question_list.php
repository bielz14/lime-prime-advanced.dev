<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\SignupForm */
   
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
   
?>
<div id="change_list" class="modal">
<div class="modal-content animate">
   <div class="container_modal">
      <span onclick="document.getElementById('change_list').style.display = 'none'" class="close" title="Close Modal">x</span>
      <p>Change list name</p>
      <?php $form = ActiveForm::begin(['action' => Yii::getAlias('@web') . '/site/updatequestionlist', 'id' => 'question-list-form', 'options' => ['class' => 'modal_form']]); ?>
      <?=  Html::hiddenInput(Yii::$app->getRequest()->csrfParam, Yii::$app->getRequest()->getCsrfToken(), []); ?>
      <?= $form->field($model, 'id')->input('hidden', ['id' => 'id', 'name' => 'id'])->label(false); ?>
      <?= $form->field($model, 'title')->input('text', ['name' => 'title', 'class' => 'modal_input', 'required' => '', 'placeholder' => 'Name of the question list'])->label(false); ?>
      <?php 
               $items = [
                    'Catering' => [
                          '0' => '2nd Cook',
                          '1' => 'Camp Boss',
                          '2' => 'Chief Cook',
                          '3' => 'Chief Steward',
                          '4' => 'Cook',
                          '5' => 'Cook Assistant',
                          '6' => 'Laundress',
                          '7' => 'Messman',
                          '8' => 'Stewardess'
                    ],
                    'Cruise Ships Positions' => [
                        '9' => 'Activities Supervisor',
                          '10' => 'Art Auctioneer',
                          '11' => 'Assistant Cook',
                          '12' => 'Assistant Hotel Manager',
                          '13' => 'Baker',
                          '14' => 'Bar Manager',
                          '15' => 'Bartender',
                          '16' => 'Barwaiteress',
                          '17' => 'Buffet attendant',
                          '18' => 'Cabin Steward assistant',
                          '19' => 'Cabin Stewardess',
                          '20' => 'Chef de Cuisine',
                          '21' => 'Chef de Rang',
                          '22' => 'Cleaner',
                          '23' => 'Cook Trainee',
                          '24' => 'Deckhand',
                          '25' => 'Dining Room Head Waiter',
                          '26' => 'Dishwasher',
                          '27' => 'First Cook',
                          '28' => 'Galley Cleaner',
                          '29' => 'General Cook',
                          '30' => 'Head Waiter',
                          '31' => 'Hotel Engineer',
                          '32' => 'Hotel Manager',
                          '33' => 'Laundry Man',
                          '34' => 'Medical Officer',
                          '35' => 'Mess Boy',
                          '36' => 'Pastry Man',
                          '37' => 'Photographer',
                          '38' => 'Purser',
                          '39' => 'Receptionist',
                          '40' => 'Restaurant Steward',
                          '41' => 'Second Cook',
                          '42' => 'Shop staff',
                          '43' => 'Shops Salesperson',
                          '44' => 'Sommelier',
                          '45' => 'Staff Engineer',
                          '46' => 'Steward',
                          '47' => 'Third Cook',
                          '48' => 'Training and Development Manager',
                          '49' => 'Utility',
                          '50' => 'Waiter',
                          '51' => 'Waitress',
                          '52' => 'Wine Steward',
                          '53' => 'Аdministrator'
                    ],
                    ' Deck' => [
                           '54' => 'Master',
                           '55' => '1st Officer',
                           '56' => '2nd Officer',
                           '57' => '3rd Officer',
                           '58' => 'AB / Crane Operator',
                           '59' => 'AB / Welder',
                           '60' => 'AB Seaman',
                           '61' => 'AB-Cook',
                           '62' => 'Anchor Foreman',
                           '63' => 'Barge Master',
                           '64' => 'Boatswain',
                           '65' => 'Carpenter',
                           '66' => 'Chief Officer',
                           '67' => 'Crane Operator',
                           '68' => 'Deck Cadet',
                           '69' => 'Deck Fitter',
                           '70' => 'DP Officer',
                           '71' => 'Fish-Processor',
                           '72' => 'Gangway operator',
                           '73' => 'Junior DP Officer',
                           '74' => 'Junior officer',
                           '75' => 'Navigation Officer',
                           '76' => 'OIM',
                           '77' => 'OS',
                           '78' => 'OS / Welder',
                           '79' => 'OS-Cook',
                           '80' => 'Painter',
                           '81' => 'Pilot',
                           '82' => 'Radio Officer',
                           '83' => 'Safety Officer',
                           '84' => 'Senior DP Engineer',
                           '85' => 'Senior DPO',
                           '86' => 'Staff Captain',
                           '87' => 'Trainee Officer',
                           '88' => 'Trawler',
                           '89' => 'Watchkeeping Officer',
                           '90' => 'Yacht Master'
                    ],
                    ' Engine' => [
                        '91' => '1st Engineer',
                           '92' => '2nd Electrical Engineer',
                           '93' => '2nd Engineer',
                           '94' => '2nd ETO',
                           '95' => '3rd Electrical Engineer',
                           '96' => '3rd Engineer',
                           '97' => '4th Engineer',
                           '98' => 'AB / Motorman',
                           '99' => 'Barge Engineer',
                           '100' => 'Cargo Engineer',
                           '101' => 'Chief Electrical Engineer',
                           '102' => 'Chief Engineer',
                           '103' => 'El. Eng. Assistant',
                           '104' => 'El. Engineer',
                           '105' => 'El.Eng / 3rd Engineer',
                           '106' => 'Electric Cadet',
                           '107' => 'Electrician',
                           '108' => 'Engine Cadet',
                           '109' => 'Engineer Assistant',
                           '110' => 'ETO',
                           '111' => 'Fitter / Welder',
                           '112' => 'Gas Engineer',
                           '113' => 'Mechanic',
                           '114' => 'Motorman',
                           '115' => 'Motorman-electrician',
                           '116' => 'Motorman-welder',
                           '117' => 'Oiler',
                           '118' => 'OOW Engine',
                           '119' => 'Plumber',
                           '120' => 'Pumpman',
                           '121' => 'Ref. Engineer',
                           '122' => 'Ref. Machinist',
                           '123' => 'Single Engineer',
                           '124' => 'Staff Chief Engineer',
                           '125' => 'Storekeeper',
                           '126' => 'Trainee Engineer',
                           '127' => 'Turner',
                           '128' => 'Wiper'
                      ],
                      'Rigs / FPSO / other units' => [
                       '129' => 'Ballast Control Operator',
                           '130' => 'Cargo Operator',
                           '131' => 'Cargo Superintendent',
                           '132' => 'Cargo Supervisor',
                           '133' => 'Control Room Operator',
                           '134' => 'Diver',
                           '135' => 'Elect & Instr Supervisor',
                           '136' => 'Electrical Technician',
                           '137' => 'Engine Room Supervisor',
                           '138' => 'Foreman',
                           '139' => 'Instrument Technician',
                           '140' => 'Maintenance Engineer',
                           '141' => 'Maintenance Operator',
                           '142' => 'Maintenance Superintendent',
                           '143' => 'Maintenance Supervisor',
                           '144' => 'Mechanical Supervisor',
                           '145' => 'Mechanical Technician',
                           '146' => 'Plant Operator',
                           '147' => 'Production Engineer',
                           '148' => 'Production Operator',
                           '149' => 'Production Technician',
                           '150' => 'Roustabout',
                           '151' => 'Superintendant'
                      ],
                      ' Safety / Security / Medical' => [
                           '152' => 'Doctor',
                           '153' => 'HSE Manager',
                           '154' => 'Security Staff',
                           '155' => 'Ship Security Officer'
                      ],
                      'Shore Positions' => [
                           '156' => 'Auditor',
                           '157' => 'Electrical inspector',
                           '158' => 'Engineer',
                           '159' => 'Health and Safety Officer',
                           '160' => 'Mechanical Foreman',
                           '161' => 'Motorist',
                           '162' => 'Safety and Operations Manager',
                           '163' => 'Sandblaster',
                           '164' => 'Senior Crewing Executive',
                           '165' => 'Service Technician',
                           '166' => 'Technical Superintendent',
                           '167' => 'Training and Competency Coordinator',
                           '168' => 'Welder'
                      ]
                ];
      ?>
      <?= $form->field($model, 'category_index')->dropDownList($items, ['name' => 'category_index', 'required' => '', 'prompt' => 'Choose country...'])->label(false); ?>
      <div>
         <button type="submit" class="modal_button">Change list name</button>
      </div>
      <?php ActiveForm::end(); ?>
      <button class="modal_button cancel_btn" onclick="document.getElementById('change_list').style.display='none'">Cancel</button>
      <div class="container_modal_rights">
         <span>- Made by <a href="#">CrewMSG -</a></span>
         <span>- All rights reserved -</span>
      </div>
   </div>
</div>