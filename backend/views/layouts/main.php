<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use backend\assets\AppAsset;
use common\widgets\Alert;
use yii\bootstrap\ActiveForm;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  
  <link rel="shortcut icon" href="web/site/image/logo.png" type="image/x-icon">
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,500,700,900" rel="stylesheet">
  <link rel="stylesheet" href="/admin/css/reset.css"> <!-- CSS reset -->
  <link rel="stylesheet" href="/admin/css/admin.css"> <!-- Resource style -->
  <title> Crewing service CrewMSG | быстрый старт вашей карьеры в море</title>
  <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
  <?= $content ?>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>