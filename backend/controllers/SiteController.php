<?php
namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use backend\models\LoginForm;
use frontend\models\Resume;
use backend\models\SignupForm;
use frontend\models\ResumeForm;
use frontend\models\TravelPassportForm;
use frontend\models\SeamanBookForm;
use frontend\models\UsVisaForm;
use frontend\models\OtherVisasForm;
use frontend\models\OtherSeamanbookForm;
use frontend\models\OtherOtherSeamanbookForm;
use frontend\models\PetroleumForm;
use frontend\models\ChemicalForm;
use frontend\models\GasForm;
use frontend\models\CertificateForm;
use frontend\models\CertificateInternationalForm;
use frontend\models\CertificateLiberianForm;
use frontend\models\CertificateNorwegianForm;
use frontend\models\CertificatePanamanianForm;
use frontend\models\CertificateYellowFeverForm;
use frontend\models\CertificateHealthListForm;
use frontend\models\CertificateDrugTestForm;
use frontend\models\TrainingInstructionsForm;
use frontend\models\BasicFireFightingForm;
use frontend\models\AdvFireFightingForm;
use frontend\models\ElementaryFirstAidForm;
use frontend\models\MedicalFirstAidForm;
use frontend\models\MedicalCareForm;
use frontend\models\PersSafetyRespForm;
use frontend\models\CraftRescueForm;
use frontend\models\FastRescueCraftForm;
use frontend\models\GmdsssForm;
use frontend\models\ManagementLevelForm;
use frontend\models\EcdisForm;
use frontend\models\RadarObservationForm;
use frontend\models\HazmatForm;
use frontend\models\OilTankerForm;
use frontend\models\AdvanceOilTankerForm;
use frontend\models\ChemicalTankerForm;
use frontend\models\GasTankerForm;
use frontend\models\AdvanceGasTankerForm;
use frontend\models\CrudeOilWashingForm;
use frontend\models\InertGasPlantForm;
use frontend\models\IsmCodeForm;
use frontend\models\ShipSecurityOfficerForm;
use frontend\models\ShipSafetyOfficerForm;
use frontend\models\BridgeTeamManagementForm;
use frontend\models\DpInductionForm;
use frontend\models\DpSimulatorForm;
use frontend\models\BridgeEngineRoomResourceManagementForm;
use frontend\models\ShipHandlingForm;
use frontend\models\InternalAuditorsCourseForm;
use frontend\models\TrainingForSeafarersForm;
use frontend\models\SecurityAwarenessForm;
use frontend\models\ElecticalElectronicForm;
use frontend\models\NewBuildingForm;
use frontend\models\SpecialisedProjectsForm;
use frontend\models\SpecialTradesForm;
use frontend\models\ShoreExperienceForm;
use frontend\models\ServiceDetailsForm;
use frontend\models\ReferenceDetailsForm;
use frontend\models\TravelPassport;
use frontend\models\SeamanBook;
use frontend\models\UsVisa;
use frontend\models\OtherVisas;
use frontend\models\OtherSeamanbook;
use frontend\models\OtherOtherSeamanbook;
use frontend\models\Petroleum;
use frontend\models\Chemical;
use frontend\models\Gas;
use frontend\models\Certificate;
use frontend\models\CertificateInternational;
use frontend\models\CertificateLiberian;
use frontend\models\CertificateNorwegian;
use frontend\models\CertificatePanamanian;
use frontend\models\CertificateYellowFever;
use frontend\models\CertificateHealthList;
use frontend\models\CertificateDrugTest;
use frontend\models\TrainingInstructions;
use frontend\models\BasicFireFighting;
use frontend\models\AdvFireFighting;
use frontend\models\ElementaryFirstAid;
use frontend\models\MedicalFirstAid;
use frontend\models\MedicalCare;
use frontend\models\PersSafetyResp;
use frontend\models\CraftRescue;
use frontend\models\FastRescueCraft;
use frontend\models\Gmdsss;
use frontend\models\ManagementLevel;
use frontend\models\Ecdis;
use frontend\models\RadarObservation;
use frontend\models\Hazmat;
use frontend\models\OilTanker;
use frontend\models\AdvanceOilTanker;
use frontend\models\ChemicalTanker;
use frontend\models\GasTanker;
use frontend\models\AdvanceGasTanker;
use frontend\models\CrudeOilWashing;
use frontend\models\InertGasPlant;
use frontend\models\IsmCode;
use frontend\models\ShipSecurityOfficer;
use frontend\models\ShipSafetyOfficer;
use frontend\models\BridgeTeamManagement;
use frontend\models\DpInduction;
use frontend\models\DpSimulator;
use frontend\models\BridgeEngineRoomResourceManagement;
use frontend\models\ShipHandling;
use frontend\models\InternalAuditorsCourse;
use frontend\models\TrainingForSeafarers;
use frontend\models\SecurityAwareness;
use frontend\models\ElecticalElectronic;
use frontend\models\OtherCourse;
use frontend\models\NewBuilding;
use frontend\models\SpecialisedProjects;
use frontend\models\SpecialTrades;
use frontend\models\ShoreExperience;
use frontend\models\ServiceDetails;
use frontend\models\ReferenceDetails;
use vendor\setasign\fpdf\FPDF;
use yii\data\Pagination;
use backend\models\ResumeFilterForm;
use frontend\models\Question;
use frontend\models\QuestionForm;
use frontend\models\QuestionList;
use frontend\models\QuestionListForm;
use frontend\models\Candidate;
use frontend\models\CandidateForm;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error', 'testing'],
                        'allow' => true
                    ],
                    [
                        'actions' => [
                            'logout', 
                            'index', 
                            'resumelist', 
                            'testingoperation', 
                            'updateresume', 
                            'resumefilter',
                            'addquestion', 
                            'deletequestion', 
                            'updatequestion', 
                            'addquestionlist', 
                            'updatequestionlist',
                            'outputquestionlist', 
                            'deletequestionlist',
                            'addcandidate',
                            'updatecandidate',
                            'deletecandidate',
                            'getquestionstolist',
                            'invitecandidate',
                            'uploadvideo',
                            'updateresume'
                        ],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post', 'get'],
                    'resumelist' => ['get'],
                    'testingoperation' => ['get'],
                    'updateresume' => ['post', 'get'],
                    'resumefilter' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {   
        //$resumes = Resume::find()->all();
        $query = Resume::find();
        $pages = new Pagination(['totalCount' => $query->count(), 'pageSize' => 6]);
        $resumes = $query->offset($pages->offset)
            ->limit($pages->limit)
            ->all();
        
        return $this->render('index', compact('resumes', 'pages'));
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {   
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post(),'') && $model->login()) {   
            return $this->goBack();
        } else {
            $model->password = '';

            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->redirect('/admin/site/login', 302);
    }

    public function actionUpdateresume()
    {   
        if (Yii::$app->user->can('updateResume')) {           
            if (!Yii::$app->user->isGuest) {
                $this->layout = 'resume';
                $resume = new ResumeForm();
                if ($resume->load(Yii::$app->request->post(),'') && $resume->validate()) {
                    $data = Yii::$app->request->post();
                    foreach ($data  as $key => $value) {
                        if ($key == 'document_no') {
                            unset($data[$key]);
                            break;
                        }
                        unset($data[$key]);
                    }
                    if ($resume->updateResume($data)) {
                        return $this->redirect('/admin/site/updateresume?id=' . $resume->id[0]);
                        
                    }
                } 

                if (!Yii::$app->request->get('id')) {
                    return $this->goHome();
                }

                $authenticationUserResume = Resume::find()
                                            ->where(['id' => Yii::$app->request->get('id')])
                                            ->one();
                if ($authenticationUserResume) {                        
                    $travelpassport = TravelPassport::findById($authenticationUserResume->travel_passport_id);
                    if (!$travelpassport) {
                        $travelpassport = TravelPassport::find()->one();
                    }
                    $seamanbook = SeamanBook::findById($authenticationUserResume->seaman_book_id);
                    if (!$seamanbook) {
                        $seamanbook = SeamanBook::find()->one();
                    }
                    $usvisa = UsVisa::findById($authenticationUserResume->us_visa_id);
                    if (!$usvisa) {
                        $usvisa = UsVisa::find()->one();
                    }
                    $othervisas = OtherVisas::findById($authenticationUserResume->other_visas_id);
                    if (!$othervisas) {
                        $othervisas = OtherVisas::find()->one();
                    }

                    $otherseamanbookIds = preg_replace('/^:|:$/', '', $authenticationUserResume->other_seaman_book_ids);
                    $otherseamanbookIds = explode('::', $otherseamanbookIds);
                    $otherseamanbooks = [];
                    foreach ($otherseamanbookIds as $otherseamanbookId) {
                        $otherseamanbook = OtherSeamanbook::findById($otherseamanbookId);
                        if ($otherseamanbook) {
                            array_push($otherseamanbooks, $otherseamanbook);
                        }
                        
                    }

                    /*$otherseamanbook = OtherSeamanbook::findById($otherseamanbookIds[0]);
                    if (!$otherseamanbook) {
                        $otherseamanbook = OtherSeamanbook::find()->one();
                    }*/

                    $petroleum = Petroleum::findById($authenticationUserResume->petroleum_id);
                    if (!$petroleum) {
                        $petroleum = Petroleum::find()->one();
                    }
                    $chemical = Chemical::findById($authenticationUserResume->chemical_id);
                    if (!$chemical) {
                        $chemical = Chemical::find()->one();
                    }
                    $gas = Gas::findById($authenticationUserResume->gas_id);
                    if (!$gas) {
                        $gas = Gas::find()->one();
                    }

                    $certificateIds = preg_replace('/^:|:$/', '', $authenticationUserResume->certificate_ids);
                    $certificateIds = explode('::', $certificateIds);
                    $certificates = [];
                    foreach ($certificateIds as $certificateId) {
                        $certificate = Certificate::findById($certificateId);
                        if ($certificate) {
                            array_push($certificates, $certificate);
                        }
                    }

                    $certificateInternational = CertificateInternational::findById($authenticationUserResume->certificate_international_id);
                    if (!$certificateInternational) {
                        $certificateInternational = CertificateInternational::find()->one();
                    }
                    $certificateLiberian = CertificateLiberian::findById($authenticationUserResume->certificate_liberian_id);
                    if (!$certificateLiberian) {
                        $certificateLiberian = CertificateLiberian::find()->one();
                    }
                    $certificateNorwegian = CertificateNorwegian::findById($authenticationUserResume->certificate_norwegian_id);
                    if (!$certificateNorwegian) {
                        $certificateNorwegian = CertificateNorwegian::find()->one();
                    }
                    $certificatePanamanian = CertificatePanamanian::findById($authenticationUserResume->certificate_panamanian_id);
                    if (!$certificatePanamanian) {
                        $certificatePanamanian = CertificatePanamanian::find()->one();
                    }
                    $certificateYellowFever = CertificateYellowFever::findById($authenticationUserResume->certificate_yellow_fever_id);
                    if (!$certificateYellowFever) {
                        $certificateYellowFever = CertificateYellowFever::find()->one();
                    }
                    $certificateHealthList = CertificateHealthList::findById($authenticationUserResume->certificate_health_list_id);
                    if (!$certificateHealthList) {
                        $certificateHealthList = CertificateHealthList::find()->one();
                    }
                    $certificateDrugTest = CertificateDrugTest::findById($authenticationUserResume->certificate_drug_test_id);
                    if (!$certificateDrugTest) {
                        $certificateDrugTest = CertificateDrugTest::find()->one();
                    }
                    $trainingInstructions = TrainingInstructions::findById($authenticationUserResume->training_instructions_id);
                    if (!$trainingInstructions) {
                        $trainingInstructions = TrainingInstructions::find()->one();
                    }
                    $basicFireFighting = BasicFireFighting::findById($authenticationUserResume->basic_fire_fighting_id);
                    if (!$basicFireFighting) {
                        $basicFireFighting = BasicFireFighting::find()->one();
                    }
                    $advFireFighting = AdvFireFighting::findById($authenticationUserResume->adv_fire_fighting_id);
                    if (!$advFireFighting) {
                        $advFireFighting = AdvFireFighting::find()->one();
                    }
                    $elementaryFirstAid = ElementaryFirstAid::findById($authenticationUserResume->elementary_first_aid_id);
                    if (!$elementaryFirstAid) {
                        $elementaryFirstAid = ElementaryFirstAid::find()->one();
                    }
                    $medicalFirstAid = MedicalFirstAid::findById($authenticationUserResume->medical_first_aid_id);
                    if (!$medicalFirstAid) {
                        $medicalFirstAid = MedicalFirstAid::find()->one();
                    }
                    $medicalCare = MedicalCare::findById($authenticationUserResume->medical_care_id);
                    if (!$medicalCare) {
                        $medicalCare = MedicalCare::find()->one();
                    }
                    $persSafetyResp = PersSafetyResp::findById($authenticationUserResume->pers_safety_resp_id);
                    if (!$persSafetyResp) {
                        $persSafetyResp = PersSafetyResp::find()->one();
                    }
                    $craftRescue = CraftRescue::findById($authenticationUserResume->craft_rescue_id);
                    if (!$craftRescue) {
                        $craftRescue = CraftRescue::find()->one();
                    }
                    $fastRescueCraft = FastRescueCraft::findById($authenticationUserResume->fast_rescuecraft_id);
                    if (!$fastRescueCraft) {
                        $fastRescueCraft = FastRescueCraft::find()->one();
                    }
                    $gmdsss = Gmdsss::findById($authenticationUserResume->gmdsss_id);
                    if (!$gmdsss) {
                        $gmdsss = Gmdsss::find()->one();
                    }
                    $managementLevel = ManagementLevel::findById($authenticationUserResume->management_level_id);
                    if (!$managementLevel) {
                        $managementLevel = ManagementLevel::find()->one();
                    }
                    $ecdis = Ecdis::findById($authenticationUserResume->ecdis_id);
                    if (!$ecdis) {
                        $ecdis = Ecdis::find()->one();
                    }
                    $radarObservation = RadarObservation::findById($authenticationUserResume->radar_observation_id);
                    if (!$radarObservation) {
                        $radarObservation = RadarObservation::find()->one();
                    }
                    $hazmat = Hazmat::findById($authenticationUserResume->hazmat_id);
                    if (!$hazmat) {
                        $hazmat = Hazmat::find()->one();
                    }
                    $oilTanker = OilTanker::findById($authenticationUserResume->oil_tanker_id);
                    if (!$oilTanker) {
                        $oilTanker = OilTanker::find()->one();
                    }
                    $advanceOilTanker = AdvanceOilTanker::findById($authenticationUserResume->advance_oil_tanker_id);
                    if (!$advanceOilTanker) {
                        $advanceOilTanker = AdvanceOilTanker::find()->one();
                    }
                    $chemicalTanker = ChemicalTanker::findById($authenticationUserResume->chemical_tanker_id);
                    if (!$chemicalTanker) {
                        $chemicalTanker = ChemicalTanker::find()->one();
                    }
                    $gasTanker = GasTanker::findById($authenticationUserResume->gas_tanker_id);
                    if (!$gasTanker) {
                        $gasTanker = GasTanker::find()->one();
                    }
                    $advanceGasTanker = AdvanceGasTanker::findById($authenticationUserResume->advance_gas_tanker_id);
                    if (!$advanceGasTanker) {
                        $advanceGasTanker = AdvanceGasTanker::find()->one();
                    }
                    $crudeOilWashing = CrudeOilWashing::findById($authenticationUserResume->crude_oil_washing_id);
                    if (!$crudeOilWashing) {
                        $crudeOilWashing = CrudeOilWashing::find()->one();
                    }
                    $inertGasPlant = InertGasPlant::findById($authenticationUserResume->inert_gas_plant_id);
                    if (!$inertGasPlant) {
                        $inertGasPlant = InertGasPlant::find()->one();
                    }
                    $ismCode = IsmCode::findById($authenticationUserResume->ism_code_id);
                    if (!$ismCode) {
                        $ismCode = IsmCode::find()->one();
                    }
                    $shipSecurityOfficer = ShipSecurityOfficer::findById($authenticationUserResume->ship_security_officer_id);
                    if (!$shipSecurityOfficer) {
                        $shipSecurityOfficer = ShipSecurityOfficer::find()->one();
                    }
                    $shipSafetyOfficer = ShipSafetyOfficer::findById($authenticationUserResume->ship_safety_officer_id);
                    if (!$shipSafetyOfficer) {
                        $shipSafetyOfficer = ShipSafetyOfficer::find()->one();
                    }
                    $bridgeTeamManagement = BridgeTeamManagement::findById($authenticationUserResume->bridge_team_management_id);
                    if (!$bridgeTeamManagement) {
                        $bridgeTeamManagement = BridgeTeamManagement::find()->one();
                    }
                    $dpInduction = DpInduction::findById($authenticationUserResume->dp_induction_id);
                    if (!$dpInduction) {
                        $dpInduction = DpInduction::find()->one();
                    }
                    $dpSimulator = DpSimulator::findById($authenticationUserResume->dp_simulator_id);
                    if (!$dpSimulator) {
                        $dpSimulator = DpSimulator::find()->one();
                    }
                    $bridgeEngineRoomResourceManagement = BridgeEngineRoomResourceManagement::findById($authenticationUserResume->bridge_engine_room_resource_management_id);
                    if (!$bridgeEngineRoomResourceManagement) {
                        $bridgeEngineRoomResourceManagement = BridgeEngineRoomResourceManagement::find()->one();
                    }
                    $shipHandling = ShipHandling::findById($authenticationUserResume->ship_handling_id);
                    if (!$shipHandling) {
                        $shipHandling = ShipHandling::find()->one();
                    }
                    $internalAuditorsCourse = InternalAuditorsCourse::findById($authenticationUserResume->internal_auditors_course_id);
                    if (!$internalAuditorsCourse) {
                        $internalAuditorsCourse = InternalAuditorsCourse::find()->one();
                    }
                    $trainingForSeafarers = TrainingForSeafarers::findById($authenticationUserResume->training_for_seafarers_id);
                    if (!$trainingForSeafarers) {
                        $trainingForSeafarers = TrainingForSeafarers::find()->one();
                    }
                    $securityAwareness = SecurityAwareness::findById($authenticationUserResume->security_awareness_id);
                    if (!$securityAwareness) {
                        $securityAwareness = SecurityAwareness::find()->one();
                    }
                    $electicalElectronic = ElecticalElectronic::findById($authenticationUserResume->electical_electronic_id);
                    if (!$electicalElectronic) {
                        $electicalElectronic = ElecticalElectronic::find()->one();
                    }
                    $courseIds = preg_replace('/^:|:$/', '', $authenticationUserResume->other_course_ids);
                    $courseIds = explode('::', $courseIds);
                    $courses = [];
                    foreach ($courseIds as $courseId) {
                        $course = OtherCourse::findById($courseId);
                        if ($course) {
                            array_push($courses, $course);
                        }
                    }

                    $newBuilding = NewBuilding::findById($authenticationUserResume->new_building_id);
                    if (!$newBuilding) {
                        $newBuilding = NewBuilding::find()->one();
                    }
                    $specialisedProjects = SpecialisedProjects::findById($authenticationUserResume->specialised_projects_id);
                    if (!$specialisedProjects) {
                        $specialisedProjects = SpecialisedProjects::find()->one();
                    }
                    $specialTrades = SpecialTrades::findById($authenticationUserResume->special_trades_id);
                    if (!$specialTrades) {
                        $specialTrades = SpecialTrades::find()->one();
                    }
                    $shoreExperience = ShoreExperience::findById($authenticationUserResume->shore_experience_id);
                    if (!$shoreExperience) {
                        $shoreExperience = ShoreExperience::find()->one();
                    }

                    $serviceDetailsIds = preg_replace('/^:|:$/', '', $authenticationUserResume->service_details_ids);
                    $serviceDetailsIds = explode('::', $serviceDetailsIds);
                    $servicesDetails = [];
                    foreach ($serviceDetailsIds as $serviceDetailsId) {
                        $serviceDetails = ServiceDetails::findById($serviceDetailsId);
                        if ($serviceDetails) {
                            array_push($servicesDetails, $serviceDetails);
                        }
                    }

                    $referenceDetailsIds = preg_replace('/^:|:$/', '', $authenticationUserResume->reference_details_ids);
                    $referenceDetailsIds = explode('::', $referenceDetailsIds);
                    $referencesDetails = [];
                    foreach ($serviceDetailsIds as $referenceDetailsId) {
                        $referenceDetails = ReferenceDetails::findById($referenceDetailsId);
                        if ($referenceDetails) {
                            array_push($referencesDetails, $referenceDetails);
                        }
                    }

                    return $this->render('update-resume', [
                            'resume' => $authenticationUserResume,
                            'travelpassport' => $travelpassport,
                            'seamanbook' => $seamanbook,
                            'usvisa' => $usvisa,
                            'othervisas' => $othervisas,
                            'otherseamanbooks' => $otherseamanbooks,
                            'petroleum' => $petroleum,
                            'chemical' => $chemical,
                            'gas' => $gas,
                            'certificates' => $certificates,
                            'certificateInternational' => $certificateInternational,
                            'certificateLiberian' => $certificateLiberian,
                            'certificateNorwegian' => $certificateNorwegian,
                            'certificatePanamanian' => $certificatePanamanian,
                            'certificateYellowFever' => $certificateYellowFever,
                            'certificateHealthList' => $certificateHealthList,
                            'certificateDrugTest' => $certificateDrugTest,
                            'trainingInstructions' => $trainingInstructions,
                            'basicFireFighting' => $basicFireFighting,
                            'advFireFighting' => $advFireFighting,
                            'elementaryFirstAid' => $elementaryFirstAid,
                            'medicalFirstAid' => $medicalFirstAid,
                            'medicalCare' => $medicalCare,
                            'persSafetyResp' => $persSafetyResp,
                            'craftRescue' => $craftRescue,
                            'fastRescueCraft' => $fastRescueCraft,
                            'gmdsss' => $gmdsss,
                            'managementLevel' => $managementLevel,
                            'ecdis' => $ecdis,
                            'radarObservation' => $radarObservation,
                            'hazmat' => $hazmat,
                            'oilTanker' => $oilTanker,
                            'advanceOilTanker' => $advanceOilTanker,
                            'chemicalTanker' => $chemicalTanker,
                            'gasTanker' => $gasTanker,
                            'advanceGasTanker' => $advanceGasTanker,
                            'crudeOilWashing' => $crudeOilWashing,
                            'inertGasPlant' => $inertGasPlant,
                            'ismCode' => $ismCode,
                            'shipSecurityOfficer' => $shipSecurityOfficer,
                            'shipSafetyOfficer' => $shipSafetyOfficer,
                            'bridgeTeamManagement' => $bridgeTeamManagement,
                            'dpInduction' => $dpInduction,
                            'dpSimulator' => $dpSimulator,
                            'bridgeEngineRoomResourceManagement' => $bridgeEngineRoomResourceManagement,
                            'shipHandling' => $shipHandling,
                            'internalAuditorsCourse' => $internalAuditorsCourse,
                            'trainingForSeafarers' => $trainingForSeafarers,
                            'securityAwareness' => $securityAwareness,
                            'electicalElectronic' => $electicalElectronic,
                            'courses' => $courses,
                            'newBuilding' => $newBuilding,
                            'specialisedProjects' => $specialisedProjects,
                            'specialTrades' => $specialTrades,
                            'shoreExperience' => $shoreExperience,
                            'servicesDetails' => $servicesDetails,
                            'referencesDetails' => $referencesDetails
                    ]);
                }
            }
        } 

        return $this->goHome();
    }

    public function actionResumefilter()
    {
        $resumeFilter = new ResumeFilterForm();
        if ($resumeFilter->load(Yii::$app->request->post(),'') && $resumeFilter->validate()) {
            $resumes = array();

            if (Yii::$app->request->post('email')) {
                $resumesByEmail = Resume::find()->where(['like', 'email', Yii::$app->request->post('email')])->asArray()->all();
                $resumes = $resumesByEmail;
            }

            if (!is_null(Yii::$app->request->post('application_for_position'))) {
                $resumesByPosition = Resume::find()->where(['application_for_position' => Yii::$app->request->post('application_for_position')])->asArray()->all();
                if (count($resumes)) {
                    $intermediateArray = array();
                    foreach ($resumes as $resume) {
                        if (in_array($resume, $resumesByPosition)) {
                            array_push($intermediateArray, $resume); 
                        }
                    }
                    $resumes = $intermediateArray;
                } else {
                    $resumes = $resumesByPosition;
                }  
            }

            if (!is_null(Yii::$app->request->post('citizenship'))) {
                $resumesByCitizenship = Resume::find()->where(['citizenship' => Yii::$app->request->post('citizenship')])->asArray()->all();
                                if (count($resumes)) {
                    $intermediateArray = array();
                    foreach ($resumes as $resume) {
                        if (in_array($resume, $resumesByCitizenship)) {
                            array_push($intermediateArray, $resume); 
                        }
                    }
                    $resumes = $intermediateArray;
                } else {
                    $resumes = $resumesByCitizenship;
                } 
            }

            if (Yii::$app->request->post('min-age')) { 
                $year = 1970 + ((int)Yii::$app->request->post('min-age')) - 1;
                $value = strtotime(date('Y-m-d')) - strtotime($year . '.01.01');
                $resumesByMinAge = Resume::find()->where(['<', 'date_of_birth', $value])->asArray()->all();
                if (count($resumes)) {
                    $intermediateArray = array();
                    foreach ($resumes as $resume) {
                        if (in_array($resume, $resumesByMinAge)) {
                            array_push($intermediateArray, $resume); 
                        }
                    }
                    $resumes = $intermediateArray;
                } else {
                    $resumes = $resumesByMinAge;
                }
            }

            if (Yii::$app->request->post('max-age')) { 
                $year = 1970 + ((int)Yii::$app->request->post('max-age'));
                $value = strtotime(date('Y-m-d')) - strtotime($year . '.01.01');
                $resumesByMaxAge = Resume::find()->where(['>=', 'date_of_birth', $value])->asArray()->all();
                if (count($resumes)) {
                    $intermediateArray = array();
                    foreach ($resumes as $resume) {
                        if (in_array($resume, $resumesByMaxAge)) {
                            array_push($intermediateArray, $resume); 
                        }
                    }
                    $resumes = $intermediateArray;
                } else {
                    $resumes = $resumesByMaxAge;
                }
            }

            if (Yii::$app->request->post('min-salary')) { 
                $resumesByMinSalary = Resume::find()->where(['>=', 'salary', Yii::$app->request->post('min-salary')])->asArray()->all();
                if (count($resumes)) {
                    $intermediateArray = array();
                    foreach ($resumes as $resume) {
                        if (in_array($resume, $resumesByMinSalary)) {
                            array_push($intermediateArray, $resume); 
                        }
                    }
                    $resumes = $intermediateArray;
                } else {
                    $resumes = $resumesByMinSalary;
                }
            }

            if (Yii::$app->request->post('max-salary')) { 
                $resumesByMaxSalary = Resume::find()->where(['<=', 'salary', Yii::$app->request->post('max-salary')])->asArray()->all();
                if (count($resumes)) {
                    $intermediateArray = array();
                    foreach ($resumes as $resume) {
                        if (in_array($resume, $resumesByMaxSalary)) {
                            array_push($intermediateArray, $resume); 
                        }
                    }
                    $resumes = $intermediateArray;
                } else {
                    $resumes = $resumesByMaxSalary;
                }
            }

            if (Yii::$app->request->post('min-dwt')) { 
                $servicesDetailsByMinDwt = ServiceDetails::find()->where(['>=', 'dwt', Yii::$app->request->post('min-dwt')])->all();
                if (!count($resumes)) {
                    $resumes = Resume::find()->asArray()->all();
                }
                foreach ($resumes as $key => $resume) {
                    $serviceDetailsIds = preg_replace('/^:|:$/', '', $resume['service_details_ids']);
                    $serviceDetailsIds = explode('::', $serviceDetailsIds);
                    $servicesDetailsByMinDwtExists = false;
                    foreach ($servicesDetailsByMinDwt as $serviceDetailsByMinDwt) {
                        if (in_array($serviceDetailsByMinDwt->id, $serviceDetailsIds)) {
                            $servicesDetailsByMinDwtExists = true;
                        }
                    }
                    if (!$servicesDetailsByMinDwtExists) {
                        unset($resumes[$key]);
                    }
                }
            }

            if (Yii::$app->request->post('max-dwt')) { 
                $servicesDetailsByMaxDwt = ServiceDetails::find()->where(['<=', 'dwt', Yii::$app->request->post('max-dwt')])->all();
                if (!count($resumes)) {
                    $resumes = Resume::find()->asArray()->all();
                }
                foreach ($resumes as $key => $resume) {
                    $serviceDetailsIds = preg_replace('/^:|:$/', '', $resume['service_details_ids']);
                    $serviceDetailsIds = explode('::', $serviceDetailsIds);
                    $servicesDetailsByMaxDwtExists = false;
                    foreach ($servicesDetailsByMaxDwt as $serviceDetailsByMaxDwt) {
                        if (in_array($serviceDetailsByMaxDwt->id, $serviceDetailsIds)) {
                            $servicesDetailsByMaxDwtExists = true;
                        }
                    }
                    if (!$servicesDetailsByMaxDwtExists) {
                        unset($resumes[$key]);
                    }
                }
            }

            $resumeFilterIds = array();
            if (count($resumes)) {
                foreach ($resumes as $resume) {
                    array_push($resumeFilterIds, $resume['id']);
                }
            } else {
                return false;
            }

            $query = Resume::find()->where(['id' => $resumeFilterIds]);
            $pages = new Pagination(['totalCount' => $query->count(), 'pageSize' => 6]);
            $resumes = $query->offset($pages->offset)
                ->limit($pages->limit)
                ->all();
            
            return $this->render('index', compact('resumes', 'pages'));
        }
    }

    public function actionTestingoperation()
    {   
        if (Yii::$app->user->can('viewManagerPage')) {
            $userId = Yii::$app->user->identity->id;
            if (Yii::$app->request->get('list_id')) {
                $questions = Question::find()->where(['list_id' => Yii::$app->request->get('list_id')])->orderBy(['position' => SORT_ASC])->all();
            } else {
                $questions = Question::find()->orderBy(['position' => SORT_ASC])->all();
            }
         
            $questionLists = QuestionList::find()->all();

            $candidates = Candidate::find()->all();  

            return $this->render('testing-operation', [
                 'questions' => $questions,
                 'questionLists' => $questionLists,
                 'questionTab' => true,
                 'candidates' => $candidates
            ]);
        }

        return $this->goHome();
    }

        public function actionGetquestionstolist()
    {  
        if (Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        if (isset(Yii::$app->request->get()['question_list_id'])) {
            $listId = Yii::$app->request->get()['question_list_id'][0];
            $questionList = QuestionList::findOne($listId);
            if ($questionList->author_id == Yii::$app->user->identity->id) {
                $questionsToList = Question::find()->where(['list_id' => $listId])->all();
                if (count($questionsToList)) {
                    return \yii\helpers\Json::encode($questionsToList);
                }
            }      
        }

        return false;
    }

    public function actionAddquestion()
    {   
        if (Yii::$app->user->can('createQuestion')) {
            if (Yii::$app->user->isGuest) {
                return $this->goHome();
            }
                
            $model = new QuestionForm(); 
            if ($model->load(Yii::$app->request->post(),'')) {
                if ($model->addQuestion()) {
                    $listId = Yii::$app->request->post('list_id');
                    return $this->redirect('/admin/site/testingoperation??questions=truelist_id=' . $listId, 302);
                }  
            } 
            
            if (Yii::$app->request->isAjax) {
                $listId = Yii::$app->request->get('list_id');
                return $this->renderAjax('question', [
                    'model' => $model,
                    'listId' => $listId
                ]);
            }

            return $this->render('question', [
                'model' => $model,
            ]);
        } else {
            return $this->redirect('/admin/site/testingoperation');
        }
    }

    public function actionUpdatequestion()
    {  
        if (Yii::$app->user->can('updateQuestion')) {
            if (Yii::$app->user->isGuest) {
                return $this->goHome();
            }

            $model = new QuestionForm();
            if ($model->load(Yii::$app->request->post(),'')) {
                if ($question = $model->updateQuestion()) {
                    return $this->redirect('/admin/site/testingoperation?list_id=' . $question->list_id);
                }
            } 

            $questionIds = [Yii::$app->request->post('id')];
            if (!$questionIds[0]) {
                $questionIds = preg_replace('/^:|:$/', '', Yii::$app->request->post('question_ids'));
                $questionIds = explode('::', $questionIds);
            }

            $model = Question::findById($questionIds[0]);
            if (Yii::$app->request->isAjax) {
                return $this->renderAjax('update_question', [
                    'model' => $model,
                ]);
            }

            return $this->render('update_question', [
                'model' => $model,
            ]);
        } else {
            return $this->redirect('/admin/site/testingoperation');
        }
    }

    public function actionDeletequestion()
    {   
        if (Yii::$app->user->can('deleteQuestion')) {
            if (Yii::$app->user->isGuest) {
                return $this->goHome();
            }

            if (Yii::$app->request->isPost && Yii::$app->request->isAjax) {
                if (Yii::$app->request->post('question_ids')) {
                    $questionIds = Yii::$app->request->post('question_ids');
                    $questionIds = preg_replace('/^:|:$/', '', $questionIds);
                    $questionIds = explode('::', $questionIds);
                    if (count($questionIds) > 1) {
                        foreach ($questionIds as $questionId) {
                            Question::findOne($questionId)->delete();
                        }
                    } else if (count($questionIds) == 1) {
                        Question::findOne($questionIds[0])->delete();
                    }  
                    return true;           
                }
            }
        }

        return $this->goHome();
    }

    public function actionAddquestionlist()
    {        
        if (Yii::$app->user->can('createQuestionsList')) {   
            if (Yii::$app->user->isGuest) {
                return $this->goHome();
            }

            $model = new QuestionListForm(); 
            if ($model->load(Yii::$app->request->post(),'')) {
                if ($model->addQuestionList()) {
                    return $this->redirect('/admin/site/testingoperation?questions=true');    
                }
            } 

            if (Yii::$app->request->isAjax) {
                return $this->renderAjax('question_list', [
                    'model' => $model
                ]);
            }

            return $this->render('question_list', [
                'model' => $model,
            ]);
        } else {
            return $this->redirect('/admin/site/testingoperation');
        }        
    }

    public function actionUpdatequestionlist()
    {  
        if (Yii::$app->user->can('updateQuestionsList')) {
            if (Yii::$app->user->isGuest) {
                return $this->goHome();
            }

            $model = new QuestionListForm();
            if ($model->load(Yii::$app->request->post(),'')) {
                if ($questionList = $model->updateQuestionList()) {
                    return $this->redirect('/admin/site/testingoperation?list_id=' . $questionList->id);
                }
            } 

            $questionListIds = [Yii::$app->request->post('id')];
            if (!$questionListIds[0]) {
                $questionListIds = preg_replace('/^:|:$/', '', Yii::$app->request->post('question_list_ids'));
                $questionListIds = explode('::', $questionListIds);
            }

            $model = QuestionList::findById($questionListIds[0]);
            if (Yii::$app->request->isAjax) {
                return $this->renderAjax('update_question_list', [
                    'model' => $model,
                ]);
            }

            return $this->render('update_question_list', [
                'model' => $model,
            ]);
        } else {
            return $this->redirect('/admin/site/testingoperation');
        }
    }

    public function actionOutputquestionlist()
    {   
        if (Yii::$app->user->can('outputQuestionsList')) {
            if (Yii::$app->user->isGuest) {
                return $this->goHome();
            }

            $questionListIds = [Yii::$app->request->post('id')];
            if (!$questionListIds[0]) {
                $questionListIds = preg_replace('/^:|:$/', '', Yii::$app->request->post('question_list_ids'));
                $questionListIds = explode('::', $questionListIds);
            }

            $questionsToList = Question::find()
                        ->where(['list_id' => $questionListIds[0]])
                        ->all();

            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return $questionsToList;
        } else {
            return null;
        }
    }

    public function actionDeletequestionlist()
    {   
        if (Yii::$app->user->can('deleteQuestionsList')) {
            if (Yii::$app->user->isGuest) {
                return $this->goHome();
            }

            if (Yii::$app->request->isPost && Yii::$app->request->isAjax) {
                if (Yii::$app->request->post('question_list_ids')) {
                    $questionListIds = Yii::$app->request->post('question_list_ids');
                    $questionListIds = preg_replace('/^:|:$/', '', $questionListIds);
                    $questionListIds = explode('::', $questionListIds);
                    if (count($questionListIds) > 1) {
                        foreach ($questionListIds as $questionListId) {
                            $questionList = QuestionList::findOne($questionListId);
                            if ($questionList->delete()) {
                                $questionsToList = Question::find()->where(['list_id' => $questionListId])->all();
                                foreach ($questionsToList as $questionToList) {
                                    $questionToList->delete();
                                }
                            }
                        }
                    } else if (count($questionListIds) == 1) {
                        $questionsToList = Question::find()->where(['list_id' => $questionListIds[0]])->all();
                        if (QuestionList::findOne($questionListIds[0])->delete()) {
                            foreach ($questionsToList as $questionToList) {
                                $questionToList->delete();
                            }
                        }
                    }  
                    return true;           
                }
            }
        }

        return $this->goHome();
    }

    public function actionAddcandidate()
    {   
        if (Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new CandidateForm();
        if ($model->load(Yii::$app->request->post(),'')) {
            if ($model->addCandidate()) {
                return $this->redirect('/admin/site/testingoperation?candidates=true');
            }
        } 
        $questionLists = QuestionList::find()->where(['author_id' => Yii::$app->user->identity->id])->all();
        $questionListsDataToForm = [];
        foreach ($questionLists as $questionList) {
            $questionListsDataToForm[$questionList->id] = $questionList->title;
        }

        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('candidate', [
                'model' => $model,
                'questionLists' => $questionListsDataToForm
            ]);
        }

        return $this->render('candidate', [
            'model' => $model,
            'questionLists' => $questionListsDataToForm
        ]);
    }

    public function actionUpdatecandidate()
    {  
        if (Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new CandidateForm();

        if ($model->load(Yii::$app->request->post(),'')) {
            if ($model->updateCandidate()) {
                return $this->redirect('/admin/site/testingoperation?candidates=true');
            }
        } 

        $candidateIds = [Yii::$app->request->post('id')];
        if (!$candidateIds[0]) {
            $candidateIds = preg_replace('/^:|:$/', '', Yii::$app->request->post('candidate_ids'));
            $candidateIds = explode('::', $candidateIds);
        }

        $questionLists = QuestionList::find()->where(['author_id' => Yii::$app->user->identity->id])->all();
        $questionListsDataToForm = [];
        foreach ($questionLists as $questionList) {
            $questionListsDataToForm[$questionList->id] = $questionList->title;
        }

        $model = Candidate::findById($candidateIds[0]);
        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('update_candidate', [
                'model' => $model,
                'questionLists' => $questionListsDataToForm
            ]);
        }

        return $this->render('update_candidate', [
            'model' => $model,
            'questionLists' => $questionListsDataToForm
        ]);
    }

    public function actionDeletecandidate()
    {   
        if (Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        if (Yii::$app->request->isPost && Yii::$app->request->isAjax) {
            if (Yii::$app->request->post('candidate_ids')) {
                $candidateIds = Yii::$app->request->post('candidate_ids');
                $candidateIds = preg_replace('/^:|:$/', '', $candidateIds);
                $candidateIds = explode('::', $candidateIds);
                if (count($candidateIds) > 1) {
                    foreach ($candidateIds as $candidateId) {
                        Candidate::findOne($candidateId)->delete(); 
                    }
                } else if (count($candidateIds) == 1) {
                    Candidate::findOne($candidateIds[0])->delete();
                }  
                return true;           
            }
        }

        return $this->goHome();
    }

    public function actionInvitecandidate()
    {  
        $candidateIds = Yii::$app->request->post('candidate_ids');
        $candidateIds = preg_replace('/^:|:$/', '', $candidateIds);
        $candidateIds = explode('::', $candidateIds);
        $invitingCandidate = [];
        foreach ($candidateIds as $candidateId) {
            $candidate = Candidate::findOne($candidateId); 
            if ($candidate && !$candidate->invited) {
                $questionsToList = Question::find()->where(['list_id' => $candidate->list_id])->all();
                if (count($questionsToList)) {
                    $result = Yii::$app->mailer->compose(['html' => 'candidateInvite-html'], ['candidate_id' => $candidateId])
                        ->setFrom(Yii::$app->user->identity->email)
                        ->setTo($candidate->email)
                        ->setSubject('lime-prime')
                        ->send();

                    if ($result) {
                        $candidate->invited = true;
                        date_default_timezone_set('Europe/Kiev');
                        $candidate->invited_date = date("d-m-Y H:i:s");
                        $candidate->save();
                        $invitingCandidates[$candidateId] = $candidate->name;
                    }
                }
            }
        }

        if (count($invitingCandidates)) {
            return json_encode($invitingCandidates);
        }

        return false;
    }

    public function actionUploadvideo()
    {
        if (isset($_FILES['video'])) {
            $fileName = 'video_of_testing_candidate' . Yii::$app->request->post('candidate_id') . '.webm';
            $uploadDirectory = Yii::getAlias('@app') . '/web/uploads/' . $fileName;
            if (!move_uploaded_file($_FILES['video']['tmp_name'], $uploadDirectory)) {
                echo('Couldn\'t upload video !');
            } else {
                $candidate = Candidate::findOne(Yii::$app->request->post('candidate_id'));
                $candidate->video_url = Yii::$app->getUrlManager()->getBaseUrl() . '/uploads' . '/' . $fileName;
                $candidate->save();
            }
        } else {
            echo 'No file uploaded';
        }
    }

    public function actionSetcandidatetested()
    {
        $candidate = Candidate::findOne(Yii::$app->request->post('candidate_id'));
        if (!$candidate->save()) {
            return false;
        }
        return true;
    }
}
