<?php
namespace backend\models;

use Yii;
use yii\base\Model;
use frontend\models\Resume;
use yii\web\UploadedFile;
use frontend\models\TravelPassportForm;
use frontend\models\SeamanBookForm;
use frontend\models\UsVisaForm;
use frontend\models\OtherVisasForm;
use frontend\models\OtherSeamanbookForm;
use frontend\models\OtherOtherSeamanbookForm;
use frontend\models\PetroleumForm;
use frontend\models\ChemicalForm;
use frontend\models\GasForm;
use frontend\models\CertificateForm;
use frontend\models\CertificateInternationalForm;
use frontend\models\CertificateLiberianForm;
use frontend\models\CertificateNorwegianForm;
use frontend\models\CertificatePanamanianForm;
use frontend\models\CertificateYellowFeverForm;
use frontend\models\CertificateHealthListForm;
use frontend\models\CertificateDrugTestForm;
use frontend\models\TrainingInstructionsForm;
use frontend\models\BasicFireFightingForm;
use frontend\models\AdvFireFightingForm;
use frontend\models\ElementaryFirstAidForm;
use frontend\models\MedicalFirstAidForm;
use frontend\models\MedicalCareForm;
use frontend\models\PersSafetyRespForm;
use frontend\models\CraftRescueForm;
use frontend\models\FastRescueCraftForm;
use frontend\models\GmdsssForm;
use frontend\models\ManagementLevelForm;
use frontend\models\EcdisForm;
use frontend\models\RadarObservationForm;
use frontend\models\HazmatForm;
use frontend\models\OilTankerForm;
use frontend\models\AdvanceOilTankerForm;
use frontend\models\ChemicalTankerForm;
use frontend\models\GasTankerForm;
use frontend\models\AdvanceGasTankerForm;
use frontend\models\CrudeOilWashingForm;
use frontend\models\InertGasPlantForm;
use frontend\models\IsmCodeForm;
use frontend\models\ShipSecurityOfficerForm;
use frontend\models\ShipSafetyOfficerForm;
use frontend\models\BridgeTeamManagementForm;
use frontend\models\DpInductionForm;
use frontend\models\DpSimulatorForm;
use frontend\models\BridgeEngineRoomResourceManagementForm;
use frontend\models\ShipHandlingForm;
use frontend\models\InternalAuditorsCourseForm;
use frontend\models\TrainingForSeafarersForm;
use frontend\models\SecurityAwarenessForm;
use frontend\models\ElecticalElectronicForm;
use frontend\models\NewBuildingForm;
use frontend\models\SpecialisedProjectsForm;
use frontend\models\SpecialTradesForm;
use frontend\models\ShoreExperienceForm;
use frontend\models\ServiceDetailsForm;
use frontend\models\ReferenceDetailsForm;

class ResumeForm extends Model
{
    public $id;
    public $image;
    public $created_at;
    public $salary;
    public $application_for_position;
    public $other_position;
    public $surname;
    public $firstname;
    public $other_names;
    public $sex;
    public $date_of_birth;
    public $place_of_birth;
    public $citizenship;
    public $marital_status;
    public $color_of_eyes;
    public $color_of_hair;
    public $height;
    public $weight;
    public $boilersuit_size;
    public $boots_size;
    public $language;
    public $level;
    public $country;
    public $city;
    public $post_code;
    public $mobile;
    public $email;
    public $skype_name;
    public $telegram;
    public $viber;
    public $whatsapp;
    public $next_of_kin;
    public $kin_adress;
    public $kin_mobile;
    public $school;
    public $school_from;
    public $school_to;

    public function rules()
    {
        return [
            ['id', 'each', 'rule' => ['integer']],
       
            //[['image'], 'file', 'skipOnEmpty' => false, 'extensions' => 'png, jpg'],

            ['created_at', 'string'],
            
            ['salary', 'integer'],
            
            ['application_for_position', 'string'],
            ['application_for_position', 'required'],
            ['other_position', 'string'],

            ['surname', 'string'],
            ['surname', 'required'],
            ['firstname', 'string'],
            ['firstname', 'required'],
            ['other_names', 'string'],
            ['other_names', 'required'],
            ['sex', 'string'],
            ['sex', 'required'],
            ['date_of_birth', 'string'],
            ['date_of_birth', 'required'],
            ['place_of_birth', 'string'],
            ['place_of_birth', 'required'],
            ['citizenship', 'string'],
            ['citizenship', 'required'],
            ['marital_status', 'string'],

            ['color_of_eyes', 'string'],
            
            ['color_of_hair', 'string'],
            
            ['height', 'integer'],
            
            ['weight', 'integer'],
            
            ['boilersuit_size', 'integer'],
            
            ['boots_size', 'integer'],
            
            ['language', 'each', 'rule' => ['string']],
            ['language', 'required'],
            ['level', 'each', 'rule' => ['string']],
            ['level', 'required'],
            ['country', 'string'],
            
            ['city', 'string'],
            
            ['post_code', 'string'],
            
            ['mobile', 'string'],
            ['mobile', 'required'],
            ['email', 'each', 'rule' => ['email']],
            ['email', 'required'],
            ['skype_name', 'string'],
            ['skype_name', 'required'],

            ['telegram', 'boolean'],
            
            ['viber', 'boolean'],
            
            ['whatsapp', 'boolean'],
            
            ['next_of_kin', 'string'],

            ['kin_adress', 'string'],

            ['kin_mobile', 'string'],

            ['school', 'string'],
            
            ['school_from', 'string'],
            
            ['school_to', 'string'],
        ];
    }

    public function updateResume($otherData)
    {                    
                $resume = Resume::findById($this->id[0]);
                $resume->created_at = $this->created_at;
                $resume->salary = $this->salary;
                $resume->application_for_position = $this->application_for_position;

                $resume->other_position = $this->other_position;
                $resume->surname = $this->surname;
                $resume->firstname = $this->firstname;
                $resume->other_names = $this->other_names;
                if ($this->sex == '0') {
                    $resume->sex = 'Male'; 
                } else {
                    $resume->sex = $this->sex;
                }
                $resume->date_of_birth = $this->date_of_birth;
                $resume->place_of_birth = $this->place_of_birth;
                $resume->citizenship = $this->citizenship;
                $resume->marital_status = $this->marital_status;
                $resume->color_of_eyes = $this->color_of_eyes;
                $resume->color_of_hair = $this->color_of_hair;
                $resume->height = $this->height;
                $resume->weight = $this->weight;
                $resume->boilersuit_size = $this->boilersuit_size;
                $resume->boots_size = $this->boots_size;
                if ($this->language && $this->level) {
                    $resume->language = '';
                    $resume->level = '';
                    foreach ($this->language as $value) {
                        $resume->language .= ':' . $value . ':';
                    }
                    foreach ($this->level as $value) {
                        $resume->level .= ':' . $value . ':';
                    }
                }
                $resume->country = $this->country;
                $resume->city = $this->city;
                $resume->post_code = $this->post_code;
                $resume->mobile = $this->mobile;
                $resume->email = $this->email[0];
                $resume->skype_name = $this->skype_name;
                $resume->telegram = $this->telegram;
                $resume->viber = $this->viber;
                $resume->whatsapp = $this->whatsapp;
                $resume->next_of_kin = $this->next_of_kin;
                $resume->kin_adress = $this->kin_adress;
                $resume->kin_mobile = $this->kin_mobile;
                $resume->school = $this->school;
                $resume->school_from = $this->school_from;
                $resume->school_to = $this->school_to;

                $image = UploadedFile::getInstance($resume, 'image');
                if ($image) {
                    $fileName = $resume->id . '.' . $image->extension;
                    $filePath = Yii::getAlias('@frontend') . '/web/uploads/' . $fileName;
                    $image->saveAs($filePath);
                    $resume->image = $fileName;     
                }
                
                $travelpassport = new TravelPassportForm();
                if ($travelpassport->load(Yii::$app->request->post(), '') && $travelpassport->validate()) {
                    $travelpassport->updateTravelpassport();
                }

                $seamanbook = new SeamanBookForm();
                if ($seamanbook->load(Yii::$app->request->post(), '') && $seamanbook->validate()) {
                    $seamanbook->updateSeamanbook();
                }
                
                $usvisa = new UsVisaForm();
                if ($usvisa->load(Yii::$app->request->post(), '') && $usvisa->validate()) {
                    $usvisa->updateUsvisa();
                }

                $othervisas = new OtherVisasForm();
                if ($othervisas->load(Yii::$app->request->post(), '') && $othervisas->validate()) {
                    $othervisas->updateOthervisas();
                }

                $issueOtherSeamanbooks = preg_replace('/^:|:$/', '', $resume->other_seaman_book_ids);
                $issueOtherSeamanbooks = explode('::', $issueOtherSeamanbooks);
                $quantityIssueOtherSeamanbooks = count($issueOtherSeamanbooks);
                $quantityOtherSeamanbooks = count(Yii::$app->request->post()['other_otherseamanbook_id']);
                for ($i = 0; $i < $quantityOtherSeamanbooks + 1; $i++) {
                    if (!$quantityIssueOtherSeamanbooks) {
                        if ($i == 0) {
                            $otherSeamanbook = new OtherSeamanBookForm();
                            if ($otherSeamanbook->load(Yii::$app->request->post(), '') && $otherSeamanbook->validate()) {
                                if ($newOtherseamanbook = $otherSeamanbook->addOtherseamanbook()) {
                                    $resume->other_seaman_book_ids .= ':' . $newOtherseamanbook->id . ':';
                                }  
                            }
                        } else {
                            $otherSeamanbook = new OtherOtherSeamanBookForm();
                            if ($otherSeamanbook->load(Yii::$app->request->post(), '') && $otherSeamanbook->validate()) {
                                $dataCertificateId = Yii::$app->request->post()['other_otherseamanbook_id'][$i - 1];
                                if ($newOtherseamanbook = $otherSeamanbook->addOtherseamanbook($i - 1)) {
                                    $resume->other_seaman_book_ids .= ':' . $newOtherseamanbook->id . ':';
                                }  
                            }
                        }
                    } else {
                        if ($i == 0) {
                            $otherSeamanbook = new OtherSeamanBookForm();
                            if ($otherSeamanbook->load(Yii::$app->request->post(), '') && $otherSeamanbook->validate()) {
                                if ($newOtherseamanbook = $otherSeamanbook->updateOtherseamanbook()) {

                                }  
                            }
                        } else {
                            if ($quantityOtherSeamanbooks > $quantityIssueOtherSeamanbooks - 1) {
                                $firstIndexNewOtherSeamanbook = $quantityIssueOtherSeamanbooks;
                                if ($firstIndexNewOtherSeamanbook <= $i) {
                                    $otherSeamanbook = new OtherOtherSeamanBookForm();
                                    if ($otherSeamanbook->load(Yii::$app->request->post(), '') && $otherSeamanbook->validate()) {
                                        $dataCertificateId = Yii::$app->request->post()['other_otherseamanbook_id'][$i - 1];
                                        if ($newOtherseamanbook = $otherSeamanbook->addOtherseamanbook($i - 1)) {
                                            $resume->other_seaman_book_ids .= ':' . $newOtherseamanbook->id . ':';
                                        }  
                                    }
                                } else {
                                    $otherSeamanbook = new OtherOtherSeamanBookForm();
                                    if ($otherSeamanbook->load(Yii::$app->request->post(), '') && $otherSeamanbook->validate()) {
                                        $dataCertificateId = Yii::$app->request->post()['other_otherseamanbook_id'][$i - 1];
                                        if ($newOtherseamanbook = $otherSeamanbook->updateOtherseamanbook($i - 1, $dataCertificateId)) {

                                        }  
                                    }
                                }
                            } else {
                                $otherSeamanbook = new OtherOtherSeamanBookForm();
                                if ($otherSeamanbook->load(Yii::$app->request->post(), '') && $otherSeamanbook->validate()) {
                                    $dataCertificateId = Yii::$app->request->post()['other_otherseamanbook_id'][$i - 1];
                                    if ($newOtherseamanbook = $otherSeamanbook->updateOtherseamanbook($i - 1, $dataCertificateId)) {

                                    }  
                                }
                            }
                        }
                    }  
                }

                $petroleum = new PetroleumForm();
                if ($petroleum->load(Yii::$app->request->post(), '') && $petroleum->validate()) {
                    $petroleum->updatePetroleum();
                }

                $chemical = new ChemicalForm();
                if ($chemical->load(Yii::$app->request->post(), '') && $chemical->validate()) {
                    $chemical->updateChemical();
                }

                $gas = new GasForm();
                if ($gas->load(Yii::$app->request->post(), '') && $gas->validate()) {
                    $gas->updateGas();
                }

                $certificate = new CertificateForm();
                if ($certificate->load(Yii::$app->request->post(), '') && $certificate->validate()) {
                    $certificate->updateCertificate();
                }

                $issueOtherCertificates = preg_replace('/^:|:$/', '', $resume->certificate_ids);
                if ($issueOtherCertificates) {
                    $issueOtherCertificates = explode('::', $issueOtherCertificates);
                    $quantityIssueOtherCertificates = count($issueOtherCertificates);
                } else {
                    $quantityIssueOtherCertificates = 0;
                }   
                $quantityOtherCertificates = count(Yii::$app->request->post()['other_certificate_name']);
                for ($i = 0; $i < $quantityOtherCertificates; $i++) {
                    $otherCertificate = new OtherCertificateForm();
                    if ($otherCertificate->load(Yii::$app->request->post(), '') && $otherCertificate->validate()) {
                        if ($quantityOtherCertificates > $quantityIssueOtherCertificates) {
                            $firstIndexNewOtherCertificate = $quantityIssueOtherCertificates;
                            if ($firstIndexNewOtherCertificate <= $i) {
                                if ($newOtherCertificate = $otherCertificate->addOthercertificate($i)) {
                                    $resume->certificate_ids .= ':' . $newOtherCertificate->id . ':';
                                } 
                            } else {
                                $dataCertificateId = Yii::$app->request->post()['other_certificate_id'][$i];
                                $otherCertificate->updateOthercertificate($i, $dataCertificateId);
                            }
                        } else if (!$quantityIssueOtherCertificates) {
                            $otherCertificate= new Certificate();
                            if ($newOtherCertificate = $otherCertificate->addOthercertificate()) {
                                $resume->certificate_ids .= ':' . $newOtherCertificate->id . ':';
                            }    
                        } else if ($quantityOtherCertificates) {
                            $dataCertificateId = Yii::$app->request->post()['other_certificate_id'][$i];
                            $otherCertificate->updateOthercertificate($i, $dataCertificateId);
                        }
                    }
                }
                
                $certificateinternational = new CertificateInternationalForm();
                if ($certificateinternational->load(Yii::$app->request->post(), '') && $certificateinternational->validate()) {         
                    $certificateinternational->updateCertificateinternational();
                }

                $certificateliberian = new CertificateLiberianForm();
                if ($certificateliberian->load(Yii::$app->request->post(), '') && $certificateliberian->validate()) {
                    $certificateliberian->updateCertificateliberian();
                }

                $certificatenorwegian = new CertificateNorwegianForm();
                if ($certificatenorwegian->load(Yii::$app->request->post(), '') && $certificatenorwegian->validate()) {
                    $certificatenorwegian->updateCertificatenorwegian();
                }

                $certificatepanamanian = new CertificatePanamanianForm();
                if ($certificatepanamanian->load(Yii::$app->request->post(), '') && $certificatepanamanian->validate()) {
                    $certificatepanamanian->updateCertificatepanamanian(); 
                }

                $certificateyellowfever = new CertificateYellowFeverForm();
                if ($certificateyellowfever->load(Yii::$app->request->post(), '') && $certificateyellowfever->validate()) {
                    $certificateyellowfever->updateCertificateyellowfever(); 
                }

                $certificatehealthlis = new CertificateHealthListForm();
                if ($certificatehealthlis->load(Yii::$app->request->post(), '') && $certificatehealthlis->validate()) {
                    $certificatehealthlis->updateCertificatehealthlist();
                }

                $certificatedrugtest = new CertificateDrugTestForm();
                if ($certificatedrugtest->load(Yii::$app->request->post(), '') && $certificatedrugtest->validate()) {
                    $certificatedrugtest->updateCertificatedrugtest(); 
                }

                $traininginstructions = new TrainingInstructionsForm();
                if ($traininginstructions->load(Yii::$app->request->post(), '') && $traininginstructions->validate()) {
                    $traininginstructions->updateTraininginstructions(); 
                }

                $basicfirefighting = new BasicFireFightingForm();
                if ($basicfirefighting->load(Yii::$app->request->post(), '') && $basicfirefighting->validate()) {
                    $basicfirefighting->updateBasicfirefighting();
                }

                $advfirefighting = new AdvFireFightingForm();
                if ($advfirefighting->load(Yii::$app->request->post(), '') && $advfirefighting->validate()) {
                    $advfirefighting->updateAdvfirefighting();
                }

                $elementaryfirstaid = new ElementaryFirstAidForm();
                if ($elementaryfirstaid->load(Yii::$app->request->post(), '') && $elementaryfirstaid->validate()) {
                    $elementaryfirstaid->updateElementaryfirstaid(); 
                }

                $medicalfirstaid = new MedicalFirstAidForm();
                if ($medicalfirstaid->load(Yii::$app->request->post(), '') && $medicalfirstaid->validate()) {
                    $medicalfirstaid->updateMedicalfirstaid();
                }
                $medicalcare = new MedicalCareForm();
                if ($medicalcare->load(Yii::$app->request->post(), '') && $medicalcare->validate()) {
                    $medicalcare->updateMedicalcare();
                }

                $perssafetyresp = new PersSafetyRespForm();
                if ($perssafetyresp->load(Yii::$app->request->post(), '') && $perssafetyresp->validate()) {
                    $perssafetyresp->updatePerssafetyresp(); 
                }

                $craftrescue = new CraftRescueForm();
                if ($craftrescue->load(Yii::$app->request->post(), '') && $craftrescue->validate()) {
                    $craftrescue->updateCraftrescue();
                }

                $fastrescuecraft = new FastRescueCraftForm();
                if ($fastrescuecraft->load(Yii::$app->request->post(), '') && $fastrescuecraft->validate()) {
                    $fastrescuecraft->updateFastrescuecraft();
                }

                $gmdsss = new GmdsssForm();
                if ($gmdsss->load(Yii::$app->request->post(), '') && $gmdsss->validate()) {
                    $gmdsss->updateGmdsss(); 
                }

                $managementlevel = new ManagementLevelForm();
                if ($managementlevel->load(Yii::$app->request->post(), '') && $managementlevel->validate()) {
                    $managementlevel->updateManagementlevel();
                }

                $ecdis = new EcdisForm();
                if ($ecdis->load(Yii::$app->request->post(), '') && $ecdis->validate()) {
                    $ecdis->updateEcdis();
                }

                $radarobservation = new RadarObservationForm();
                if ($radarobservation->load(Yii::$app->request->post(), '') && $radarobservation->validate()) {
                    $radarobservation->updateRadarobservation(); 
                }

                $hazmat = new HazmatForm();
                if ($hazmat->load(Yii::$app->request->post(), '') && $hazmat->validate()) {
                    $hazmat->updateHazmat();
                }

                $oiltanker = new OilTankerForm();
                if ($oiltanker->load(Yii::$app->request->post(), '') && $oiltanker->validate()) {
                    $oiltanker->updateOiltanker(); 
                }

                $advanceoiltanker = new AdvanceOilTankerForm();
                if ($advanceoiltanker->load(Yii::$app->request->post(), '') && $advanceoiltanker->validate()) {
                    $advanceoiltanker->updateAdvanceoiltanker();
                }

                $chemicaltanker = new ChemicalTankerForm();
                if ($chemicaltanker->load(Yii::$app->request->post(), '') && $chemicaltanker->validate()) {
                    $chemicaltanker->updateChemicaltanker(); 
                }

                $gastanker = new GasTankerForm();
                if ($gastanker->load(Yii::$app->request->post(), '') && $gastanker->validate()) {
                    $gastanker->updateGastanker(); 
                }

                $advancegastanker = new AdvanceGasTankerForm();
                if ($advancegastanker->load(Yii::$app->request->post(), '') && $advancegastanker->validate()) {
                    $advancegastanker->updateAdvancegastanker();
                }

                $crudeoilwashing = new CrudeOilWashingForm();
                if ($crudeoilwashing->load(Yii::$app->request->post(), '') && $crudeoilwashing->validate()) {
                    $crudeoilwashing->updateCrudeoilwashing(); 
                }

                $inertgasplant = new InertGasPlantForm();
                if ($inertgasplant->load(Yii::$app->request->post(), '') && $inertgasplant->validate()) {
                    $inertgasplant->updateInertgasplant();
                }

                $ismcode = new IsmCodeForm();
                if ($ismcode->load(Yii::$app->request->post(), '') && $ismcode->validate()) {
                    $ismcode->updateIsmcode(); 
                }

                $shipsecurityofficer = new ShipSecurityOfficerForm();
                if ($shipsecurityofficer->load(Yii::$app->request->post(), '') && $shipsecurityofficer->validate()) {
                    $shipsecurityofficer->updateShipsecurityofficer(); 
                }

                $shipsafetyofficer = new ShipSafetyOfficerForm();
                if ($shipsafetyofficer->load(Yii::$app->request->post(), '') && $shipsafetyofficer->validate()) {
                    $shipsafetyofficer->updateShipsafetyofficer();
                }

                $bridgeteammanagement = new BridgeTeamManagementForm();
                if ($bridgeteammanagement->load(Yii::$app->request->post(), '') && $bridgeteammanagement->validate()) {
                    $bridgeteammanagement->updateBridgeteammanagement(); 
                }

                $dpinduction = new DpInductionForm();
                if ($dpinduction->load(Yii::$app->request->post(), '') && $dpinduction->validate()) {
                    $dpinduction->updateDpinduction(); 
                }

                $dpsimulator = new DpSimulatorForm();
                if ($dpsimulator->load(Yii::$app->request->post(), '') && $dpsimulator->validate()) {
                    $dpsimulator->updateDpsimulator();
                }

                $bridgeengineroomresourcemanagement = new BridgeEngineRoomResourceManagementForm();
                if ($bridgeengineroomresourcemanagement->load(Yii::$app->request->post(), '') && $bridgeengineroomresourcemanagement->validate()) {
                    $bridgeengineroomresourcemanagement->updateBridgeengineroomresourcemanagement();
                }

                $shiphandling = new ShipHandlingForm();
                if ($shiphandling->load(Yii::$app->request->post(), '') && $shiphandling->validate()) {
                    $shiphandling->updateShiphandling();
                }

                $internalauditorscourse = new InternalAuditorsCourseForm();
                if ($internalauditorscourse->load(Yii::$app->request->post(), '') && $internalauditorscourse->validate()) {
                    $internalauditorscourse->updateInternalauditorscourse();
                }

                $trainingforseafarers = new TrainingForSeafarersForm();
                if ($trainingforseafarers->load(Yii::$app->request->post(), '') && $trainingforseafarers->validate()) {
                    $trainingforseafarers->updateTrainingforseafarers(); 
                }

                $securityawareness = new SecurityAwarenessForm();
                if ($securityawareness->load(Yii::$app->request->post(), '') && $securityawareness->validate()) {
                    $securityawareness->updateSecurityawareness();
                }

                $electicalelectronic = new ElecticalElectronicForm();
                if ($electicalelectronic->load(Yii::$app->request->post(), '') && $electicalelectronic->validate()) {
                    $electicalelectronic->updateElecticalelectronic();
                }


                $issueOtherCourses = preg_replace('/^:|:$/', '', $resume->other_course_ids);
                if ($issueOtherCourses) {
                    $issueOtherCourses = explode('::', $issueOtherCourses);
                    $quantityIssueOtherCourses = count($issueOtherCourses);
                } else {
                    $quantityIssueOtherCourses = 0;
                }   
                $quantityOtherCourses = count(Yii::$app->request->post()['other_course_number']);
                for ($i = 0; $i < $quantityOtherCourses; $i++) {
                    $otherCourse = new OtherCourseForm();
                    if ($otherCourse->load(Yii::$app->request->post(), '') && $otherCourse->validate()) {
                        if ($quantityOtherCourses > $quantityIssueOtherCourses) {
                            $firstIndexNewOtherCourse = $quantityIssueOtherCourses;
                            if ($firstIndexNewOtherCourse <= $i) {
                                if ($newOtherCourse = $otherCourse->addOthercourse($i)) {
                                    $resume->other_course_ids .= ':' . $newOtherCourse->id . ':';
                                } 
                            } else {
                                $dataCourseId = Yii::$app->request->post()['other_course_id'][$i];
                                $otherCourse->updateOthercourse($i, $dataCourseId);
                            }
                        } else if (!$quantityIssueOtherCourses) {
                            $otherCourse= new OtherCourseForm();
                            if ($newOtherCourse = $otherCourse->addOthercourse()) {
                                $resume->other_course_ids .= ':' . $newOtherCourse->id . ':';
                            }    
                        } else if ($quantityOtherCourses) {
                            $dataCourseId = Yii::$app->request->post()['other_course_id'][$i];
                            $otherCourse->updateOthercourse($i, $dataCourseId);
                        }
                    }
                }

                $newbuilding = new NewBuildingForm();
                if ($newbuilding->load(Yii::$app->request->post(), '') && $newbuilding->validate()) {
                    $newbuilding->updateNewbuilding();
                }

                $specialisedprojects = new SpecialisedProjectsForm();
                if ($specialisedprojects->load(Yii::$app->request->post(), '') && $specialisedprojects->validate()) {
                    $specialisedprojects->updateSpecialisedprojects();
                }

                $specialtrades = new SpecialTradesForm();
                if ($specialtrades->load(Yii::$app->request->post(), '') && $specialtrades->validate()) {
                    $specialtrades->updateSpecialtrades();
                }

                $shoreexperience = new ShoreExperienceForm();
                if ($shoreexperience->load(Yii::$app->request->post(), '') && $shoreexperience->validate()) {
                    $shoreexperience->updateShoreexperience();
                }

                $issueServiceDetails = preg_replace('/^:|:$/', '', $resume->service_details_ids);
                if ($issueServiceDetails) {
                    $issueServiceDetails = explode('::', $issueServiceDetails);
                    $quantityIssueServiceDetails = count($issueServiceDetails);
                } else {
                    $quantityIssueServiceDetails = 0;
                }
                $quantityServiceDetails = count(Yii::$app->request->post()['service_details_company_name']);
                for ($i = 0; $i < $quantityServiceDetails; $i++) {
                    $serviceDetails = new ServiceDetailsForm();
                    if ($serviceDetails->load(Yii::$app->request->post(), '') && $serviceDetails->validate()) {
                        if ($quantityServiceDetails > $quantityIssueServiceDetails) {
                            $firstIndexNewServiceDetails = $quantityIssueServiceDetails;
                            if ($firstIndexNewServiceDetails <= $i) {
                                if ($newServiceDetails = $serviceDetails->addServicedetails($i)) {
                                    $resume->service_details_ids .= ':' . $newServiceDetails->id . ':';
                                } 
                            } else {
                                $dataDetailsId = Yii::$app->request->post()['service_details_id'][$i];
                                $serviceDetails->updateServicedetails($i, $dataDetailsId);
                            }
                        } else if (!$quantityIssueServiceDetails) {
                            $serviceDetails = new ServiceDetails();
                            if ($newServiceDetails = $serviceDetail->addServicedetails()) {
                                $resume->service_details_ids .= ':' . $newServiceDetails->id . ':';
                            }    
                        } else if ($quantityServiceDetails) {
                            $dataDetailsId = Yii::$app->request->post()['service_details_id'][$i];
                            $serviceDetails->updateServicedetails($i, $dataDetailsId);
                        }
                    }
                }

                $issueReferenceDetails = preg_replace('/^:|:$/', '', $resume->reference_details_ids);
                if ($issueReferenceDetails) {
                    $issueReferenceDetails = explode('::', $issueReferenceDetails);
                    $quantityIssueReferenceDetails = count($issueReferenceDetails);
                } else {
                    $quantityIssueReferenceDetails = 0;
                }
                $quantityReferenceDetails = count(Yii::$app->request->post()['reference_details_company_name']);
                for ($i = 0; $i < $quantityReferenceDetails; $i++) {
                    $referenceDetails = new ReferenceDetailsForm();
                    if ($referenceDetails->load(Yii::$app->request->post(), '') && $referenceDetails->validate()) {
                        if ($quantityReferenceDetails > $quantityIssueReferenceDetails) {
                            $firstIndexNewReferenceDetails = $quantityIssueReferenceDetails;
                            if ($firstIndexNewReferenceDetails <= $i) {
                                if ($newReferenceDetails = $referenceDetails->addReferencedetails($i)) {
                                    $resume->reference_details_ids .= ':' . $newReferenceDetails->id . ':';
                                } 
                            } else {
                                $dataDetailsId = Yii::$app->request->post()['reference_details_id'][$i];
                                $referenceDetails->updateReferencedetails($i, $dataDetailsId);
                            }
                        } else if (!$quantityIssueReferenceDetails) {
                            $referenceDetails = new ReferenceDetails();
                            if ($newReferenceDetails = $referenceDetail->addReferencedetails()) {
                                $resume->reference_details_ids .= ':' . $newReferenceDetails->id . ':';
                            }    
                        } else if ($quantityReferenceDetails) {
                            $dataDetailsId = Yii::$app->request->post()['reference_details_id'][$i];
                            $referenceDetails->updateReferencedetails($i, $dataDetailsId);
                        }
                    }
                }

                if ($resume->save()) {
                    return $resume;
                }

        return null;
    }
}

