<?php
namespace backend\models;

use Yii;
use yii\base\Model;
use frontend\models\Resume;

class ResumeFilterForm extends Model
{
    public $email;
    public $application_for_position;
    public $citizenship;
    public $minAge;
    public $maxAge;
    public $minSalary;
    public $maxSalary;
    public $minDwt;
    public $maxDwt;

    public function rules()
    {
        return [
            ['email', 'email'],

            ['application_for_position', 'string'],
            ['citizenship', 'string'],

            ['minAge', 'integer'],
            ['maxAge', 'integer'],

            ['minSalary', 'integer'],
            ['maxSalary', 'integer'],
            
            ['minDwt', 'integer'],
            ['maxDwt', 'integer'],
        ];
    }
}

