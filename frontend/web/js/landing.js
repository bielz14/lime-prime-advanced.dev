// anchor

/*$(document).ready(function(){
    $("#mynav").on("click","a", function (event) {
        event.preventDefault();
        var id  = $(this).attr('href'),
            top = $(id).offset().top;
        $('body,html').animate({scrollTop: top}, 1500);
    });
});*/

$(document).ready(function(){
    $("#company_offer").on("click","a", function (event) {
        event.preventDefault();
        var id  = $(this).attr('href'),
            top = $(id).offset().top;
        $('body,html').animate({scrollTop: top}, 1500);
      openForm();
    });
});

// Modal

var modal = document.getElementById('video');
window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
}

var modal = document.getElementById('mariners_offer');
window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
}

var modal = document.getElementById('company_offer');
window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
}

var modal = document.getElementById('bot_offer');
window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
}

var modal = document.getElementById('service_offer');
window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
}


// Humburger

function changeHumb() {
    var elem = document.querySelector("#humburger")
    elem.classList.toggle("change");
}
function openNav() {
    setTimeout(function() {document.getElementById("nav-descript").style.right = "0"}, 200);
    setTimeout(function() {document.getElementById("nav-offer").style.right = "0"}, 400);
    setTimeout(function() {document.getElementById("nav-seafarer").style.right = "0"}, 600);
    setTimeout(function() {document.getElementById("nav-companies").style.right = "0"}, 800);
    setTimeout(function() {document.getElementById("nav-contacts").style.right = "0"}, 1000);
    changeHumb();
}
function closeNav() {
    setTimeout(function() {document.getElementById("nav-descript").style.right = "-235px"}, 1000);
    setTimeout(function() {document.getElementById("nav-offer").style.right = "-235px"}, 800);
    setTimeout(function() {document.getElementById("nav-seafarer").style.right = "-235px"}, 600);
    setTimeout(function() {document.getElementById("nav-companies").style.right = "-235px"}, 400);
    setTimeout(function() {document.getElementById("nav-contacts").style.right = "-235px"}, 200);
    changeHumb();
}

$(document).ready(function(){
    $("#mynav").on("click", "a",function(){
        closeNav();
    })
})

function forHumburger() {
    var x = document.getElementById("nav-seafarer");
    if (x.style.right === "0px") {
        closeNav();
    } else {
        openNav();
    }
}

//For Form

function openForm() {
    document.getElementById("application").style.display = "block";
    document.getElementById("open-form").style.display = "none";
}

function closeForm() {
    document.getElementById("application").style.display = "none";
    document.getElementById("open-form").style.display = "block";
}

var btn = document.querySelector('.open-button');
var blockHidden = document.querySelector('.application');

function showBlock() {

  blockHidden.classList.add('b-show');
}
btn.addEventListener('click', showBlock);

// function showSnack() {
//   var x = document.getElementById("snackbar");
//   x.className = "show";
//   setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
// }

var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
    acc[i].addEventListener("click", function() {
        this.classList.toggle("active");
        var panel = this.nextElementSibling;
        if (panel.style.maxHeight){
            panel.style.maxHeight = null;
        } else {
            panel.style.maxHeight = panel.scrollHeight + "px";
        }
    });
}

