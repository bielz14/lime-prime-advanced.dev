//English version

$(function(){
    $('.translate').click(function(){
        var lang = $(this).attr('id');

        $('.lang').each(function(index,element){
            $(this).text(arrLang[lang][$(this).attr('key')]);
        });
    });
});

$(document).ready(function () {
    $("#en").click(function () {
        $("#en").hide();
        $("#ru").show();
    });
    $("#ru").click(function () {
        $("#ru").hide();
        $("#en").show();
    });
 
});

var arrLang ={
    'en':{
        'en_home': 'HOME',
        'en_for_offer':'WE OFFER',
        'en_for_marines':'FOR SEAMEN',
        'en_for_companies':'FOR COMPANIES',
        'en_contacts':'CONTACTS',
        'en_description':'FAST START OF YOUR MARITIME CAREER',
        'en_description_text':'What is CrewMSG',
        'en_watchVideo':'Watch the video',
        'en_description_p':'CrewMSG is a German company with representative office in Ukraine. We provide services to foreign employers and shipowners in the areas of recruiting. We help in the recruitment of qualified sailors from Ukraine and Europe. Our team of the specialists who every day improve knowledges and skills to find new concepts and solutions to keep up with the times and improve quality of the services that we are provide for our partners and company’s clients',
        'en_offer-container':'WHAT WE OFFER',
        'en_offer-box1':'Recruitment for seamen',
        'en_offer-box2':'Cooperation with shipowners and crewing companies',
        'en_offer-box3':'Bots assistance',
        'en_offer-box4':'New service This service is being developed',
        'en_button1':'Contact us',
        'en_button2':'Try it now!',
        'en_button3':'More about us',
        'en_marins_p':'Leave your resume to let our recruiters  offer you a job. Also you can receive mailings with vacancies on e-mail and the in the telegram channel.',
        'en_company_p':'Our company offers high-quality services for the staff  recruitment. We can recruit specialists for work on various types of vessels: dry cargo ships, refrigerated and fishing fleets, tanker fleets, bulk carriers, container ships, offshore fleet vessels, etc.',
        'en_bot_p':'Our IT and educational departments have prepared for you bot which one help you to prepare to all maritime tests',
        'en_service_p':'Our service will allow to do a remote interview with candidates. As a result, you have the opportunity to watch a video in which the candidate answers your questions.',
        'en_sendBtn':'send',
        'en_feedbackBtn':'Feedback',
        'en_snackfeedback':'Thank you! Soon we contact you!'
    },
    'ru':{
        'en_home': 'ОПИСАНИЕ',
        'en_for_offer':'ЧТО МЫ ПРЕДЛАГАЕМ',
        'en_for_marines':'ДЛЯ МОРЯКОВ',
        'en_for_companies':'ДЛЯ КОМПАНИЙ',
        'en_contacts':'КОНТАКТЫ',
        'en_description':'БЫСТРЫЙ СТАРТ ВАШЕЙ КАРЬЕРЫ В МОРЕ',
        'en_description_text':'Что такое CrewMSG',
        'en_watchVideo':'Смотреть видео',
        'en_description_p':'CrewMSG – это Немецкая компания с представительством в Украине, предоставляющая услуги иностранным работодателям и судовладельцам в сферах рекрутинга. Мы помогаем в наборе квалифицированных моряков из Украины и Европы. Наша команда специалистов находится в постоянном поиске новых концепций и решений, позволяющих идти в ногу со временем и повышать качество предоставляемых услуг для партнеров и клиентов компании.',
        'en_offer-container':'ЧТО МЫ ПРЕДЛАГАЕМ',
        'en_offer-box1':'Быстрое трудоустройство моряков',
        'en_offer-box2':'Сотрудничество для крюинговых компаний',
        'en_offer-box3':'Боты помощники',
        'en_offer-box4':'Новый сервис Раздел в разработке',
        'en_button1':'Написать нам',
        'en_button2':'Начать',
        'en_button3':'Узнать больше',
        'en_marins_p':'Оставьте ваше резюме, чтобы рекрутеры смогли предложить вам работу. А также получайте рассылки с подходящими для вас вакансиями на электронную почту и канал в телеграмме.',
        'en_company_p':'Наша компания предлагает качественные услуги по подбору и отправке на суда морских специалистов - офицерского и рядового состава.  Мы подбираем специалистов для работы на различных типах судов, на сухогрузы, рефрижераторный и рыбопромысловый флот, танкерный флот, балкеры, контейнеровозы, суда оффшорного флота и др.',
        'en_bot_p':'IT - департамент и образовательный центр компании CrewMsg подготовили для вас бота, который поможет вам подготовиться ко всем известным морским тестам.',
        'en_service_p':'Наш сервис позволит вам провести дистанционное интервью с кандидатами. В результате у вас есть возможность посмотреть видеозапись, на которой кандидат отвечает на ваши вопросы.',
        'en_sendBtn':'отправить',
        'en_feedbackBtn':'Обратная связь',
        'en_snackfeedback':'Спасибо за ваше сообщение!'
    }
};