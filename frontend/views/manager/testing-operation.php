<!--<h2>Tabs</h2>-->

<!--<div class="tab">
  <button class="tablinks" onclick="openCity(event, 'Record')">Записать вопросы</button>
  <button class="tablinks" onclick="openCity(event, 'Add')">Добавить кандидата</button>
</div>-->
<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use frontend\assets\AppAsset;
use yii\bootstrap\ActiveForm;

AppAsset::register($this);
?>
<section class="wrapper">
    <!--<button id="btn-1" onclick="location.href = '../'" style="margin-left: -5%; position: absolute"><i class="icon"></i>
      <p>&#8592 Back</p>-->
    </button>
    <ul class="tabs">
      <li class="active" id="instruction-head">Instruction</li>
      <li id="questions-head">Add question</li>
      <li id="candidates-head">Add and invite candidate</li>
    </ul>

    <ul class="tab__content" style="height: 483.5px;">
      <li class="active" id="instruction-content">
        <div class="content__wrapper">
          <h2 class="text-color">Watch the video instruction</h2>
          <video controls="controls" preload="auto" poster="/frontned/web/video/1.png">
            <source src="/frontend/web/video/main.ogv" type="video/ogg; codecs=&quot;theora, vorbis&quot;">
            <source src="/frontend/web/video/main.m4v" type="video/mp4; codecs=&quot;avc1.42E01E, mp4a.40.2&quot;">
            <source src="/frontend/web/video/main.webm" type="video/webm; codecs=&quot;vp8, vorbis&quot;">
          </video>
        </div>
      </li>
      <li id="questions-content">
        <div class="content__wrapper">
          <h2 class="text-color">Create question lists and questions for employees</h2>
          <table class="table" style="float: left;">
                  <tbody>
              <tr> 
                <th>
                  <button class="manage_btn" id="add-list"><i class="icon icon-Files"></i><p>Create list</p></button>
                  <button class="manage_btn" id="delete-list"><i class="icon icon-Delete"></i><p>Remove list</p></button>
                  <button class="manage_btn" id="update-list"><i class="icon icon-Pencil"></i><p>Rename list</p></button>
                  <button class="manage_btn" id="output-list"><i class="icon icon-Pencil"></i><p>Output list</p></button>
                </th>
              </tr>
            </tbody>
          </table>       
          <table class="table" style="float: left;">
            <tbody>
              <?php if (isset($questionLists)): ?>
                <?= Html::hiddenInput('question_list_ids', null, ['id' => 'question_list_ids']) ?>
                <?php foreach ($questionLists as $questionList): ?>
                  <tr data-question-list-id="<?= $questionList->id; ?>">
                    <td class="td_list_name">
                      <div style="display: inline-block">
                        <?= Html::checkbox('question-list', false, ['class' => 'question-list', 'data-question-list-id' => $questionList->id]) ?>
                      </div>
                      <span class="list_name"><?= $questionList->title; ?></span>
                    </td>
                  </tr>
                <?php endforeach; ?>
              <?php endif; ?>
            </tbody>
          </table>
        </div>
          <table class="table">
            <tbody>
              <tr> 
                <th>
                  <button class="manage_btn" id="add-question"><i class="icon icon-Files"></i><p>Create question</p></button>
                  <button class="manage_btn" id="delete-question"><i class="icon icon-Delete"></i><p>Remove</p></button>
                  <button class="manage_btn"id="update-question"><i class="icon icon-Pencil"></i><p>Change</p></button>
                </th>
              </tr>
            </tbody>
          </table>
          <table id="questions_table" class="table">
            <tbody>
              <?php if (isset($questions)): ?>
                <?= Html::hiddenInput('question_ids', null, ['id' => 'question_ids']) ?>
                <?php foreach ($questions as $question): ?>
                  <tr data-question-id="<?= $question->id; ?>">
                    <td>
                      <?= Html::checkbox('question', false, ['class' => 'question', 'data-question-id' => $question->id]) ?>
                      <span class="question"><?= $question->content; ?></span>
                    </td>
                  </tr>
                <?php endforeach; ?>
              <?php endif; ?>
            </tbody>
          </table>
          
        </div>
      </li>
      <li id="candidates-content">
        <div class="content__wrapper">
          <h2 class="text-color">Invite candidate for interview</h2>
          <table class="table candidate_table">
                  <tbody>
              <tr> 
                <th>
                  <button class="manage_btn" id="add-candidate"><i class="icon icon-Files"></i><p>Add candidate</p></button>
                  <button class="manage_btn" id="delete-candidate"><i class="icon icon-Delete"></i><p>Remove candidate</p></button>
                  <button class="manage_btn" id="update-candidate"><i class="icon icon-Pencil"></i><p>Edit data</p></button>
                  <button class="manage_btn" id="invite-candidate"><i class="icon icon-Mail"></i><p>Invite</p></button>
                </th>
              </tr>
            </tbody>
          </table>
          <table class="table candidate_table">
                  <tbody>
              <tr> 
                <th class="candidate_td candidate_th">
                  <input type="checkbox" name="">
                </th>
                <th class="candidate_td candidate_th">
                  <p>Name and Surname</p>
                </th>
                <th class="candidate_td candidate_th">
                  <p>Email</p>
                </th>
                <th class="candidate_td candidate_th">
                  <p>Question list name</p>
                </th>
                <th class="candidate_td candidate_th">
                  <p>Date</p>
                </th>
                <th class="candidate_td candidate_th">
                  <p>Video</p>
                </th>
              </tr>
              <?php if (isset($candidates)): ?>
                <?= Html::hiddenInput('candidate_ids', null, ['id' => 'candidate_ids']) ?>
                <?php foreach ($candidates as $candidate): ?>
                  <tr data-candidate-id="<?= $candidate->id; ?>"> 
                    <td class="candidate_td">
                      <?= Html::checkbox('candidate', false, ['class' => 'candidate', 'data-candidate-id' => $candidate->id]) ?>
                    </td>
                    <td class="candidate_td">
                      <p><?= $candidate->firstname . ' ' . $candidate->surname; ?></p>
                    </td>
                    <td class="candidate_td">
                      <p><?= $candidate->email; ?></p>
                    </td>
                    <td class="candidate_td">
                      <?php if ($candidate->questionList): ?>
                        <p><?= $candidate->questionList->title; ?></p>
                      <?php endif; ?>
                    </td>
                    <td class="candidate_td">
                      <p><?= $candidate->invited_date; ?></p>
                    </td>
                    <td class="candidate_td td_video">
                      <?php if ($candidate->video_url): ?>
                        <a href="<?= $candidate->video_url; ?>"><i class="icon icon-Video"></i></a>
                      <?php endif; ?>
                    </td>
                  </tr>
                <?php endforeach; ?>
              <?php endif; ?>
            </tbody>
          </table>
        </div>
      </li>
    </ul>
  </section>
  <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
  <script src="/frontend/web/js/video_pannel.js"></script>
<script>
function openCity(evt, cityName) {
  var i, tabcontent, tablinks;
  tabcontent = document.getElementsByClassName("tabcontent");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablinks");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" active", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " active";
}
</script>

<script>
  $(document).ready(function(){
      <?php if (Yii::$app->request->get('list_id')): ?>
        var list_id = <?= Yii::$app->request->get('list_id') ?>;
        $("#question_list_ids").val(':' + list_id + ':');
        $("input[data-question-list-id=" + list_id + "]").prop('checked', true);
      <?php endif; ?>  

      <?php if (Yii::$app->request->get('questions') || Yii::$app->request->get('list_id')|| Yii::$app->request->get('list_id')): ?>
        $("#questions-head").addClass('active');
        $("#candidates-head").removeClass('active');
        $("#instruction-head").removeClass('active');

        $("#questions-content").show();
        $("#candidates-content").hide();
        $("#instruction-content").hide();
      <?php elseif (Yii::$app->request->get('candidates')): ?>
        $("#candidates-head").addClass('active');
        $("#instruction-head").removeClass('active');

        $("#candidates-content").addClass('active');
        $("#instruction-content").removeClass('active');

        $("#instruction-content").hide();
        $("#candidates-content").show();
      <?php else: ?>  
        $("#instruction-content").show();
        $("#questions-content").hide();
        $("#candidates-content").hide();
      <?php endif; ?>  

      $("body").on("click", "#questions-head", function(){
        $("#instruction-content").hide();
        $("#candidates-content").hide();

        $("#candidates-head").removeClass('active');
        $("#instruction-head").removeClass('active');
        $("#questions-head").addClass('active');

        var tabWrapper = $(".tab__content");
        var activeTab = tabWrapper.find(".active");
        var activeTabHeight = activeTab.outerHeight();
        tabWrapper.stop().delay(50).animate({
          height: activeTabHeight
        }, 500, function() {
          $("#questions-content").delay(50).fadeIn(250);
        });
      });

      $("body").on("click", "#candidates-head", function(){
        $("#instruction-content").hide();
        $("#questions-content").hide();

        $("#questions-head").removeClass('active');
        $("#instruction-head").removeClass('active');
        $("#candidates-head").addClass('active');

        var tabWrapper = $(".tab__content");
        var activeTab = tabWrapper.find(".active");
        var activeTabHeight = activeTab.outerHeight();
        tabWrapper.stop().delay(50).animate({
          height: activeTabHeight
        }, 500, function() {
          $("#candidates-content").delay(50).fadeIn(250);
        });
      });

      $("body").on("click", "#instruction-head", function(){
        $("#candidates-content").hide();
        $("#questions-content").hide();

        $("#questions-head").removeClass('active');
        $("#candidates-head").removeClass('active');
        $("#instruction-head").addClass('active');

        var tabWrapper = $(".tab__content");
        var activeTab = tabWrapper.find(".active");
        var activeTabHeight = activeTab.outerHeight();
        tabWrapper.stop().delay(50).animate({
          height: activeTabHeight
        }, 500, function() {
          $("#instruction-content").delay(50).fadeIn(250);
        });

      });

      $("body").on("click", "#add-question", function(){
        if ($('.question-list:checked').length > 1) {
          alert('Выберите только лишь один список!'); 
        } else if (!$('.question-list:checked').length) {
          alert('Выберите один список в который будет входить новый вопрос!');
        } else {
          var value = $("#question_list_ids").val();
          value = value.replace(/^:/, '').replace(/:$/, '');
          value = value.split('::')[0];
          $.ajax({
              type: "GET",
              url: "/frontend/web/manager/addquestion",
              data: {'list_id': value, '<?= Yii::$app->request->csrfParam ?>': '<?= \yii::$app->request->csrfToken ?>'}, 
              success: function(data) {
                $("body").append(data);
                document.getElementById('new_question').style.display = 'block';
              },  
              error: function(xhr, str) {
                
              }
          });
        }
      });

      $("body").on("click", "#delete-question", function(){
          if ($('.question:checked').length) {
            if (confirm('Вы действительно желаете удалить вопрос?')) {
                var value = $("#question_ids").val();
                $.ajax({
                  type: "POST",
                  url: "/frontend/web/manager/deletequestion",
                  data: {'question_ids': value, '<?= Yii::$app->request->csrfParam ?>': '<?= \yii::$app->request->csrfToken ?>'},
                  success: function(data) {
                    if (data) {
                      value = value.replace(/^:/, '').replace(/:$/, '');
                      value = value.split('::');
                      $.each(value, function( index, value ) {
                        console.log($('tr[data-question-id=' + value + ']'));
                        $('tr[data-question-id=' + value + ']').remove();
                        var replaced_value = $("#question_ids").val().replace(':' + value + ':', '');
                        $("#question_ids").val(replaced_value);
                      });
                    }
                  },  
                  error: function(xhr, str) {
                    
                  }
                });
              }
          } else {
            alert('Выберите хотя бы один вопрос!');
          }
      });

      $("body").on("click", "#update-question", function(){
           if ($('.question:checked').length > 1) {
              alert('Выберите только лишь один вопрос!');
           } else if (!$('.question:checked').length) {
              alert('Выберите один вопрос!');
           } else {
              var value = $("#question_ids").val();
              $.ajax({
                  type: "POST",
                  url: "/frontend/web/manager/updatequestion",
                  data: {'question_ids': value, '<?= Yii::$app->request->csrfParam ?>': '<?= \yii::$app->request->csrfToken ?>'}, 
                  success: function(data) {
                    $('.question:checked').checked = false;
                    //$("#question_ids").val('');
                    if (data) {
                      $("body").append(data);
                      document.getElementById('change_question').style.display = 'block';
                      //value = value.replace(/^:/, '').replace(/:$/, '');
                      //value = value.split('::');
                      //var text = $('tr[data-question-id="' + value + '"] > td > span').text();
                      //$("#question-title").val(text);
                    }
                  },  
                  error: function(xhr, str) {
                    
                  }
              });
           }
      });

      $("body").on("change", ".question", function() {
        var value = $("#question_ids").val();
        if ($(this).is(":checked")) {     
          $("#question_ids").val(value + ':' + $(this).data('question-id') +  ':');
        } else {
          value = value.replace(':' + $(this).data('question-id') + ':', '');
          $("#question_ids").val(value);
        }
      });

      $("body").on("click", "#clear-question", function() {
        $.each($('.question:checked'), function( index, value ) {
          value.checked = false;
        });
        $("#question_ids").val('');
      });


      //////////////////////////////////////////////////////////////////////


      $("body").on("click", "#add-list", function(){
        $.ajax({
          type: "GET",
          url: "/frontend/web/manager/addquestionlist",
          data: {'<?= Yii::$app->request->csrfParam ?>': '<?= \yii::$app->request->csrfToken ?>'}, 
          success: function(data) {
            $("body").append(data);
            document.getElementById('new_list').style.display = 'block';
          },  
          error: function(xhr, str) {
                  
          }
        });
      });

      $("body").on("click", "#update-list", function(){
           if ($('.question-list:checked').length > 1) {
              alert('Выберите только один список!')
           } else if (!$('.question-list:checked').length) {
              alert('Выберите один список!')
           } else {
              var value = $("#question_list_ids").val();
              $.ajax({
                  type: "POST",
                  url: "/frontend/web/manager/updatequestionlist",
                  data: {'question_list_ids': value, '<?= Yii::$app->request->csrfParam ?>': '<?= \yii::$app->request->csrfToken ?>'}, 
                  success: function(data) {
                    $('.question-list:checked').checked = false;
                    //$("#question_list_ids").val('');
                    if (data) {
                      $("body").append(data);
                      document.getElementById('change_list').style.display = 'block';
                      //value = value.replace(/^:/, '').replace(/:$/, '');
                      //value = value.split('::');
                      //var text = $('tr[data-question-id="' + value + '"] > td > span').text();
                      //$("#question-title").val(text);
                    }
                  },  
                  error: function(xhr, str) {
                    
                  }
              });
           }
      });

      $("body").on("click", "#output-list", function(){
           if ($('.question-list:checked').length > 1) {
              alert('Выберите только один список!')
           } else if (!$('.question-list:checked').length) {
              alert('Выберите один список!')
           } else {
              var value = $("#question_list_ids").val();
              $.ajax({
                  type: "POST",
                  url: "/frontend/web/manager/outputquestionlist",
                  data: {'question_list_ids': value, '<?= Yii::$app->request->csrfParam ?>': '<?= \yii::$app->request->csrfToken ?>'}, 
                  success: function(data) {
                    if (data) {
                      newContent = '<tbody>';
                      data.forEach(function(element) {                    
                        newContent += '<tr data-question-id="' + element.id + '">' +
                                        '<td>' +
                                          '<input type="checkbox" class="question" name="question" value="1" data-question-id="' + element.id + '">' +
                                          '<span class="question">' + element.content + '</span>' +
                                        '</td>' +
                                      '</tr>';          
                      });
                      newContent += '</tbody>';
                    }
                    $('#questions_table').html(newContent);
                  },  
                  error: function(xhr, str) {
                    
                  }
              });
           }
      });

      $("body").on("click", "#delete-list", function(){
            if ($('.question-list:checked').length) {
              if (confirm('Вы действительно желаете удалить список?')) {
                var value = $("#question_list_ids").val();
                $.ajax({
                  type: "POST",
                  url: "/frontend/web/manager/deletequestionlist",
                  data: {'question_list_ids': value, '<?= Yii::$app->request->csrfParam ?>': '<?= \yii::$app->request->csrfToken ?>'},
                  success: function(data) {
                    if (data) {
                      value = value.replace(/^:/, '').replace(/:$/, '');
                      value = value.split('::');
                      $.each(value, function( index, value ) {
                        $('tr[data-question-list-id=' + value + ']').remove();
                        var replaced_value = $("#question_list_ids").val().replace(':' + value + ':', '');
                        $("#question_list_ids").val(replaced_value);
                      });
                    }
                  },  
                  error: function(xhr, str) {
                    
                  }
                });
              }
          } else {
            alert('Выберите хотя бы один список!');
          }
      });

      /*$("body").on("click", "#out-question", function() {
        if ($('.question-list:checked').length > 1) {
          alert('Выберите только один список для вывода его вопросов!');
        } else if (!$('.question-list:checked').length) {
          alert('Выберите один список для вывода его вопросов!');
        } else {
          var value = $("#question_list_ids").val();
          value = value.replace(/^:/, '').replace(/:$/, '');
          value = value.split('::');
          $.ajax({
            type: "GET",
            url: "/frontend/web/manager/getquestionstolist",
            data: {'question_list_id': value, '<?= Yii::$app->request->csrfParam ?>': '<?= \yii::$app->request->csrfToken ?>'}, 
            success: function(data) {
              if (data) {
                var list_title = $("tr[data-question-list-id=" + value + "] > td > span:first-child")
                                            .text().replace(/^\s*(.*)\s*$/, '$1');
                var content_title = '<div id="question-list-name" style="float: right">' +
                                      '<span><b>' + list_title + ':</b></span>' +
                                    '</div>';
                $("#question-list-name").remove();
                $("#clear-question").after(content_title);
                var content = '<input type="hidden" id="question_ids" name="question_ids">';
                $("#question_table > tbody").html('');
                $("#question_table > tbody").append(content);
                $.each(JSON.parse(data), function( index, value ) {
                  content = '<tr data-question-id="' + value.id + '">' +
                              '<td>' +
                                '<span>' + value.title + '</span>' +
                                '<div style="display: inline-block; margin-left: 1.5%">' +
                                '<input type="checkbox" id="question" class="question" name="question" data-question-id="' + value.id + '">' +
                                '</div>' +
                              '</td>' +
                            '</tr>';         
                  $("#question_table > tbody").append(content);
                });
                $(".question-content").show();
              } else {
                alert('У выбранного списка отсутствуют вопросы!');
              }
            },  
            error: function(xhr, str) {
                    
            }
          });
        }
      });*/


      $("body").on("click", "#clear-list", function() {
        $.each($('.question-list:checked'), function( index, value ) {
          value.checked = false;
        });
        $("#question_list_ids").val('');
      });

      $("body").on("change", ".question-list", function() {
        var value = $("#question_list_ids").val();
        if ($(this).is(":checked")) {     
          $("#question_list_ids").val(value + ':' + $(this).data('question-list-id') +  ':');
        } else {
          value = value.replace(':' + $(this).data('question-list-id') + ':', '');
          $("#question_list_ids").val(value);
        }
      });


      //////////////////////////////////////////////////////////////////////


      $("body").on("click", "#add-candidate", function(){
        $.ajax({
          type: "GET",
          url: "/frontend/web/manager/addcandidate",
          data: {'<?= Yii::$app->request->csrfParam ?>': '<?= \yii::$app->request->csrfToken ?>'}, 
          success: function(data) {
            $("body").append(data);
            document.getElementById('new_candidate').style.display = 'block';
          },  
          error: function(xhr, str) {
                  
          }
        });
      });

      $("body").on("click", "#delete-candidate", function(){
           if ($('.candidate:checked').length) {
              if (confirm('Вы действительно желаете удалить кандидата?')) {
                var value = $("#candidate_ids").val();
                $.ajax({
                    type: "POST",
                    url: "/frontend/web/manager/deletecandidate",
                    data: {'candidate_ids': value, '<?= Yii::$app->request->csrfParam ?>': '<?= \yii::$app->request->csrfToken ?>'},
                    success: function(data) {
                      if (data) {
                        value = value.replace(/^:/, '').replace(/:$/, '');
                        value = value.split('::');
                        $.each(value, function( index, value ) {
                          $('tr[data-candidate-id=' + value + ']').remove();
                          var replaced_value = $("#candidate_ids").val().replace(':' + value + ':', '');
                          $("#candidate_ids").val(replaced_value);
                        });
                      }
                    },  
                    error: function(xhr, str) {
                      
                    }
                });
              }
           } else {
            alert('Выберите хотя бы одного кандидата!');
           }
           var value = $("#candidate_ids").val();
      });

      $("body").on("click", "#update-candidate", function(){
           if ($('.candidate:checked').length > 1) {
              alert('Выберите только лишь одного кандидата!');
           } else if (!$('.candidate:checked').length) {
              alert('Выберите одного кандидата!');
           } else {
              var value = $("#candidate_ids").val();
              $.ajax({
                  type: "POST",
                  url: "/frontend/web/manager/updatecandidate",
                  data: {'candidate_ids': value, '<?= Yii::$app->request->csrfParam ?>': '<?= \yii::$app->request->csrfToken ?>'}, 
                  success: function(data) {
                    $('.candidate:checked').checked = false;
                    //$("#candidate_ids").val('');
                    if (data) {
                      $("body").append(data);
                      document.getElementById('edit_candidate').style.display = 'block';
                      //value = value.replace(/^:/, '').replace(/:$/, '');
                      //value = value.split('::');
                      //var text = $('tr[data-question-id="' + value + '"] > td > span').text();
                      //$("#question-title").val(text);
                    }
                  },  
                  error: function(xhr, str) {
                    
                  }
              });
           }
      });

      $("body").on("click", "#clear-candidate", function() {
        $.each($('.candidate:checked'), function( index, value ) {
          value.checked = false;
        });
        $("#candidate_ids").val('');
      });

      $("body").on("change", ".candidate", function() {
        var value = $("#candidate_ids").val();
        if ($(this).is(":checked")) {     
          $("#candidate_ids").val(value + ':' + $(this).data('candidate-id') +  ':');
        } else {
          value = value.replace(':' + $(this).data('candidate-id') + ':', '');
          $("#candidate_ids").val(value);
        }
      });

      $(document).ready(function(){
        $("body").on("click", "#invite-candidate", function(){
          if ($('.candidate:checked').length) {
              var value = $("#candidate_ids").val();
              $.ajax({
                type: "POST",
                url: "/frontend/web/manager/invitecandidate",
                data: {'candidate_ids': value, '<?= Yii::$app->request->csrfParam ?>': '<?= \yii::$app->request->csrfToken ?>'}, 
                success: function(data) {
                  $.each($('.candidate:checked'), function( index, value ) {
                    value.checked = false;
                  });
                  $("#candidate_ids").val('');
                  if (!data) {
                    alert('Ни один из кандидатов не был приглашен по техническим причинам!');
                  } else {
                    $.each(JSON.parse(data), function( index, value ) {
                        alert('Кандидату ' + value + ' было отправлено приглашение на его email.');
                    });
                    /*$(".inviting-candidates-content").remove();
                 
                    var content = '<div class="inviting-candidates-content" style="margin-top: 2%">' +
                                    '<table class="table table-bordered" id="question_id_table" style="float: left">' +
                                      '<thead>' +
                                        '<caption style="color: #13968d; font-size: 285%">Успешно приглашенные кандидаты</caption>' +
                                        '<tr>' +
                                          '<th scope="col">' +
                                            '<span>id</span>' +
                                          '</th>' +
                                          '<th scope="col">' +
                                            '<span>Имя</span>' +
                                          '</th>' +
                                        '</tr>' +
                                      '</thead>' +
                                      '<tbody>';
                    $.each(JSON.parse(data), function( index, value ) {
                        content += '<tr data-question-list-id="<?= $questionList->id; ?>">' +
                                    '<td>' +
                                      '<span>' + index + '<span>' +
                                    '</td>' +
                                    '<td>' +
                                      '<span>' + value + '<span>' +
                                    '</td>' +
                                   '</tr>';
                    });
                    content +=   '</tbody>' +
                                '</table>' +
                               '</div>';
                    $(".wrapper").append(content);
                    $('html, body').animate({ scrollTop: $(".inviting-candidates-content").offset().top }, 500);*/
                  }
                },  
                error: function(xhr, str) {
                            
                }
              });
          } else {
            alert('Выберите хотя бы одного кандидата!');
          }
        });
      });

      <?php if (Yii::$app->request->get('list_id')): ?>
          $("#question_list_ids").val(':' + <?= Yii::$app->request->get('list_id') ?> + ':');
      <?php else: ?>
        if ($('.question-list:input')[0]) {
          $('.question-list:input')[0].checked = true
          $("#question_list_ids").val(':' + $('.question-list:input')[0].getAttribute('data-question-list-id') + ':');
        }
      <?php endif; ?>

      $('.list_name').on('click', function(){
        var value = $(this).parent().parent().data('question-list-id');
        $.ajax({
            type: "GET",
            url: "/frontend/web/manager/getquestionstolist",
            data: {'question_list_id': value, '<?= Yii::$app->request->csrfParam ?>': '<?= \yii::$app->request->csrfToken ?>'}, 
            success: function(data) {
              if (data) {
                var list_title = $("tr[data-question-list-id=" + value + "] > td > span:first-child")
                                            .text().replace(/^\s*(.*)\s*$/, '$1');
                var content_title = '<div id="question-list-name" style="float: right">' +
                                      '<span><b>' + list_title + ':</b></span>' +
                                    '</div>';
                $("#question-list-name").remove();
                $("#clear-question").after(content_title);
                var content = '<input type="hidden" id="question_ids" name="question_ids">';
                $("#questions_table > tbody").html('');
                $("#questions_table > tbody").append(content);
                $.each(JSON.parse(data), function( index, value ) {
                  content = '<tr data-question-id="' + value.id + '">' +
                              '<td>' +
                                '<span>' + value.content + '</span>' +
                                '<div style="display: inline-block; margin-left: 1.5%">' +
                                '<input type="checkbox" id="question" class="question" name="question" data-question-id="' + value.id + '">' +
                                '</div>' +
                              '</td>' +
                            '</tr>';         
                  $("#questions_table > tbody").append(content);
                });
                $(".question-content").show();
                $('input[type="checkbox"]:checked').prop('checked', false)
                $('input[type="checkbox"][data-question-list-id="' + value + '"]').prop('checked', true);
              } else {
                alert('У выбранного списка отсутствуют вопросы!');
              }
            },  
            error: function(xhr, str) {
                    
            }
        });
      });

      /*$("body").on("click", "#invite-candidate", function(){
        if ($('.candidate:checked').length > 1) {
            alert('Выберите только лишь одного кандидата!');
        } else if (!$('.candidate:checked').length) {
            alert('Выберите одного кандидата!');
        } else {
          var value = $("#candidate_ids").val();
          $.ajax({
            type: "POST",
            url: "/frontend/web/manager/invitecandidate",
            data: {'candidate_ids': value, '<?= Yii::$app->request->csrfParam ?>': '<?= \yii::$app->request->csrfToken ?>'}, 
            success: function(data) {
              $.each($('.candidate:checked'), function( index, value ) {
                value.checked = false;
              });
              $("#candidate_ids").val('');
              if (data == 0) {
                alert('Кандидат уже был приглашен ранее!');
              } else if (data == 1) {
                alert('Кандидат успешно приглашен!');
              }  else if (data == 3) {
                alert('Кандидат не приглашен, так как, отсутствуют вопросы в его списке!');
              } else {
                alert('Кандидат не приглашен по техническим причинам!');
              }
            },  
            error: function(xhr, str) {
                        
            }
          });
        }
      });*/
  });
</script>