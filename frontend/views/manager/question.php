<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\SignupForm */
   
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
   
?>
<div id="new_question" class="modal">
   <div class="modal-content animate">
      <div class="container_modal">
         <span onclick="document.getElementById('new_question').style.display='none'" class="close" title="Close Modal">x</span>
         <p>Create new question</p>
         <?php $form = ActiveForm::begin(['action' => Yii::getAlias('@web') . '/manager/addquestion', 'id' => 'question-form', 'options' => ['class' => 'modal_form']]); ?>
           <?=  Html::hiddenInput(Yii::$app->getRequest()->csrfParam, Yii::$app->getRequest()->getCsrfToken(), []); ?>
           <?=  Html::hiddenInput('list_id', $listId); ?>
           <?= $form->field($model, 'content')->input('text', ['class' => 'modal_input', 'name' => 'content', 'placeholder' => 'Type a question', 'required' => ''])->label(false); ?>
           <?= $form->field($model, 'position')->input('number', ['class' => 'modal_input', 'name' => 'position', 'placeholder' => 'Type number of the question', 'required' => ''])->label(false); ?>
           <?= $form->field($model, 'time')->input('text', ['class' => 'modal_input', 'name' => 'time', 'placeholder' => 'Input time for question in second', 'required' => ''])->label(false); ?>
           <div>
              <button class="modal_button">Create question</button>
           </div>
         <?php ActiveForm::end(); ?>
         <button class="modal_button cancel_btn" onclick="document.getElementById('new_question').style.display='none'">Cancel</button>
      </div>
      <div class="container_modal_rights">
         <span>- Made by <a href="#">CrewMSG -</a></span>
         <span>- All rights reserved -</span>
      </div>
   </div>
</div>