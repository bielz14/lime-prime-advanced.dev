<div class="description-container" id="description-container">
    <div class="description-content">
        <div class="description-question">
            <div class="line">
                <div class="line1"></div>
                <div class="line2"></div>
            </div>
            <p><img src="images/question.png"><p>
        </div>
        <div class="description-text">
            <h2 class="lang description_h2" key="en_description_text">Что такое CrewMSG</h2>
            <p class="lang what-description" key="en_description_p">CrewMSG – это Немецкая компания с представительством в Украине, предоставляющая услуги иностранным работодателям и судовладельцам в сферах рекрутинга. Мы помогаем в наборе квалифицированных моряков из Украины и Европы. Наша команда специалистов находится в постоянном поиске новых концепций и решений, позволяющих идти в ногу со временем и повышать качество предоставляемых услуг для партнеров и клиентов компании.
            </p>
            <a href="#" class="watchVideo" onclick="document.getElementById('video').style.display='block'">
                <i class="icon icon-Play"></i>
                <span class="lang" key="en_watchVideo">Смотреть видео</span>
            </a>
            <div id="video" class="modal">
                <div class="modal-content animate">
                    <div class="container_modal">
                        <span onclick="document.getElementById('video').style.display='none'" class="close" title="Close Modal">x</span>
                        <video controls="controls" preload="auto" poster="video/1.png">
                            <source src="video/main.ogv" type="video/ogg; codecs=&quot;theora, vorbis&quot;">
                            <source src="video/main.m4v" type="video/mp4; codecs=&quot;avc1.42E01E, mp4a.40.2&quot;">
                            <source src="video/main.webm" type="video/webm; codecs=&quot;vp8, vorbis&quot;">
                        </video>
                    </div>
                    <div class="container_modal_rights">
                        <span>- Made by <a href="#">CrewMSG -</a></span>
                        <span>- All rights reserved -</span>
                    </div>
                </div>
            </div>
            <div class="line_bottom"></div>
        </div>
    </div>
</div>
<div class="offer-container" id="offer-container">
    <h2 class="lang" key="en_offer-container">ЧТО МЫ ПРЕДЛАГАЕМ</h2>
    <div class="offer-box">
        <div class="offer" onclick="document.getElementById('mariners_offer').style.display='block'">
            <img src="images/offer1.png">
            <p class="lang" key="en_offer-box1">Быстрое трудоустройство моряков</p>
        </div>
        <div class="offer"  onclick="document.getElementById('company_offer').style.display='block'">
            <img src="images/offer2.png">
            <p class="lang" key="en_offer-box2">Сотрудничество для крюинговых компаний</p>
        </div>
        <div class="offer" onclick="document.getElementById('bot_offer').style.display='block'">
            <!-- 				<span>Try it!</span> -->
            <img src="images/offer3.png">
            <p class="lang" key="en_offer-box3">Боты помощники</p>
        </div>
        <div class="offer" onclick="document.getElementById('service_offer').style.display='block'">
            <img src="images/offer4.png">
            <p class="lang" key="en_offer-box4">Новый сервис - Раздел в разработке</p>
        </div>
        <!-- modal -->
        <!-- 			<div id="mariners_offer" class="modal">
                      <div class="modal-content animate">
                        <div class="container_modal">
                            <span onclick="document.getElementById('mariners_offer').style.display='none'" class="close" title="Close Modal">x</span>
                            <p class="lang" key ="en_marins_p">Оставьте ваше резюме и телефон, чтобы рекрутеры смогли предложить вам работу. А также получайте рассылки с подходящими для вас вакансиями на электронную почту и канал в телеграмме.</p>
                            <form enctype="multipart/form-data" method="post" id="feedback-form">
                                <input class="mariners_input" type="tel" name="phoneF" id="phoneF" placeholder="phone" >
                                <input class="test" type="file" name="fileF" id="fileF" required>
                                <button  class="modal_button lang" type="submit" id="submitF" key="en_sendBtn">Отправить</button>
                            </form>
                        </div>
                        <div class="container_modal_rights">
                              <span>- Made by <a href="#">CrewMSG -</a></span>
                              <span>- All rights reserved -</span>
                        </div>
                      </div>
                    </div>	 -->
        <div id="company_offer" class="modal">
            <div class="modal-content animate">
                <div class="container_modal">
                    <span onclick="document.getElementById('company_offer').style.display='none';" class="close" title="Close Modal">x</span>
                    <p class="lang" key ="en_company_p">Наша компания предлагает качественные услуги по подбору и отправке на суда морских специалистов - офицерского и рядового состава.  Мы подбираем специалистов для работы на различных типах судов, на сухогрузы, рефрижераторный и рыбопромысловый флот, танкерный флот, балкеры, контейнеровозы, суда оффшорного флота и др.</p>
                    <button class="modal_button">
                        <a href="#feedback-container" class="lang" key="en_button1" onclick="document.getElementById('company_offer').style.display='none'">Написать нам</a>
                    </button>
                </div>
                <div class="container_modal_rights">
                    <span>- Made by <a href="#">CrewMSG -</a></span>
                    <span>- All rights reserved -</span>
                </div>
            </div>
        </div>
        <div id="bot_offer" class="modal">
            <div class="modal-content animate">
                <div class="container_modal">
                    <span onclick="document.getElementById('bot_offer').style.display='none'" class="close" title="Close Modal">x</span>

                    <p class="lang" key ="en_bot_p">IT - департамент и образовательный центр компании CrewMsg подготовили для вас бота, который поможет вам подготовиться ко всем известным морским тестам.</p>
                    <button class="accordion">Bot 1</button>
                    <div class="panel">
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                        <button class="modal_button">
                            <a href="https://t.me/jsdhkfdjufewnkdf_bot" target="_blank" class="lang" key="en_button2">Начать</a>
                        </button>
                    </div>

                    <button class="accordion">Bot 2</button>
                    <div class="panel">
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                        <button class="modal_button">
                            <a href="#" target="_blank" class="lang" key="en_button2">Начать</a>
                        </button>
                    </div>

                    <button class="accordion">Bot 3</button>
                    <div class="panel">
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                        <button class="modal_button">
                            <a href="#" target="_blank" class="lang" key="en_button2">Начать</a>
                        </button>
                    </div>

                    <button class="accordion">Bot 4</button>
                    <div class="panel">
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                        <button class="modal_button">
                            <a href="#" target="_blank" class="lang" key="en_button2">Начать</a>
                        </button>
                    </div>

                    <button class="accordion">Bot 5</button>
                    <div class="panel">
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                        <button class="modal_button">
                            <a href="#" target="_blank" class="lang" key="en_button2">Начать</a>
                        </button>
                    </div>

                    <button class="accordion">Bot 6</button>
                    <div class="panel">
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                        <button class="modal_button">
                            <a href="#" target="_blank" class="lang" key="en_button2">Начать</a>
                        </button>
                    </div>
                </div>
                <div class="container_modal_rights">
                    <span>- Made by <a href="#">CrewMSG -</a></span>
                    <span>- All rights reserved -</span>
                </div>
            </div>
        </div>
        <!-- 			<div id="service_offer" class="modal">
                      <div class="modal-content animate">
                        <div class="container_modal">
                            <span onclick="document.getElementById('service_offer').style.display='none'" class="close" title="Close Modal">x</span>
                            <p class="lang" key ="en_service_p">Наш сервис позволит вам провести дистанционное интервью с кандидатами. В результате у вас есть возможность посмотреть видеозапись, на которой кандидат отвечает на ваши вопросы.</p>
                            <button class="modal_button">
                                <a href="#description-container" class="lang" key="en_button3" onclick="document.getElementById('service_offer').style.display='none'">Узнать больше</a>
                            </button>
                        </div>
                        <div class="container_modal_rights">
                              <span>- Made by <a href="#">CrewMSG -</a></span>
                              <span>- All rights reserved -</span>
                        </div>
                      </div>
                    </div> -->

    </div>
</div>
<div class="feedback-container" id="feedback-container">
    <form action="send.php" method="POST" id="application" class="application" name=" application">
        <input class="application-item application-field" name="first_name" id="applicationName" maxlength="20" placeholder="Name" type="text" required />
        <input class="application-item application-field" name="email" type="email" id="applicationEmail" maxlength="40" placeholder="E-mail" required />
        <input class="application-item application-field" name="phone" type="tel" id="applicationTelephone" maxlength="20" placeholder="Phone"/>
        <textarea class="application-item application-field" name="message" id="applicationText" placeholder="Message" type="text"></textarea>
        <button class="applicationButton application-item" type="submit" form="application" name="submit">
            <i class="icon-Plaine icon"></i>
            <span class="lang" key="en_sendBtn">отправить</span>
        </button>
        <button type="button" class="applicationButton application-item" onclick="closeForm()">
            <span class="lang" key="en_feedbackBtn">Обратная связь</span>
            <i class="arrow up"></i>
        </button>
    </form>
    <div class="open-form" id="open-form">
        <button id="send-button" class="applicationButton  application-item open-button" onclick="openForm()">
            <span class="lang" key="en_feedbackBtn">Обратная связь</span>
            <i class="arrow down"></i>
        </button>
    </div>
    <!-- 		<div id="snackbar" class="lang"  key="en_snackfeedback">Спасибо!<br>Мы скоро с вами свяжемся!</div> -->
    <hr>
</div>

<footer class="footer-container" id="footer-container">
    <div class="contacts">
        <div class="info">
            <img src="images/ukr.png">
            <a href="http://crewmsg.com" target="_blank">CrewMSG</a>
            <div>65111 - Odessa, Ukraine</div>
            <a href="#">+380487890087</a>
            <a href="mailto:">info@crewmsg.com</a>
        </div>
        <div class="info">
            <img src="images/ger.png">
            <a href="http://www.kmarecruiting.net/ru">KMA Recruiting UG</a>
            <div>28199 - Am Deich 64 f -  Bremen, Germany</div>
            <a href="#">+4917653243390</a>
            <a href="mailto:">info@kmarecruiting.net</a>
        </div>
    </div>
    <div class="socials">
        <a class="fa fa-instagram" href="https://www.instagram.com/crewmsg/?hl=de" target="_blank"></a>
        <a class="fa fa-facebook" href="https://www.facebook.com/groups/325275011585933/" target="_blank"></a>
        <a class="fa fa-linkedin" href="https://www.linkedin.com/company/crewmsg/" target="_blank"></a>
    </div>
</footer>
<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
<script src="/js/landing.js"></script>
<script src="/js/translate.js"></script>