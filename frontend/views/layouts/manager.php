<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use backend\assets\AppAsset;
use common\widgets\Alert;
use yii\bootstrap\ActiveForm;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <!--<link rel="stylesheet" type="text/css" href="/backend/web/css/style.css">
  <link rel="stylesheet" type="text/css" href="/backend/web/css/styleModal.css">
  <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">-->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
  <link rel="stylesheet" type="text/css" href="/frontend/web/css/video_pannel.css">
  <link href="https://fonts.googleapis.com/css?family=Roboto:400,500,700,900" rel="stylesheet">
  <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>


    <div class="navbar" id="navbar">
        <div class="navbar-content">
            <div class="name">
                <h3><a href="http://crewmsg.com" target="_blank">Crew<span>MSG</span></a></h3>
            </div>
            <div class="nav" id="mynav">
              <a id="nav-companies" href="#offer-container" class="menu_item menu_item_first" style="color: #05324a">
                <i class="icon icon-Settings"></i>
                <p class="lang" key="en_for_companies" >Settings</p>
              </a>   
                <?php if (!Yii::$app->user->isGuest): ?>
                    <?= 
                        Html::tag('a', '<p class="lang" key="en_contacts">
                                            <i class="icon icon-Exit"></i>
                                            Exit
                                        </p>', 
                        ['id' => 'nav-contacts', 'class' => 'menu_item', 'style' => 'color: #05324a;', 'href' => '/frontend/web/manager/logout']) 
                    ?>
                <?php endif; ?>
              </a>
            </div>
        </div>
    </div>

    <?= $content ?>

<footer class="footer">
    <!--<div class="container">
        <p class="pull-left">&copy; <?= Html::encode(Yii::$app->name) ?> <?= date('Y') ?></p>

        <p class="pull-right"><?= Yii::powered() ?></p>
    </div>-->
</footer>

<?php $this->endBody() ?>
</body>
<script type="text/javascript">
    $(document).ready(function(){
        $("body").on("click", "#logout", function(){
            event.preventDefault();
            $('#form_logout').submit();
        });
    });
</script>
</html>
<?php $this->endPage() ?>