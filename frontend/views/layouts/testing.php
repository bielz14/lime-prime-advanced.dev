<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;
use yii\bootstrap\ActiveForm;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
    <!DOCTYPE html>
    <html lang="<?= Yii::$app->language ?>">

    <head>
        <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
        <title> Crewing service CrewMSG | быстрый старт вашей карьеры в море</title>
        <meta name="Description" content=" CrewMsg, crewmessenger, Крюинг. Крюинговые компании Украины, России, Индии, Польши, Румынии, Болгарии, Турции и Филиппины, осуществляют подбор для работы в море. Морские агентства Одессы, Николаева, Херсона, Мариуполя, Керчи, Измаила, Ялты, Симферополя, Новороссийск, Латвии, Литвы, Эстонии, Манила, Мумбаи, Костанца, Варна, Стамбул. База анкет, резюме и CV моряков. Новые вакансии морских агентств.Горячие вакансии от морских агентств СНГ, Европы. Свежая база.
        Crewing of Ukraine, Russia, India, Poland, Romania, Bulgaria, Turkey and the Philippines, are recruiting for work at sea. Maritime agencies of Odessa, Nikolaev, Kherson, Mariupol, Kerch, Izmail, Yalta, Simferopol, Novorossiysk, Latvia, Lithuania, Estonia, Manila, Mumbai, Kostanca, Varna, Istanbul. The database of CV, of seafarers. New vacancies for marine agencies.">

        <meta name="Keywords" content=" crewmsg, crewmessenger, крюмсг, работа для моряков, крюинг, вакансии для моряков , резюме моряков,   крюинговые компании Украины, морские компании России, горячие вакансии, оффшор, crew management,crewing,maritime,maritime companies,ship, ship management,vessel,odessa ukraine,ukraina maritime agency,agency,crewing agency,container,odessa jobs,crewing management,boat,ukraine odessa,certification centers,ukraina,crewing company,Logistics,jobs in odessa,it certification centers,specialists,hotel staff,forwarding,odessa port,training and certification,cruise passenger staff,chartering,Driller, Driller Assistant, QA/QC Manager, Painting Inspector, Safety Engineer, Hse Officer, Safety Officer">
        <meta name="viewport"
              content="width=device-width,  height=device-height, user-scalable=0, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,500,700,900" rel="stylesheet">
        <link rel="shortcut icon" href="/images/logo.png" type="image/x-icon">
        <!--<link rel="stylesheet" type="text/css" href="/css/landing.css">-->
        <link rel="stylesheet" type="text/css" href="/css/landing-media.css">
        <link rel="stylesheet" type="text/css" href="/css/animate.css">
        <link rel="stylesheet" href="/css/stroke-gap-icons.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
        <!-- Facebook Pixel Code -->
        <script>
            !function(f,b,e,v,n,t,s)
            {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
                n.callMethod.apply(n,arguments):n.queue.push(arguments)};
                if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
                n.queue=[];t=b.createElement(e);t.async=!0;
                t.src=v;s=b.getElementsByTagName(e)[0];
                s.parentNode.insertBefore(t,s)}(window, document,'script',
                'https://connect.facebook.net/en_US/fbevents.js');
            fbq('init', '338723726900434');
            fbq('track', 'PageView');
        </script>
        <noscript><img height="1" width="1" style="display:none"
                       src="https://www.facebook.com/tr?id=338723726900434&ev=PageView&noscript=1"
            /></noscript>
        <!-- End Facebook Pixel Code -->
    </head>
    <?php $this->beginBody() ?>
        <body link="#fff" vlink="#fff" alink="#fff" bgcolor="#fff">
            <?= $content ?>
        </body>
    <?php $this->endBody() ?>
</html>
<?php $this->endPage() ?>