<?php
namespace frontend\models;

use Yii;
use yii\base\Model;
use frontend\models\BridgeEngineRoomResourceManagement;

class BridgeEngineRoomResourceManagementForm extends Model
{
    public $id;
    public $number;
    public $iss_date;
    public $exp_date;
    public $iss_by;

    public function rules()
    {
        return [
            ['id', 'each', 'rule' => ['integer']],
            
            ['number', 'each', 'rule' => ['string']],
            
            ['iss_date', 'each', 'rule' => ['string']],
            
            ['exp_date', 'each', 'rule' => ['string']],
            
            ['iss_by', 'each', 'rule' => ['string']],
        ];
    }

    public function addBridgeengineroomresourcemanagement()
    {        
        $bridgeengineroomresourcemanagemen = new BridgeEngineRoomResourceManagement();
        if ($this->validate()) {
            $bridgeengineroomresourcemanagemen->number = $this->number[35];
            $bridgeengineroomresourcemanagemen->iss_date = $this->iss_date[41];
            $bridgeengineroomresourcemanagemen->exp_date = $this->exp_date[41];
            $bridgeengineroomresourcemanagemen->iss_by = $this->iss_by[40];
            if ($bridgeengineroomresourcemanagemen->save()) {
                return $bridgeengineroomresourcemanagemen;
            }
        }

        return null;
    }

    public function updateBridgeengineroomresourcemanagement()
    {        
        $bridgeengineroomresourcemanagemen = BridgeEngineRoomResourceManagement::findById($this->id[43]);
        if ($this->validate()) {
            $bridgeengineroomresourcemanagemen->number = $this->number[34];
            $bridgeengineroomresourcemanagemen->iss_date = $this->iss_date[42];
            $bridgeengineroomresourcemanagemen->exp_date = $this->exp_date[42];
            $bridgeengineroomresourcemanagemen->iss_by = $this->iss_by[39];
            if ($bridgeengineroomresourcemanagemen->save()) {
                return $bridgeengineroomresourcemanagemen;
            }
        }

        return null;
    }
}

