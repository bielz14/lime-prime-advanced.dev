<?php
namespace frontend\models;

use Yii;
use yii\base\Model;
use frontend\models\FastRescueCraft;

class FastRescueCraftForm extends Model
{
    public $id;
    public $number;
    public $iss_date;
    public $exp_date;
    public $iss_by;

    public function rules()
    {
        return [
            ['id', 'each', 'rule' => ['integer']],
            
            ['number', 'each', 'rule' => ['string']],
            
            ['iss_date', 'each', 'rule' => ['string']],
            
            ['exp_date', 'each', 'rule' => ['string']],
            
            ['iss_by', 'each', 'rule' => ['string']],
        ];
    }

    public function addFastrescuecraft()
    {   
        $fastrescuecraft = new FastRescueCraft();
        if ($this->validate()) {
            $fastrescuecraft->number = $this->number[16];
            $fastrescuecraft->iss_date = $this->iss_date[22];
            $fastrescuecraft->exp_date = $this->exp_date[22];
            $fastrescuecraft->iss_by = $this->iss_by[21];
            if ($fastrescuecraft->save()) {
                return $fastrescuecraft;
            }
        }

        return null;
    }

    public function updateFastrescuecraft()
    {        
        $fastrescuecraft = FastRescueCraft::findById($this->id[24]);
        if ($this->validate()) {
            $fastrescuecraft->number = $this->number[15];
            $fastrescuecraft->iss_date = $this->iss_date[23];
            $fastrescuecraft->exp_date = $this->exp_date[23];
            $fastrescuecraft->iss_by = $this->iss_by[20];
            if ($fastrescuecraft->save()) {
                return $fastrescuecraft;
            }
        }

        return null;
    }
}

