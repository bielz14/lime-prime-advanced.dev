<?php
namespace frontend\models;

use Yii;
use yii\base\Model;
use frontend\models\AdvFireFighting;

class AdvFireFightingForm extends Model
{
    public $id;
    public $number;
    public $iss_date;
    public $exp_date;
    public $iss_by;

    public function rules()
    {
        return [
            ['id', 'each', 'rule' => ['integer']],
            
            ['number', 'each', 'rule' => ['string']],
            
            ['iss_date', 'each', 'rule' => ['string']],
            
            ['exp_date', 'each', 'rule' => ['string']],
            
            ['iss_by', 'each', 'rule' => ['string']],
        ];
    }

    public function addAdvfirefighting()
    {        
        $advfirefighting = new AdvFireFighting();
        if ($this->validate()) {
            $advfirefighting->number = $this->number[10];
            $advfirefighting->iss_date = $this->iss_date[16];
            $advfirefighting->exp_date = $this->exp_date[16];
            $advfirefighting->iss_by = $this->iss_by[15];
            if ($advfirefighting->save()) {
                return $advfirefighting;
            }
        }

        return null;
    }

    public function updateAdvfirefighting()
    {        
        $advfirefighting = AdvFireFighting::findById($this->id[18]);
        if ($this->validate()) {
            $advfirefighting->number = $this->number[9];
            $advfirefighting->iss_date = $this->iss_date[17];
            $advfirefighting->exp_date = $this->exp_date[17];
            $advfirefighting->iss_by = $this->iss_by[14];
            if ($advfirefighting->save()) {
                return $advfirefighting;
            }
        }

        return null;
    }
}

