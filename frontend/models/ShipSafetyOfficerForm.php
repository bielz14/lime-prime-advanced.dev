<?php
namespace frontend\models;

use Yii;
use yii\base\Model;
use frontend\models\ShipSafetyOfficer;

class ShipSafetyOfficerForm extends Model
{
    public $id;
    public $number;
    public $iss_date;
    public $exp_date;
    public $iss_by;

    public function rules()
    {
        return [
            ['id', 'each', 'rule' => ['integer']],
            
            ['number', 'each', 'rule' => ['string']],
            
            ['iss_date', 'each', 'rule' => ['string']],
            
            ['exp_date', 'each', 'rule' => ['string']],
            
            ['iss_by', 'each', 'rule' => ['string']],
        ];
    }

    public function addShipsafetyofficer()
    {        
        $shipsafetyofficer = new ShipSafetyOfficer();
        if ($this->validate()) {
            $shipsafetyofficer->number = $this->number[31];
            $shipsafetyofficer->iss_date = $this->iss_date[37];
            $shipsafetyofficer->exp_date = $this->exp_date[37];
            $shipsafetyofficer->iss_by = $this->iss_by[36];
            if ($shipsafetyofficer->save()) {
                return $shipsafetyofficer;
            }
        }

        return null;
    }

    public function updateShipsafetyofficer()
    {        
        $shipsafetyofficer = ShipSafetyOfficer::findById($this->id[39]);
        if ($this->validate()) {
            $shipsafetyofficer->number = $this->number[30];
            $shipsafetyofficer->iss_date = $this->iss_date[38];
            $shipsafetyofficer->exp_date = $this->exp_date[38];
            $shipsafetyofficer->iss_by = $this->iss_by[35];
            if ($shipsafetyofficer->save()) {
                return $shipsafetyofficer;
            }
        }

        return null;
    }
}

