<?php
namespace frontend\models;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use common\models\User;

/**
 * Resume model
 *
 * @property integer $id
 * @property integer $created_at
 * @property integer $salary
 * @property integer $application_for_position
 * @property integer $other_position
 * @property string $surname
 * @property string $lastname
 * @property string $other_names
 * @property string $sex
 * @property integer $date_of_birth
 * @property string $place_of_birth
 * @property string $citizenship
 * @property string $marital_status
 * @property string $color_of_eyes
 * @property string $color_of_hair
 * @property integer $height
 * @property integer $weight
 * @property integer $boilersuit_size
 * @property integer $boots_size
 * @property string $languague
 * @property string $level
 * @property string $country
 * @property string $city
 * @property string $post_code
 * @property string $mobile
 * @property string $email
 * @property string $skype_name
 * @property boolean $telegram
 * @property boolean $viber
 * @property boolean $whatsapp
 * @property string $next_of_kin
 * @property string $kin_adress
 * @property string $kin_mobile
 * @property integer $travel_passport_id
 * @property integer $seaman_book_id
 * @property integer $us_visa_id
 * @property integer $other_visas_id
 * @property integer $other_seaman_book_ids
 * @property string $school
 * @property integer $school_from
 * @property integer $school_to
 * @property integer $petroleum_id
 * @property integer $chemical_id
 * @property integer $gas_id
 * @property integer $certificate_ids
 * @property integer $certificate_international_id
 * @property integer $certificate_liberian_id
 * @property integer $certificate_norwegian_id
 * @property integer $certificate_panamanian_id
 * @property integer $certificate_yellow_fever_id
 * @property integer $certificate_health_list_id
 * @property integer $certificate_drug_test_id
 * @property integer $training_instructions_id
 * @property integer $basic_fire_fighting_id
 * @property integer $adv_fire_fighting_id
 * @property integer $elementary_first_aid_id
 * @property integer $medical_first_aid_id
 * @property integer $medical_care_id
 * @property integer $pers_safety_resp_id
 * @property integer $craft_rescue_id
 * @property integer $gmdsss_id
 * @property integer $management_level_id
 * @property integer $ecdis_id
 * @property integer $radar_observation_id
 * @property integer $hazmat_id
 * @property integer $oil_tanker_id
 * @property integer $advanced_oil_tanker_id
 * @property integer $chemical_tanker_id
 * @property integer $gas_tanker_id
 * @property integer $adnvance_gas_tanker_id
 * @property integer $crude_oil_washing_id
 * @property integer $inert_gas_plant_id
 * @property integer $ism_code_id
 * @property integer $ship_security_officer_id
 * @property integer $ship_safety_officer_id
 * @property integer $bridge_team_management_id
 * @property integer $dp_induction_id
 * @property integer $bridge_engine_room_resource_management_id
 * @property integer $ship_handling_id
 * @property integer $internal_auditors_course_id
 * @property integer $training_for_seafarers_id
 * @property integer $secuirity_awareness_id
 * @property integer $electical_electronic_id
 * @property integer $new_building_id
 * @property integer $specialised_projects_id
 * @property integer $special_trades_id
 * @property integer $shore_experience_id
 * @property string  $service_details_ids
 * @property string  $reference_details_ids
 */
class Resume extends ActiveRecord implements IdentityInterface
{
    public static function tableName()
    {
        return '{{%resume}}';
    }

    public function getId()
    {
        return $this->getPrimaryKey();
    }

    public function getAuthKey()
    {
        return $this->auth_key;
    }

    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    public static function findById($id)
    {
        return static::findOne(['id' => $id]);
    }

    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }

    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    public function getOwner()
    {
        return $this->hasOne(User::className(), ['id' => 'owner_id']);
    }

    /*public function getTravellpassport()
    {
        return $this->hasOne(TravelPassport::className(), ['travel_passport_id' => 'id']);
    }

    public function getSeamanbook()
    {
        return $this->hasOne(SeamanBook::className(), ['seaman_book_id' => 'id']);
    }

    public function getUsvisa()
    {
        return $this->hasOne(UsVisa::className(), ['us_visa_id' => 'id']);
    }

    public function getOthervisas()
    {
        return $this->hasOne(OtherVisas::className(), ['other_visas_id' => 'id']);
    }

    public function getPtroleum()
    {
        return $this->hasOne(Ptroleum::className(), ['ptroleum_id' => 'id']);
    }

    public function getChemical()
    {
        return $this->hasOne(Chemical::className(), ['chemical_id' => 'id']);
    }

    public function getGas()
    {
        return $this->hasOne(Gas::className(), ['gas_id' => 'id']);
    }

    public function getCertificateinternational()
    {
        return $this->hasOne(CertificateInternational::className(), ['certificate_international_id' => 'id']);
    }

    public function getCertificateliberian()
    {
        return $this->hasOne(CertificateLiberian::className(), ['certificate_liberian_id' => 'id']);
    }

    public function getCertificatenorwegian()
    {
        return $this->hasOne(CertificateNorwegian::className(), ['certificate_norwegian_id' => 'id']);
    }

    public function getCertificatepanamanian()
    {
        return $this->hasOne(CertificatePanamanian::className(), ['certificate_panamanian_id' => 'id']);
    }

    public function getCertificateyellowfever()
    {
        return $this->hasOne(CertificateYellowFever::className(), ['certificate_yellow_fever_id' => 'id']);
    }

    public function getCertificatehealthlist()
    {
        return $this->hasOne(CertificateHealthList::className(), ['certificate_health_list_id' => 'id']);
    }

    public function getCertificatedrugtest()
    {
        return $this->hasOne(CertificateDrugTest::className(), ['certificate_drug_test_id' => 'id']);
    }

    public function getTraininginstructions()
    {
        return $this->hasOne(TrainingInstructions::className(), ['training_instructions_id' => 'id']);
    }

    public function getBasicfirefighting()
    {
        return $this->hasOne(BasicFireFighting::className(), ['basic_fire_fighting_id' => 'id']);
    }

    public function getAdvfirefighting()
    {
        return $this->hasOne(AdvFireFighting::className(), ['adv_fire_fighting_id' => 'id']);
    }

    public function getElementaryfirstaid()
    {
        return $this->hasOne(ElementaryFirstAid::className(), ['elementary_first_aid_id' => 'id']);
    }

    public function getMedicalfirstaid()
    {
        return $this->hasOne(MedicalFirstAid::className(), ['medical_first_aid_id' => 'id']);
    }


    public function getMedicalcare()
    {
        return $this->hasOne(MedicalCare::className(), ['medical_care_id' => 'id']);
    }

    public function getPerssafetyresp()
    {
        return $this->hasOne(PersSafetyResp::className(), ['pers_safety_resp_id' => 'id']);
    }

    public function getCraftrescue()
    {
        return $this->hasOne(CraftRescue::className(), ['craft_rescue_id' => 'id']);
    }

    public function getFastrescuecraft()
    {
        return $this->hasOne(FastRescueCraft::className(), ['fast_rescue_craft_id' => 'id']);
    }

    public function getGmdsss()
    {
        return $this->hasOne(Gmdsss::className(), ['gmdsss_id' => 'id']);
    }

    public function getManagementlevel()
    {
        return $this->hasOne(ManagementLevel::className(), ['management_level_id' => 'id']);
    }

    public function getEcdis()
    {
        return $this->hasOne(Ecdis::className(), ['ecdis_id' => 'id']);
    }

    public function getRadarobservation()
    {
        return $this->hasOne(RadarObservation::className(), ['radar_observation_id' => 'id']);
    }

    public function getHazmat()
    {
        return $this->hasOne(Hazmat::className(), ['hazmat_id' => 'id']);
    }

    public function getOiltanker()
    {
        return $this->hasOne(OilTanker::className(), ['oil_tanker_id' => 'id']);
    }

    public function getAdvanceoiltanker()
    {
        return $this->hasOne(AdvanceOilTanker::className(), ['advance_oil_tanker_id' => 'id']);
    }

    public function getChemicaltanker()
    {
        return $this->hasOne(ChemicalTanker::className(), ['chemical_tanker_id' => 'id']);
    }

    public function getGastanker()
    {
        return $this->hasOne(GasTanker::className(), ['gas_tanker_id' => 'id']);
    }

    public function getAdvancegastanker()
    {
        return $this->hasOne(AdvanceGasTanker::className(), ['advance_gas_tanker_id' => 'id']);
    }

    public function getCrudeoilwashing()
    {
        return $this->hasOne(CrudeOilWashing::className(), ['crude_oil_washing_id' => 'id']);
    }

    public function getInertgasplant()
    {
        return $this->hasOne(InertGasPlant::className(), ['inert_gas_plant_id' => 'id']);
    }

    public function getIsmcode()
    {
        return $this->hasOne(IsmCode::className(), ['ism_code_id' => 'id']);
    }

    public function getShipsecurityofficer()
    {
        return $this->hasOne(ShipSecurityOfficer::className(), ['ship_security_officer_id' => 'id']);
    }

    public function getShipsafetyofficer()
    {
        return $this->hasOne(ShipSafetyOfficer::className(), ['shipS_safety_officer_id' => 'id']);
    }

    public function getBridgeteammanagement()
    {
        return $this->hasOne(BridgeTeamManagement::className(), ['bridge_team_management_id' => 'id']);
    }

    public function getDpinduction()
    {
        return $this->hasOne(DpInduction::className(), ['dp_induction_id' => 'id']);
    }

    public function getDpsimulator()
    {
        return $this->hasOne(DpSimulator::className(), ['dp_simulator_id' => 'id']);
    }

    public function getBridgeengineroomresourcemanagement()
    {
        return $this->hasOne(BridgeEngineRoomResourceManagement::className(), ['bridge_engine_room_resource_management_id' => 'id']);
    }

    public function getShiphandling()
    {
        return $this->hasOne(ShipHandling::className(), ['ship_handling_id' => 'id']);
    }

    public function getInternalauditorscourse()
    {
        return $this->hasOne(InternalAuditorsCourse::className(), ['internal_auditors_course_id' => 'id']);
    }

    public function getTrainingforseafarers()
    {
        return $this->hasOne(TrainingForSeafarers::className(), ['training_for_seafarers_id' => 'id']);
    }

    public function getSecurityawareness()
    {
        return $this->hasOne(SecurityAwareness::className(), ['security_awareness_id' => 'id']);
    }

    public function getElecticalelectronic()
    {
        return $this->hasOne(ElecticalElectronic::className(), ['electical_electronic_id' => 'id']);
    }

    public function getNewbuilding()
    {
        return $this->hasOne(NewBuilding::className(), ['new_building_id' => 'id']);
    }

    public function getSpecialisedprojects()
    {
        return $this->hasOne(SpecialisedProjects::className(), ['specialised_projects_id' => 'id']);
    }

    public function getSpecialtrades()
    {
        return $this->hasOne(SpecialTrades::className(), ['special_trades_id' => 'id']);
    }

    public function getServicedetails()
    {
        return $this->hasOne(ServiceDetails::className(), ['service_details_id' => 'id']);
    }

    public function getReferencedetails()
    {
        return $this->hasOne(ReferenceDetails::className(), ['reference_details_id' => 'id']);
    }*/
}   

