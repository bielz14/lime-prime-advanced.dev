<?php
namespace frontend\models;

use Yii;
use yii\base\Model;

/**
 * Login form
 */
class QuestionListForm extends Model
{
    public $id;
    public $title;
    public $category_index;

    private $_questionList;


    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['id', 'integer'],
            ['title', 'required'],
            ['title', 'string'],
            ['category_index', 'required'],
            ['category_index', 'integer'],
        ];
    }

    protected function getQuestionList()
    {
        if ($this->_questionList === null) {
            $this->_questionList = QuestionList::findById($this->id);
        }

        return $this->_questionList;
    }

    public function addQuestionList()
    {
        if (!$this->validate()) {
            return null;
        }
        
        $questionList = new QuestionList();
        $questionList->title = $this->title;
        $questionList->category_index = $this->category_index;
        $questionList->author_id = Yii::$app->user->identity->id;
        $questionLists = QuestionList::find()->all();

        return $questionList->save() ? $questionLists : null;
    }

    public function updateQuestionList()
    {   
        $questionList = QuestionList::findById($this->id);
        if ($this->validate() && ($questionList->author_id == Yii::$app->user->identity->id || isset(Yii::$app->authManager->getRolesByUser(Yii::$app->user->identity->id)['admin']))) {
            $questionList->title = $this->title;
            $questionList->category_index = $this->category_index;
            if ($questionList->save()) {
                return $questionList;
            }
        }
        
        return null;
    }
}
