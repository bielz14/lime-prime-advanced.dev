<?php
namespace frontend\models;

use Yii;
use yii\base\Model;
use frontend\models\InternalAuditorsCourse;

class InternalAuditorsCourseForm extends Model
{
    public $id;
    public $number;
    public $iss_date;
    public $exp_date;
    public $iss_by;

    public function rules()
    {
        return [
            ['id', 'each', 'rule' => ['integer']],
            
            ['number', 'each', 'rule' => ['string']],
            
            ['iss_date', 'each', 'rule' => ['string']],
            
            ['exp_date', 'each', 'rule' => ['string']],
            
            ['iss_by', 'each', 'rule' => ['string']],
        ];
    }

    public function addInternalauditorscourse()
    {        
        $internalauditorscourse = new InternalAuditorsCourse();
        if ($this->validate()) {
            $internalauditorscourse->number = $this->number[37];
            $internalauditorscourse->iss_date = $this->iss_date[43];
            $internalauditorscourse->exp_date = $this->exp_date[43];
            $internalauditorscourse->iss_by = $this->iss_by[42];
            if ($internalauditorscourse->save()) {
                return $internalauditorscourse;
            }
        }

        return null;
    }

    public function updateInternalauditorscourse()
    {        
        $internalauditorscourse = InternalAuditorsCourse::findById($this->id[45]);
        if ($this->validate()) {
            $internalauditorscourse->number = $this->number[36];
            $internalauditorscourse->iss_date = $this->iss_date[44];
            $internalauditorscourse->exp_date = $this->exp_date[44];
            $internalauditorscourse->iss_by = $this->iss_by[41];
            if ($internalauditorscourse->save()) {
                return $internalauditorscourse;
            }
        }

        return null;
    }
}

