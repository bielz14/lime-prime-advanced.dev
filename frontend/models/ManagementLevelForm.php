<?php
namespace frontend\models;

use Yii;
use yii\base\Model;
use frontend\models\ManagementLevel;

class ManagementLevelForm extends Model
{
    public $id;
    public $number;
    public $iss_date;
    public $exp_date;
    public $iss_by;

    public function rules()
    {
        return [
            ['id', 'each', 'rule' => ['integer']],
            
            ['number', 'each', 'rule' => ['string']],
            
            ['iss_date', 'each', 'rule' => ['string']],
            
            ['exp_date', 'each', 'rule' => ['string']],
            
            ['iss_by', 'each', 'rule' => ['string']],
        ];
    }

    public function addManagementlevel()
    {        
        $managementlevel = new ManagementLevel();
        if ($this->validate()) {
            $managementlevel->number = $this->number[18];
            $managementlevel->iss_date = $this->iss_date[24];
            $managementlevel->exp_date = $this->exp_date[24];
            $managementlevel->iss_by = $this->iss_by[23];
            if ($managementlevel->save()) {
                return $managementlevel;
            }
        }

        return null;
    }

    public function updateManagementlevel()
    {        
        $managementlevel = ManagementLevel::findById($this->id[26]);
        if ($this->validate()) {
            $managementlevel->number = $this->number[17];
            $managementlevel->iss_date = $this->iss_date[25];
            $managementlevel->exp_date = $this->exp_date[25];
            $managementlevel->iss_by = $this->iss_by[22];
            if ($managementlevel->save()) {
                return $managementlevel;
            }
        }

        return null;
    }
}

