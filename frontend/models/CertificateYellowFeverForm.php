<?php
namespace frontend\models;

use Yii;
use yii\base\Model;
use frontend\models\CertificateYellowFever;

class CertificateYellowFeverForm extends Model
{
    public $id;
    public $iss_date;
    public $exp_date;
    public $iss_by;
    public $iss_at;

    public function rules()
    {
        return [
            ['id', 'each', 'rule' => ['integer']],
            
            ['iss_date', 'each', 'rule' => ['string']],
            
            ['exp_date', 'each', 'rule' => ['string']],
            
            ['iss_by', 'each', 'rule' => ['string']],
            
            ['iss_at', 'each', 'rule' => ['string']],
        ];
    }

    public function addCertificateyellowfever()
    {        
        $certificateyellowfever = new CertificateYellowFever();
        if ($this->validate()) {
            $certificateyellowfever->iss_date = $this->iss_date[13];
            $certificateyellowfever->exp_date = $this->exp_date[13];
            $certificateyellowfever->iss_by = $this->iss_by[10];
            $certificateyellowfever->iss_at = $this->iss_at[0];
            if ($certificateyellowfever->save()) {
                return $certificateyellowfever;
            }
        }

        return null;
    }

    public function updateCertificateyellowfever()
    {        
        $certificateyellowfever = CertificateYellowFever::findById($this->id[13]);
        if ($this->validate()) {
            $certificateyellowfever->iss_date = $this->iss_date[12];
            $certificateyellowfever->exp_date = $this->exp_date[12];
            $certificateyellowfever->iss_by = $this->iss_by[9];
            $certificateyellowfever->iss_at = $this->iss_at[0];
            if ($certificateyellowfever->save()) {
                return $certificateyellowfever;
            }
        }

        return null;
    }
}

