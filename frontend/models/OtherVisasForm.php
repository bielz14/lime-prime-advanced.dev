<?php
namespace frontend\models;

use Yii;
use yii\base\Model;
use frontend\models\OtherVisas;

class OtherVisasForm extends Model
{
    public $id;
    public $document_no;
    public $iss_date;
    public $exp_date;
    public $iss_by;
    public $place_of_issue;

    public function rules()
    {
        return [
            ['id', 'each', 'rule' => ['integer']],
            
            ['document_no', 'each', 'rule' => ['string']],
            
            ['iss_date', 'each', 'rule' => ['string']],
            
            ['exp_date', 'each', 'rule' => ['string']],
            
            ['iss_by', 'each', 'rule' => ['string']],
            
            ['place_of_issue', 'each', 'rule' => ['string']],
        ];
    }

    public function addOthervisas()
    {        
        $othervisas = new OtherVisas();
        if ($this->validate()) {
            $othervisas->document_no = $this->document_no[3];
            $othervisas->iss_date = $this->iss_date[3];
            $othervisas->exp_date = $this->exp_date[3];
            $othervisas->iss_by = $this->iss_by[3];
            $othervisas->place_of_issue = $this->place_of_issue[3];
            if ($othervisas->save()) {
                return $othervisas;
            }
        }

        return null;
    }

    public function updateOthervisas()
    {        
        $othervisas = OtherVisas::findById($this->id[4]);
        if ($this->validate()) {
            $othervisas->document_no = $this->document_no[3];
            $othervisas->iss_date = $this->iss_date[3];
            $othervisas->exp_date = $this->exp_date[3];
            $othervisas->iss_by = $this->iss_by[3];
            $othervisas->place_of_issue = $this->place_of_issue[3];
            if ($othervisas->save()) {
                return $othervisas;
            }
        }

        return null;
    }
}

