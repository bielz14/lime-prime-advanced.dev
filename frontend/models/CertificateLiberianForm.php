<?php
namespace frontend\models;

use Yii;
use yii\base\Model;
use frontend\models\CertificateLiberian;

class CertificateLiberianForm extends Model
{
    public $id;
    public $number;
    public $iss_date;
    public $exp_date;
    public $iss_by;

    public function rules()
    {
        return [
            ['id', 'each', 'rule' => ['integer']],
            
            ['number', 'each', 'rule' => ['string']],
            
            ['iss_date', 'each', 'rule' => ['string']],
            
            ['exp_date', 'each', 'rule' => ['string']],
            
            ['iss_by', 'each', 'rule' => ['string']],
        ];
    }

    public function addCertificateliberian()
    {        
        $certificateliberian = new CertificateLiberian();
        if ($this->validate()) {
            $certificateliberian->number = $this->number[5];
            $certificateliberian->iss_date = $this->iss_date[8];
            $certificateliberian->exp_date = $this->exp_date[8];
            $certificateliberian->iss_by = $this->iss_by[7];
            if ($certificateliberian->save()) {
                return $certificateliberian;
            }
        }

        return null;
    }

    public function updateCertificateliberian()
    {        
        $certificateliberian = CertificateLiberian::findById($this->id[10]);
        if ($this->validate()) {
            $certificateliberian->number = $this->number[4];
            $certificateliberian->iss_date = $this->iss_date[7];
            $certificateliberian->exp_date = $this->exp_date[7];
            $certificateliberian->iss_by = $this->iss_by[6];
            if ($certificateliberian->save()) {
                return $certificateliberian;
            }
        }

        return null;
    }
}

