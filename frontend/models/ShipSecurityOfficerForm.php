<?php
namespace frontend\models;

use Yii;
use yii\base\Model;
use frontend\models\ShipSecurityOfficer;

class ShipSecurityOfficerForm extends Model
{
    public $id;
    public $number;
    public $iss_date;
    public $exp_date;
    public $iss_by;

    public function rules()
    {
        return [
            ['id', 'each', 'rule' => ['integer']],
            
            ['number', 'each', 'rule' => ['string']],
            
            ['iss_date', 'each', 'rule' => ['string']],
            
            ['exp_date', 'each', 'rule' => ['string']],
            
            ['iss_by', 'each', 'rule' => ['string']],
        ];
    }

    public function addShipsecurityofficer()
    {        
        $shipsecurityofficer = new ShipSecurityOfficer();
        if ($this->validate()) {
            $shipsecurityofficer->number = $this->number[30];
            $shipsecurityofficer->iss_date = $this->iss_date[36];
            $shipsecurityofficer->exp_date = $this->exp_date[36];
            $shipsecurityofficer->iss_by = $this->iss_by[35];
            if ($shipsecurityofficer->save()) {
                return $shipsecurityofficer;
            }
        }

        return null;
    }

    public function updateShipsecurityofficer()
    {        
        $shipsecurityofficer = ShipSecurityOfficer::findById($this->id[38]);
        if ($this->validate()) {
            $shipsecurityofficer->number = $this->number[29];
            $shipsecurityofficer->iss_date = $this->iss_date[37];
            $shipsecurityofficer->exp_date = $this->exp_date[37];
            $shipsecurityofficer->iss_by = $this->iss_by[34];
            if ($shipsecurityofficer->save()) {
                return $shipsecurityofficer;
            }
        }

        return null;
    }
}

