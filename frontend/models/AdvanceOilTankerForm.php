<?php
namespace frontend\models;

use Yii;
use yii\base\Model;
use frontend\models\AdvanceOilTanker;

class AdvanceOilTankerForm extends Model
{
    public $id;
    public $number;
    public $iss_date;
    public $exp_date;
    public $iss_by;

    public function rules()
    {
        return [
            ['id', 'each', 'rule' => ['integer']],
            
            ['number', 'each', 'rule' => ['string']],
            
            ['iss_date', 'each', 'rule' => ['string']],
            
            ['exp_date', 'each', 'rule' => ['string']],
            
            ['iss_by', 'each', 'rule' => ['string']],
        ];
    }

    public function addAdvanceoiltanker()
    {        
        $advanceoiltanker = new AdvanceOilTanker();
        if ($this->validate()) {
            $advanceoiltanker->number = $this->number[23];
            $advanceoiltanker->iss_date = $this->iss_date[29];
            $advanceoiltanker->exp_date = $this->exp_date[29];
            $advanceoiltanker->iss_by = $this->iss_by[28];
            if ($advanceoiltanker->save()) {
                return $advanceoiltanker;
            }
        }

        return null;
    }

    public function updateAdvanceoiltanker()
    {        
        $advanceoiltanker = AdvanceOilTanker::findById($this->id[31]);
        if ($this->validate()) {
            $advanceoiltanker->number = $this->number[22];
            $advanceoiltanker->iss_date = $this->iss_date[30];
            $advanceoiltanker->exp_date = $this->exp_date[30];
            $advanceoiltanker->iss_by = $this->iss_by[27];
            if ($advanceoiltanker->save()) {
                return $advanceoiltanker;
            }
        }

        return null;
    }
}

