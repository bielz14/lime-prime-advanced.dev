<?php
namespace frontend\models;

use Yii;
use yii\base\Model;
use frontend\models\MedicalCare;

class MedicalCareForm extends Model
{
    public $id;
    public $number;
    public $iss_date;
    public $exp_date;
    public $iss_by;

    public function rules()
    {
        return [
            ['id', 'each', 'rule' => ['integer']],
            
            ['number', 'each', 'rule' => ['string']],
            
            ['iss_date', 'each', 'rule' => ['string']],
            
            ['exp_date', 'each', 'rule' => ['string']],
            
            ['iss_by', 'each', 'rule' => ['string']],
        ];
    }

    public function addMedicalcare()
    {        
        $medicalcare = new MedicalCare();
        if ($this->validate()) {
            $medicalcare->number = $this->number[13];
            $medicalcare->iss_date = $this->iss_date[19];
            $medicalcare->exp_date = $this->exp_date[19];
            $medicalcare->iss_by = $this->iss_by[18];
            if ($medicalcare->save()) {
                return $medicalcare;
            }
        }

        return null;
    }

    public function updateMedicalcare()
    {    
        $medicalcare = MedicalCare::findById($this->id[21]);
        if ($this->validate()) {
            $medicalcare->number = $this->number[12];
            $medicalcare->iss_date = $this->iss_date[20];
            $medicalcare->exp_date = $this->exp_date[20];
            $medicalcare->iss_by = $this->iss_by[17];
            if ($medicalcare->save()) {
                return $medicalcare;
            }
        }

        return null;
    }
}

