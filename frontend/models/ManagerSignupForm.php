<?php
namespace frontend\models;

use yii\base\Model;
use common\models\User;

/**
 * Signup form
 */
class ManagerSignupForm extends Model
{
    public $firstname;
    public $lastname;
    public $company;
    public $phone;
    public $email;
    public $password;
    public $password_repeat;


    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['firstname', 'trim'],
            ['firstname', 'required'],
            ['firstname', 'string', 'min' => 2, 'max' => 255],

            ['lastname', 'trim'],
            ['lastname', 'required'],
            ['lastname', 'string', 'min' => 2, 'max' => 255],

            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This email address has already been taken.'],

            ['password', 'required'],
            ['password', 'string', 'min' => 6],

            ['company', 'trim'],
            ['company', 'required'],
            ['company', 'string', 'min' => 2, 'max' => 255],

            ['phone', 'trim'],
            ['phone', 'required'],
            ['phone', 'string', 'min' => 2, 'max' => 255],
        ];
    }

    /**
     * Signs manager up.
     *
     * @return Manager|null the saved model or null if saving fails
     */
    public function signup()
    {
        if (!$this->validate()) {
            return null;
        }

        $manager = new User();
        $manager->firstname = $this->firstname;
        $manager->lastname = $this->lastname;
        $manager->company = $this->company;
        $manager->phone = $this->phone;
        $manager->email = $this->email;
        $manager->setPassword($this->password);
        $manager->generateAuthKey();
        
        if ($manager->save()) {
            $auth = \Yii::$app->authManager;
            $role = $auth->getRole('manager');
            $auth->assign($role, $manager->id);
            return $manager;
        }
        return null;
    }
}
