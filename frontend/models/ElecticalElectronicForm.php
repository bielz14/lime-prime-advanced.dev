<?php
namespace frontend\models;

use Yii;
use yii\base\Model;
use frontend\models\ElecticalElectronic;

class ElecticalElectronicForm extends Model
{
    public $id;
    public $number;
    public $iss_date;
    public $exp_date;
    public $iss_by;

    public function rules()
    {
        return [
            ['id', 'each', 'rule' => ['integer']],
            
            ['number', 'each', 'rule' => ['string']],
            
            ['iss_date', 'each', 'rule' => ['string']],
            
            ['exp_date', 'each', 'rule' => ['string']],
            
            ['iss_by', 'each', 'rule' => ['string']],
        ];
    }

    public function addElecticalelectronic()
    {        
        $electicalelectronic = new ElecticalElectronic();
        if ($this->validate()) {
            $electicalelectronic->number = $this->number[40];
            $electicalelectronic->iss_date = $this->iss_date[46];
            $electicalelectronic->exp_date = $this->exp_date[46];
            $electicalelectronic->iss_by = $this->iss_by[45];
            if ($electicalelectronic->save()) {
                return $electicalelectronic;
            }
        }

        return null;
    }

    public function updateElecticalelectronic()
    {        
        $electicalelectronic = ElecticalElectronic::findById($this->id[48]);
        if ($this->validate()) {
            $electicalelectronic->number = $this->number[39];
            $electicalelectronic->iss_date = $this->iss_date[47];
            $electicalelectronic->exp_date = $this->exp_date[47];
            $electicalelectronic->iss_by = $this->iss_by[44];
            if ($electicalelectronic->save()) {
                return $electicalelectronic;
            }
        }

        return null;
    }
}

