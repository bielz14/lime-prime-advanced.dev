<?php
namespace frontend\models;

use Yii;
use yii\base\Model;

/**
 * Login form
 */
class QuestionForm extends Model
{
    public $id;
    public $content;
    public $list_id;
    public $position;
    public $time;

    private $_question;


    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['id', 'integer'],
            ['content', 'required'],
            ['content', 'string'],
            ['list_id', 'integer'],
            ['position', 'integer'],
            ['position', 'required'],
            ['time', 'integer'],
            ['time', 'required'],
        ];
    }

    protected function getQuestion()
    {
        if ($this->_question === null) {
            $this->_question = Question::findById($this->id);
        }

        return $this->_question;
    }

    public function addQuestion()
    {        
        $question = new Question();
        $questionList = QuestionList::findOne($this->list_id);
        if ($this->validate() && ($questionList->author_id == Yii::$app->user->identity->id || isset(Yii::$app->authManager->getRolesByUser(Yii::$app->user->identity->id)['admin']))) {
            $question->content = $this->content;
            $question->position = $this->position;
            $question->time = $this->time;
            if ($this->list_id) {
                $question->list_id = $this->list_id;
            }
            if ($question->save()) {
                return $question;
            }
        }

        return null;
    }

    public function updateQuestion()
    {   
        $question = Question::findById($this->id);
        if ($this->validate() && ($question->questionList->author_id == Yii::$app->user->identity->id || isset(Yii::$app->authManager->getRolesByUser(Yii::$app->user->identity->id)['admin']))) {
            $question->content = $this->content;
            $question->position = $this->position;
            $question->time = $this->time;
            if ($question->save()) {
                return $question;
            }
        }
        
        return null;
    }
}
