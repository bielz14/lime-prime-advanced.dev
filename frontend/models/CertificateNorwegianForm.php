<?php
namespace frontend\models;

use Yii;
use yii\base\Model;
use frontend\models\CertificateNorwegian;

class CertificateNorwegianForm extends Model
{
    public $id;
    public $number;
    public $iss_date;
    public $exp_date;
    public $iss_by;

    public function rules()
    {
        return [
            ['id', 'each', 'rule' => ['integer']],
            
            ['number', 'each', 'rule' => ['string']],
            
            ['iss_date', 'each', 'rule' => ['string']],
            
            ['exp_date', 'each', 'rule' => ['string']],
            
            ['iss_by', 'each', 'rule' => ['string']],
        ];
    }

    public function addCertificatenorwegian()
    {        
        $certificatenorwegian = new CertificateNorwegian();
        if ($this->validate()) {
            $certificatenorwegian->number = $this->number[6];
            $certificatenorwegian->iss_date = $this->iss_date[9];
            $certificatenorwegian->exp_date = $this->exp_date[9];
            $certificatenorwegian->iss_by = $this->iss_by[8];
            if ($certificatenorwegian->save()) {
                return $certificatenorwegian;
            }
        }

        return null;
    }

    public function updateCertificatenorwegian()
    {        
        $certificatenorwegian = CertificateNorwegian::findById($this->id[11]);
        if ($this->validate()) {
            $certificatenorwegian->number = $this->number[5];
            $certificatenorwegian->iss_date = $this->iss_date[8];
            $certificatenorwegian->exp_date = $this->exp_date[8];
            $certificatenorwegian->iss_by = $this->iss_by[9];
            if ($certificatenorwegian->save()) {
                return $certificatenorwegian;
            }
        }

        return null;
    }
}

