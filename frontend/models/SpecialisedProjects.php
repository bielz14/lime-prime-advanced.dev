<?php
namespace frontend\models;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * Resume model
 *
 * @property integer $id
 * @property string  $exp_from
 * @property string  $exp_to
 * @property string  $comments
 */
class SpecialisedProjects extends ActiveRecord implements IdentityInterface
{
    public static function tableName()
    {
        return '{{%specialised_projects}}';
    }

    public function getId()
    {
        return $this->getPrimaryKey();
    }

    public function getAuthKey()
    {
        return $this->auth_key;
    }

    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    public static function findById($id)
    {
        return static::findOne(['id' => $id]);
    }

    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }

    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    public function getResume()
    {
        return $this->hasMany(Resume::className(), ['id' => 'specialised_project_id']);
    }
}