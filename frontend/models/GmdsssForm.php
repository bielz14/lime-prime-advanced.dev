<?php
namespace frontend\models;

use Yii;
use yii\base\Model;
use frontend\models\Gmdsss;

class GmdsssForm extends Model
{
    public $id;
    public $number;
    public $iss_date;
    public $exp_date;
    public $iss_by;

    public function rules()
    {
        return [
            ['id', 'each', 'rule' => ['integer']],
            
            ['number', 'each', 'rule' => ['string']],
            
            ['iss_date', 'each', 'rule' => ['string']],
            
            ['exp_date', 'each', 'rule' => ['string']],
            
            ['iss_by', 'each', 'rule' => ['string']],
        ];
    }

    public function addGmdsss()
    {        
        $gmdsss = new Gmdsss();
        if ($this->validate()) {
            $gmdsss->number = $this->number[17];
            $gmdsss->iss_date = $this->iss_date[23];
            $gmdsss->exp_date = $this->exp_date[23];
            $gmdsss->iss_by = $this->iss_by[22];
            if ($gmdsss->save()) {
                return $gmdsss;
            }
        }

        return null;
    }

    public function updateGmdsss()
    {        
        $gmdsss = Gmdsss::findById($this->id[25]);
        if ($this->validate()) {
            $gmdsss->number = $this->number[16];
            $gmdsss->iss_date = $this->iss_date[24];
            $gmdsss->exp_date = $this->exp_date[24];
            $gmdsss->iss_by = $this->iss_by[21];
            if ($gmdsss->save()) {
                return $gmdsss;
            }
        }

        return null;
    }
}

