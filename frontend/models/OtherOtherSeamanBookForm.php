<?php
namespace frontend\models;

use Yii;
use yii\base\Model;
use frontend\models\OtherSeamanBook;

class OtherOtherSeamanBookForm extends Model
{
    public $other_otherseamanbook_document_no;
    public $other_otherseamanbook_iss_date;
    public $other_otherseamanbook_exp_date;
    public $other_otherseamanbook_iss_by;
    public $other_otherseamanbook_place_of_issue;

    public function rules()
    {
        return [            
            ['other_otherseamanbook_document_no', 'each', 'rule' => ['string']],
            
            ['other_otherseamanbook_iss_date', 'each', 'rule' => ['string']],
            
            ['other_otherseamanbook_exp_date', 'each', 'rule' => ['string']],
            
            ['other_otherseamanbook_iss_by', 'each', 'rule' => ['string']],
            
            ['other_otherseamanbook_place_of_issue', 'each', 'rule' => ['string']],
        ];
    }

    public function addOtherseamanbook($index)
    {        
        $otherseamanbook = new OtherSeamanBook();
        if ($this->validate()) {
            $otherseamanbook->document_no = $this->other_otherseamanbook_document_no[$index];
            $otherseamanbook->iss_date = $this->other_otherseamanbook_iss_date[$index];
            $otherseamanbook->exp_date = $this->other_otherseamanbook_exp_date[$index];
            $otherseamanbook->iss_by = $this->other_otherseamanbook_iss_by[$index];
            $otherseamanbook->place_of_issue = $this->other_otherseamanbook_place_of_issue[$index];
            if ($otherseamanbook->save()) {
                return $otherseamanbook;
            }
        }

        return null;
    }

    public function updateOtherseamanbook($index, $id)
    {     
        $otherseamanbook = OtherSeamanBook::findById($id);
        if ($this->validate()) {
            $otherseamanbook->document_no = $this->other_otherseamanbook_document_no[$index];
            $otherseamanbook->iss_date = $this->other_otherseamanbook_iss_date[$index];
            $otherseamanbook->exp_date = $this->other_otherseamanbook_exp_date[$index];
            $otherseamanbook->iss_by = $this->other_otherseamanbook_iss_by[$index];
            $otherseamanbook->place_of_issue = $this->other_otherseamanbook_place_of_issue[$index];
            if ($otherseamanbook->save()) {
                return $otherseamanbook;
            }
        }

        return null;
    }
}

