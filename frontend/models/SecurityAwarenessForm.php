<?php
namespace frontend\models;

use Yii;
use yii\base\Model;
use frontend\models\SecurityAwareness;

class SecurityAwarenessForm extends Model
{
    public $id;
    public $number;
    public $iss_date;
    public $exp_date;
    public $iss_by;

    public function rules()
    {
        return [
            ['id', 'each', 'rule' => ['integer']],
            
            ['number', 'each', 'rule' => ['string']],
            
            ['iss_date', 'each', 'rule' => ['string']],
            
            ['exp_date', 'each', 'rule' => ['string']],
            
            ['iss_by', 'each', 'rule' => ['string']],
        ];
    }

    public function addSecurityawareness()
    {        
        $securityawareness = new SecurityAwareness();
        if ($this->validate()) {
            $securityawareness->number = $this->number[39];
            $securityawareness->iss_date = $this->iss_date[45];
            $securityawareness->exp_date = $this->exp_date[45];
            $securityawareness->iss_by = $this->iss_by[44];
            if ($securityawareness->save()) {
                return $securityawareness;
            }
        }

        return null;
    }

    public function updateSecurityawareness()
    {        
        $securityawareness = SecurityAwareness::findById($this->id[47]);
        if ($this->validate()) {
            $securityawareness->number = $this->number[38];
            $securityawareness->iss_date = $this->iss_date[46];
            $securityawareness->exp_date = $this->exp_date[46];
            $securityawareness->iss_by = $this->iss_by[43];
            if ($securityawareness->save()) {
                return $securityawareness;
            }
        }

        return null;
    }
}

