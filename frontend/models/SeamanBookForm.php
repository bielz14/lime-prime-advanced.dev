<?php
namespace frontend\models;

use Yii;
use yii\base\Model;
use frontend\models\SeamanBook;

class SeamanBookForm extends Model
{
    public $id;
    public $document_no;
    public $iss_date;
    public $exp_date;
    public $iss_by;
    public $place_of_issue;

    public function rules()
    {
        return [
            ['id', 'each', 'rule' => ['integer']],
            
            ['document_no', 'each', 'rule' => ['string']],
            
            ['iss_date', 'each', 'rule' => ['string']],
            
            ['exp_date', 'each', 'rule' => ['string']],
            
            ['iss_by', 'each', 'rule' => ['string']],
            
            ['place_of_issue', 'each', 'rule' => ['string']],
        ];
    }

    public function addSeamanbook()
    {        
        $seamanbook = new SeamanBook();
        if ($this->validate()) {
            $seamanbook->document_no = $this->document_no[1];
            $seamanbook->iss_date = $this->iss_date[1];
            $seamanbook->exp_date = $this->exp_date[1];
            $seamanbook->iss_by = $this->iss_by[1];
            $seamanbook->place_of_issue = $this->place_of_issue[1];
            if ($seamanbook->save()) {
                return $seamanbook;
            }
        }

        return null;
    }

    public function updateSeamanbook()
    {        
        $seamanbook = SeamanBook::findById($this->id[2]);
        if ($this->validate()) {
            $seamanbook->document_no = $this->document_no[1];
            $seamanbook->iss_date = $this->iss_date[1];
            $seamanbook->exp_date = $this->exp_date[1];
            $seamanbook->iss_by = $this->iss_by[1];
            $seamanbook->place_of_issue = $this->place_of_issue[1];
            if ($seamanbook->save()) {
                return $seamanbook;
            }
        }

        return null;
    }
}

