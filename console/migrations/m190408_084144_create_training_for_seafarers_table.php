<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%training_for_seafarers}}`.
 */
class m190408_084144_create_training_for_seafarers_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%training_for_seafarers}}', [
            'id' => $this->primaryKey(),
            'number' => $this->string(),
            'iss_date' => $this->string(),
            'exp_date' => $this->string(),
            'iss_by' => $this->string(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%training_for_seafarers}}');
    }
}
