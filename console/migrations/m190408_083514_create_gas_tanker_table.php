<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%gas_tanker}}`.
 */
class m190408_083514_create_gas_tanker_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%gas_tanker}}', [
            'id' => $this->primaryKey(),
            'number' => $this->string(),
            'iss_date' => $this->string(),
            'exp_date' => $this->string(),
            'iss_by' => $this->string(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%gas_tanker}}');
    }
}
