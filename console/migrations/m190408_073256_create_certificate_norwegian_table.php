<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%certificate_norwegian}}`.
 */
class m190408_073256_create_certificate_norwegian_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%certificate_norwegian}}', [
            'id' => $this->primaryKey(),
            'number' => $this->string(),
            'iss_date' => $this->string(),
            'exp_date' => $this->string(),
            'iss_by' => $this->string(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%certificate_norwegian}}');
    }
}
