<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%elementary_first_aid}}`.
 */
class m190408_083147_create_elementary_first_aid_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%elementary_first_aid}}', [
            'id' => $this->primaryKey(),
            'number' => $this->string(),
            'iss_date' => $this->string(),
            'exp_date' => $this->string(),
            'iss_by' => $this->string(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%elementary_first_aid}}');
    }
}
