<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%new_building}}`.
 */
class m190408_084258_create_new_building_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%new_building}}', [
            'id' => $this->primaryKey(),
            'exp_from' => $this->string(),
            'exp_to' => $this->string(),
            'comments' => $this->string(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%new_building}}');
    }
}
