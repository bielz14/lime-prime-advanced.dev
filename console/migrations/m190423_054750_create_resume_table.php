<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%resume}}`.
 */
class m190423_054750_create_resume_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%resume}}', [
            'id' => $this->primaryKey(),
            'owner_id' => $this->integer()->notNull(),
            'image' => $this->string()->notNull(),
            'created_at' => $this->string(),
            'salary' => $this->integer(),
            'application_for_position' => $this->integer()->notNull(),
            'other_position' => $this->integer(),

            'surname' => $this->string()->notNull(),
            'firstname' => $this->string()->notNull(),
            'other_names' => $this->string()->notNull(),
            'sex' => $this->string(),
            'date_of_birth' => $this->string()->notNull(),
            'place_of_birth' => $this->string()->notNull(),
            'citizenship' => $this->string()->notNull(),
            'marital_status' => $this->string(),
            'color_of_eyes' => $this->string(),
            'color_of_hair' => $this->string(),
            'height' => $this->integer(),
            'weight' => $this->integer(),
            'boilersuit_size' => $this->integer(),
            'boots_size' => $this->integer(),
            'language' => $this->string()->notNull(),
            'level' => $this->string()->notNull(),

            'country' => $this->string(),
            'city' => $this->string(),
            'post_code' => $this->string(),
            'mobile' => $this->string(),
            'email' => $this->string(),
            'skype_name' => $this->string(),
            'telegram' => $this->boolean()->defaultValue(false),
            'viber' => $this->boolean()->defaultValue(false),
            'whatsapp' => $this->boolean()->defaultValue(false),
            'next_of_kin' => $this->string(),
            'kin_adress' => $this->string(),
            'kin_mobile' => $this->string(),

            'travel_passport_id' => $this->integer()->notNull(),
            'seaman_book_id' => $this->integer()->notNull(),
            'us_visa_id' => $this->integer(),
            'other_visas_id' => $this->integer(),
            'other_seaman_book_ids' => $this->string(),

            'school' => $this->string(),
            'school_from' => $this->string(),
            'school_to' => $this->string(),

            'petroleum_id' => $this->integer(),
            'chemical_id' => $this->integer(),
            'gas_id' => $this->integer(),
            'certificate_ids' => $this->string(),

            'certificate_international_id' => $this->integer(),
            'certificate_liberian_id' => $this->integer(),
            'certificate_norwegian_id' => $this->integer(),
            'certificate_panamanian_id' => $this->integer(),
            'certificate_yellow_fever_id' => $this->integer(),
            'certificate_health_list_id' => $this->integer(),
            'certificate_drug_test_id' => $this->integer(),

            'training_instructions_id' => $this->integer(),
            'basic_fire_fighting_id' => $this->integer(),
            'adv_fire_fighting_id' => $this->integer(),
            'elementary_first_aid_id' => $this->integer(),
            'medical_first_aid_id' => $this->integer(),
            'medical_care_id' => $this->integer(),
            'pers_safety_resp_id' => $this->integer(),
            'fast_rescuecraft_id' => $this->integer(),
            'craft_rescue_id' => $this->integer(),
            'gmdsss_id' => $this->integer(),
            'management_level_id' => $this->integer(),
            'ecdis_id' => $this->integer(),
            'radar_observation_id' => $this->integer(),
            'hazmat_id' => $this->integer(),
            'oil_tanker_id' => $this->integer(),
            'advance_oil_tanker_id' => $this->integer(),
            'chemical_tanker_id' => $this->integer(),
            'gas_tanker_id' => $this->integer(),
            'advance_gas_tanker_id' => $this->integer(),
            'crude_oil_washing_id' => $this->integer(),
            'inert_gas_plant_id' => $this->integer(),
            'ism_code_id' => $this->integer(),
            'ship_security_officer_id' => $this->integer(),
            'ship_safety_officer_id' => $this->integer(),
            'bridge_team_management_id' => $this->integer(),
            'dp_induction_id' => $this->integer(),
            'dp_simulator_id' => $this->integer(),
            'bridge_engine_room_resource_management_id' => $this->integer(),
            'ship_handling_id' => $this->integer(),
            'internal_auditors_course_id' => $this->integer(),
            'training_for_seafarers_id' => $this->integer(),
            'security_awareness_id' => $this->integer(),
            'electical_electronic_id' => $this->integer(),
            'other_course_ids' => $this->string(),

            'new_building_id' => $this->integer(),
            'specialised_projects_id' => $this->integer(),
            'special_trades_id' => $this->integer(),
            'shore_experience_id' => $this->integer(),

            'service_details_ids' => $this->string(),

            'reference_details_ids' => $this->string(),
        ]);
        
        $this->createIndex(
            'idx-resume-owner_id',
            'resume',
            'owner_id'
        );

        $this->addForeignKey(
            'fk-resume-owner_id',
            'resume',
            'owner_id',
            'user',
            'id',
            'CASCADE'
        );

        $this->createIndex(
            'idx-resume-travel_passport_id',
            'resume',
            'travel_passport_id'
        );

        $this->addForeignKey(
            'fk-resume-travel_passport_id',
            'resume',
            'travel_passport_id',
            'travel_passport',
            'id',
            'CASCADE'
        );

        $this->createIndex(
            'idx-resume-seaman_book_id',
            'resume',
            'seaman_book_id'
        );

        $this->addForeignKey(
            'fk-resume-seaman_book_id',
            'resume',
            'seaman_book_id',
            'seaman_book',
            'id',
            'CASCADE'
        );

        $this->createIndex(
            'idx-resume-us_visa_id',
            'resume',
            'us_visa_id'
        );

        $this->addForeignKey(
            'fk-resume-us_visa_id',
            'resume',
            'us_visa_id',
            'us_visa',
            'id',
            'CASCADE'
        );

        $this->createIndex(
            'idx-resume-other_visas_id',
            'resume',
            'other_visas_id'
        );

        $this->addForeignKey(
            'fk-resume-other_visas_id',
            'resume',
            'other_visas_id',
            'other_visas',
            'id',
            'CASCADE'
        );

        $this->createIndex(
            'idx-resume-petroleum_id',
            'resume',
            'petroleum_id'
        );

        $this->addForeignKey(
            'fk-resume-petroleum_id',
            'resume',
            'petroleum_id',
            'petroleum',
            'id',
            'CASCADE'
        );

        $this->createIndex(
            'idx-resume-chemical_id',
            'resume',
            'chemical_id'
        );

        $this->addForeignKey(
            'fk-resume-chemical_id',
            'resume',
            'chemical_id',
            'chemical',
            'id',
            'CASCADE'
        );

        $this->createIndex(
            'idx-resume-gas_id',
            'resume',
            'gas_id'
        );

        $this->addForeignKey(
            'fk-resume-gas_id',
            'resume',
            'gas_id',
            'gas',
            'id',
            'CASCADE'
        );

        /*$this->createIndex(
            'idx-resume-certificate_id',
            'resume',
            'gas_id'
        );

        $this->addForeignKey(
            'fk-resume-certificate_id',
            'resume',
            'certificate_id',
            'certificate',
            'id',
            'CASCADE'
        );*/

        $this->createIndex(
            'idx-resume-certificate_international_id',
            'resume',
            'certificate_international_id'
        );

        $this->addForeignKey(
            'fk-resume-certificate_international_id',
            'resume',
            'certificate_international_id',
            'certificate_international',
            'id',
            'CASCADE'
        );

        $this->createIndex(
            'idx-resume-certificate_liberian_id',
            'resume',
            'certificate_liberian_id'
        );

        $this->addForeignKey(
            'fk-resume-certificate_liberian_id',
            'resume',
            'certificate_liberian_id',
            'certificate_liberian',
            'id',
            'CASCADE'
        );

        $this->createIndex(
            'idx-resume-certificate_norwegian_id',
            'resume',
            'certificate_norwegian_id'
        );

        $this->addForeignKey(
            'fk-resume-certificate_norwegian_id',
            'resume',
            'certificate_norwegian_id',
            'certificate_norwegian',
            'id',
            'CASCADE'
        );

        $this->createIndex(
            'idx-resume-certificate_panamanian_id',
            'resume',
            'certificate_panamanian_id'
        );

        $this->addForeignKey(
            'fk-resume-certificate_panamanian_id',
            'resume',
            'certificate_panamanian_id',
            'certificate_panamanian',
            'id',
            'CASCADE'
        );

        $this->createIndex(
            'idx-resume-certificate_yellow_fever_id',
            'resume',
            'certificate_yellow_fever_id'
        );

        $this->addForeignKey(
            'fk-resume-certificate_yellow_fever_id',
            'resume',
            'certificate_yellow_fever_id',
            'certificate_yellow_fever',
            'id',
            'CASCADE'
        );

        $this->createIndex(
            'idx-resume-certificate_health_list_id',
            'resume',
            'certificate_health_list_id'
        );

        $this->addForeignKey(
            'fk-resume-certificate_health_list_id',
            'resume',
            'certificate_health_list_id',
            'certificate_health_list',
            'id',
            'CASCADE'
        );

        $this->createIndex(
            'idx-resume-certificate_drug_test_id',
            'resume',
            'certificate_drug_test_id'
        );

        $this->addForeignKey(
            'fk-resume-certificate_drug_test_id',
            'resume',
            'certificate_drug_test_id',
            'certificate_drug_test',
            'id',
            'CASCADE'
        );

        /*$this->createIndex(
            'idx-resume-service_details_id',
            'resume',
            'service_details_id'
        );

        $this->addForeignKey(
            'fk-resume-service_details_id',
            'resume',
            'service_details_id',
            'service_details',
            'id',
            'CASCADE'
        );

        $this->createIndex(
            'idx-resume-reference_details_id',
            'resume',
            'reference_details_id'
        );

        $this->addForeignKey(
            'fk-resume-reference_details_id',
            'resume',
            'reference_details_id',
            'reference_details',
            'id',
            'CASCADE'
        );*/

        $this->createIndex(
            'idx-resume-training_instructions_id',
            'resume',
            'training_instructions_id'
        );

        $this->addForeignKey(
            'fk-resume-training_instructions_id',
            'resume',
            'training_instructions_id',
            'training_instructions',
            'id',
            'CASCADE'
        );

        $this->createIndex(
            'idx-resume-basic_fire_fighting_id',
            'resume',
            'basic_fire_fighting_id'
        );

        $this->addForeignKey(
            'fk-resume-basic_fire_fighting_id',
            'resume',
            'basic_fire_fighting_id',
            'basic_fire_fighting',
            'id',
            'CASCADE'
        );

        $this->createIndex(
            'idx-resume-adv_fire_fighting_id',
            'resume',
            'adv_fire_fighting_id'
        );

        $this->addForeignKey(
            'fk-resume-adv_fire_fighting_id',
            'resume',
            'adv_fire_fighting_id',
            'adv_fire_fighting',
            'id',
            'CASCADE'
        );

        $this->createIndex(
            'idx-resume-elementary_first_aid_id',
            'resume',
            'elementary_first_aid_id'
        );

        $this->addForeignKey(
            'fk-resume-elementary_first_aid_id',
            'resume',
            'elementary_first_aid_id',
            'elementary_first_aid',
            'id',
            'CASCADE'
        );

        $this->createIndex(
            'idx-resume-medical_first_aid_id',
            'resume',
            'medical_first_aid_id'
        );

        $this->addForeignKey(
            'fk-resume-medical_first_aid_id',
            'resume',
            'medical_first_aid_id',
            'medical_first_aid',
            'id',
            'CASCADE'
        );

        $this->createIndex(
            'idx-resume-medical_care_id',
            'resume',
            'medical_care_id'
        );

        $this->addForeignKey(
            'fk-resume-medical_care_id',
            'resume',
            'medical_care_id',
            'medical_care',
            'id',
            'CASCADE'
        );

        $this->createIndex(
            'idx-resume-pers_safety_resp_id',
            'resume',
            'pers_safety_resp_id'
        );

        $this->addForeignKey(
            'fk-resume-pers_safety_resp_id',
            'resume',
            'pers_safety_resp_id',
            'pers_safety_resp',
            'id',
            'CASCADE'
        );
        
        $this->createIndex(
            'idx-resume-craft_rescue_id',
            'resume',
            'craft_rescue_id'
        );

        $this->addForeignKey(
            'fk-fast_rescuecraft_id',
            'resume',
            'craft_rescue_id',
            'craft_rescue',
            'id',
            'CASCADE'
        );

        $this->createIndex(
            'idx-fast_rescuecraft_id',
            'resume',
            'craft_rescue_id'
        );

        $this->addForeignKey(
            'fk-resume-craft_rescue_id',
            'resume',
            'craft_rescue_id',
            'craft_rescue',
            'id',
            'CASCADE'
        );

        $this->createIndex(
            'idx-resume-gmdsss_id',
            'resume',
            'gmdsss_id'
        );

        $this->addForeignKey(
            'fk-resume-gmdsss_id',
            'resume',
            'gmdsss_id',
            'gmdsss',
            'id',
            'CASCADE'
        );

        $this->createIndex(
            'idx-resume-management_level_id',
            'resume',
            'management_level_id'
        );

        $this->addForeignKey(
            'fk-resume-management_level_id',
            'resume',
            'management_level_id',
            'management_level',
            'id',
            'CASCADE'
        );

        $this->createIndex(
            'idx-resume-ecdis_id',
            'resume',
            'ecdis_id'
        );

        $this->addForeignKey(
            'fk-resume-ecdis_id',
            'resume',
            'ecdis_id',
            'ecdis',
            'id',
            'CASCADE'
        );

        $this->createIndex(
            'idx-resume-radar_observation_id',
            'resume',
            'radar_observation_id'
        );

        $this->addForeignKey(
            'fk-resume-radar_observation_id',
            'resume',
            'radar_observation_id',
            'radar_observation',
            'id',
            'CASCADE'
        );

        $this->createIndex(
            'idx-resume-hazmat_id',
            'resume',
            'hazmat_id'
        );

        $this->addForeignKey(
            'fk-resume-hazmat_id',
            'resume',
            'hazmat_id',
            'hazmat',
            'id',
            'CASCADE'
        );

        $this->createIndex(
            'idx-resume-oil_tanker_id',
            'resume',
            'oil_tanker_id'
        );

        $this->addForeignKey(
            'fk-resume-oil_tanker_id',
            'resume',
            'oil_tanker_id',
            'oil_tanker',
            'id',
            'CASCADE'
        );

        $this->createIndex(
            'idx-resume-advance_oil_tanker_id',
            'resume',
            'advance_oil_tanker_id'
        );

        $this->addForeignKey(
            'fk-resume-advance_oil_tanker_id',
            'resume',
            'advance_oil_tanker_id',
            'advance_oil_tanker',
            'id',
            'CASCADE'
        );

        $this->createIndex(
            'idx-resume-chemical_tanker_id',
            'resume',
            'chemical_tanker_id'
        );

        $this->addForeignKey(
            'fk-resume-chemical_tanker_id',
            'resume',
            'chemical_tanker_id',
            'chemical_tanker',
            'id',
            'CASCADE'
        );

        $this->createIndex(
            'idx-resume-gas_tanker_id',
            'resume',
            'gas_tanker_id'
        );

        $this->addForeignKey(
            'fk-resume-gas_tanker_id',
            'resume',
            'gas_tanker_id',
            'gas_tanker',
            'id',
            'CASCADE'
        );

        $this->createIndex(
            'idx-resume-advance_gas_tanker_id',
            'resume',
            'advance_gas_tanker_id'
        );

        $this->addForeignKey(
            'fk-resume-advance_gas_tanker_id',
            'resume',
            'advance_gas_tanker_id',
            'advance_gas_tanker',
            'id',
            'CASCADE'
        );

        $this->createIndex(
            'idx-resume-crude_oil_washing_id',
            'resume',
            'crude_oil_washing_id'
        );

        $this->addForeignKey(
            'fk-resume-crude_oil_washing_id',
            'resume',
            'crude_oil_washing_id',
            'crude_oil_washing',
            'id',
            'CASCADE'
        );

        $this->createIndex(
            'idx-resume-inert_gas_plant_id',
            'resume',
            'inert_gas_plant_id'
        );

        $this->addForeignKey(
            'fk-resume-inert_gas_plant_id',
            'resume',
            'inert_gas_plant_id',
            'inert_gas_plant',
            'id',
            'CASCADE'
        );

        $this->createIndex(
            'idx-resume-ism_code_id',
            'resume',
            'ism_code_id'
        );

        $this->addForeignKey(
            'fk-resume-ism_code_id',
            'resume',
            'ism_code_id',
            'ism_code',
            'id',
            'CASCADE'
        );

        $this->createIndex(
            'idx-resume-ship_security_officer_id',
            'resume',
            'ship_security_officer_id'
        );

        $this->addForeignKey(
            'fk-resume-ship_security_officer_id',
            'resume',
            'ship_security_officer_id',
            'ship_security_officer',
            'id',
            'CASCADE'
        );

        $this->createIndex(
            'idx-resume-ship_safety_officer_id',
            'resume',
            'ship_safety_officer_id'
        );

        $this->addForeignKey(
            'fk-resume-ship_safety_officer_id',
            'resume',
            'ship_safety_officer_id',
            'ship_safety_officer',
            'id',
            'CASCADE'
        );

        $this->createIndex(
            'idx-resume-bridge_team_management_id',
            'resume',
            'bridge_team_management_id'
        );

        $this->addForeignKey(
            'fk-resume-bridge_team_management_id',
            'resume',
            'bridge_team_management_id',
            'bridge_team_management',
            'id',
            'CASCADE'
        );

        $this->createIndex(
            'idx-resume-dp_induction_id',
            'resume',
            'dp_induction_id'
        );

        $this->addForeignKey(
            'fk-resume-dp_induction_id',
            'resume',
            'dp_induction_id',
            'dp_induction',
            'id',
            'CASCADE'
        );

        $this->createIndex(
            'idx-resume-dp_simulator_id',
            'resume',
            'dp_simulator_id'
        );

        $this->addForeignKey(
            'fk-resume-dp_simulator_id',
            'resume',
            'dp_simulator_id',
            'dp_simulator',
            'id',
            'CASCADE'
        );

        $this->createIndex(
            'idx-resume-bridge_engine_room_resource_management_id',
            'resume',
            'bridge_engine_room_resource_management_id'
        );

        $this->addForeignKey(
            'fk-resume-bridge_engine_room_resource_management_id',
            'resume',
            'bridge_engine_room_resource_management_id',
            'bridge_engine_room_resource_management',
            'id',
            'CASCADE'
        );

        $this->createIndex(
            'idx-resume-ship_handling_id',
            'resume',
            'ship_handling_id'
        );

        $this->addForeignKey(
            'fk-resume-ship_handling_id',
            'resume',
            'ship_handling_id',
            'ship_handling',
            'id',
            'CASCADE'
        );

        $this->createIndex(
            'idx-resume-internal_auditors_course_id',
            'resume',
            'internal_auditors_course_id'
        );

        $this->addForeignKey(
            'fk-resume-internal_auditors_course_id',
            'resume',
            'internal_auditors_course_id',
            'internal_auditors_course',
            'id',
            'CASCADE'
        );

        $this->createIndex(
            'idx-resume-training_for_seafarers_id',
            'resume',
            'training_for_seafarers_id'
        );

        $this->addForeignKey(
            'fk-resume-training_for_seafarers_id',
            'resume',
            'training_for_seafarers_id',
            'training_for_seafarers',
            'id',
            'CASCADE'
        );

        $this->createIndex(
            'idx-resume-security_awareness_id',
            'resume',
            'security_awareness_id'
        );

        $this->addForeignKey(
            'fk-resume-security_awareness_id',
            'resume',
            'security_awareness_id',
            'security_awareness',
            'id',
            'CASCADE'
        );

        $this->createIndex(
            'idx-resume-electical_electronic_id',
            'resume',
            'electical_electronic_id'
        );

        $this->addForeignKey(
            'fk-resume-electical_electronic_id',
            'resume',
            'electical_electronic_id',
            'electical_electronic',
            'id',
            'CASCADE'
        );

        $this->createIndex(
            'idx-resume-new_building_id',
            'resume',
            'new_building_id'
        );

        $this->addForeignKey(
            'fk-resume-new_building_id',
            'resume',
            'new_building_id',
            'new_building',
            'id',
            'CASCADE'
        );

        $this->createIndex(
            'idx-resume-specialised_projects_id',
            'resume',
            'specialised_projects_id'
        );

        $this->addForeignKey(
            'fk-resume-specialised_projects_id',
            'resume',
            'specialised_projects_id',
            'specialised_projects',
            'id',
            'CASCADE'
        );

        $this->createIndex(
            'idx-resume-special_trades_id',
            'resume',
            'special_trades_id'
        );

        $this->addForeignKey(
            'fk-resume-special_trades_id',
            'resume',
            'special_trades_id',
            'special_trades',
            'id',
            'CASCADE'
        );

        $this->createIndex(
            'idx-resume-shore_experience_id',
            'resume',
            'shore_experience_id'
        );

        $this->addForeignKey(
            'fk-resume-shore_experience_id',
            'resume',
            'shore_experience_id',
            'shore_experience',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%resume}}');
    }
}
