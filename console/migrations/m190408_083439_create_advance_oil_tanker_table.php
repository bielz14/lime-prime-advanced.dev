<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%advanced_oil_tanker}}`.
 */
class m190408_083439_create_advance_oil_tanker_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%advance_oil_tanker}}', [
            'id' => $this->primaryKey(),
            'number' => $this->string(),
            'iss_date' => $this->string(),
            'exp_date' => $this->string(),
            'iss_by' => $this->string(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%advance_oil_tanker}}');
    }
}
