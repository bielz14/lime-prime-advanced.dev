<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%service_details}}`.
 */
class m190410_135158_create_service_details_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%service_details}}', [
            'id' => $this->primaryKey(),
            'company_name' => $this->string(),
            'rank' => $this->string(),
            'vessel_name' => $this->string(),
            'signed_on' => $this->string(),
            'signed_off' => $this->string(),
            'period_in_months' => $this->string(),
            'type_of_vessel' => $this->string(),
            'dwt' => $this->string(),
            'engine_type' => $this->string(),
            'bhp' => $this->string(),
            'kw' => $this->string()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%service_details}}');
    }
}
