<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%certificate_yellow_fever}}`.
 */
class m190408_075358_create_certificate_yellow_fever_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%certificate_yellow_fever}}', [
            'id' => $this->primaryKey(),
            'iss_date' => $this->string(),
            'exp_date' => $this->string(),
            'iss_by' => $this->string(),
            'iss_at' => $this->string()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%certificate_yellow_fever}}');
    }
}
