<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%ship_safety_officer}}`.
 */
class m190408_083701_create_ship_safety_officer_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%ship_safety_officer}}', [
            'id' => $this->primaryKey(),
            'number' => $this->string(),
            'iss_date' => $this->string(),
            'exp_date' => $this->string(),
            'iss_by' => $this->string(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%ship_safety_officer}}');
    }
}
