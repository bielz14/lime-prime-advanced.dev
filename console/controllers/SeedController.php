<?php 

namespace console\controllers;

use Yii;
use yii\console\Controller;
use common\models\Admin;

class SeedController extends Controller
{
    public function actionIndex()
    {
        $faker = \Faker\Factory::create();

        $user = new Admin();
        $user->setIsNewRecord(true);
        $user->name = 'root';
        $user->email = 'root@mail.com';
        $user->setPassword('Gt3Pd2Kh');
        $user->generateAuthKey();
        $user->save();
    }
}