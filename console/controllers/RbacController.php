<?php

namespace console\controllers;

use Yii;
use yii\console\Controller;
/**
 * Инициализатор RBAC выполняется в консоли php yii rbac/init
 */
class RbacController extends Controller {

    public function actionInit() {
        $auth = Yii::$app->authManager;
        
        $auth->removeAll(); //На всякий случай удаляем старые данные из БД...
        
        $admin = $auth->createRole('admin');
        $manager = $auth->createRole('manager');
        $user = $auth->createRole('user');
        
        $auth->add($admin);
        $auth->add($manager);
        $auth->add($user);
        
        $viewAdminPage = $auth->createPermission('viewAdminPage');
        $viewAdminPage->description = 'Просмотр админки';
        $auth->add($viewAdminPage);
        $auth->addChild($admin, $viewAdminPage);

        $viewManagerPage = $auth->createPermission('viewManagerPage');
        $viewManagerPage->description = 'Просмотр менеджер-панели';
        $auth->add($viewManagerPage);
        $auth->addChild($manager, $viewManagerPage);

        $createQuestionsList = $auth->createPermission('createQuestionsList');
        $createQuestionsList->description = 'Создание списка вопросов';
        $auth->add($createQuestionsList);
        $auth->addChild($manager, $createQuestionsList);

        $updateQuestionsList = $auth->createPermission('updateQuestionsList');
        $updateQuestionsList->description = 'Обновление списка вопросов';
        $auth->add($updateQuestionsList);
        $auth->addChild($manager, $updateQuestionsList);

        $outputQuestionsList = $auth->createPermission('outputQuestionsList');
        $outputQuestionsList->description = 'Вывод вопросов из списка вопросов';
        $auth->add($outputQuestionsList);
        $auth->addChild($manager, $outputQuestionsList);

        $deleteQuestionsList = $auth->createPermission('deleteQuestionsList');
        $deleteQuestionsList->description = 'Удаление списка вопросов';
        $auth->add($deleteQuestionsList);
        $auth->addChild($manager, $deleteQuestionsList);

        $createQuestion = $auth->createPermission('createQuestion');
        $createQuestion->description = 'Создание вопроса';
        $auth->add($createQuestion);
        $auth->addChild($manager, $createQuestion);

        $updateQuestion = $auth->createPermission('updateQuestion');
        $updateQuestion->description = 'Обновление вопроса';
        $auth->add($updateQuestion);
        $auth->addChild($manager, $updateQuestion);

        $deleteQuestion = $auth->createPermission('deleteQuestion');
        $deleteQuestion->description = 'Удаление вопроса';
        $auth->add($deleteQuestion);
        $auth->addChild($manager, $deleteQuestion);

        $viewResumePage = $auth->createPermission('viewResumePage');
        $viewResumePage->description = 'Доступ к странице для создания резюме';
        $auth->add($viewResumePage);
        $auth->addChild($user, $viewResumePage);

        $createResume = $auth->createPermission('createResume');
        $createResume->description = 'Создание резюме';
        $auth->add($createResume);
        $auth->addChild($user, $createResume);

        $updateResume = $auth->createPermission('updateResume');
        $updateResume->description = 'Обновление резюме';
        $auth->add($updateResume);
        $auth->addChild($user, $updateResume);

        $auth->addChild($admin, $user);
        $auth->addChild($admin, $manager);
        //$auth->addChild($manager, $user);

        //$auth->assign($admin, 19);//here value 19 is ID
        //$auth->assign($manager, 23);
    }
}